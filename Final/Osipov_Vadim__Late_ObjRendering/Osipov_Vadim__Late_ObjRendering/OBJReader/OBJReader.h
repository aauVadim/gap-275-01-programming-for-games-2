//	Name: Vadim Osipov
//	OBJReader.h


#ifndef _OBJREADER_H__
#define _OBJREADER_H__

#include <iostream>
#include <fstream>
#include <vector>
#include <string>
using std::string;

#include "../Object/Object.h"

class OBJReader
{
public:
	Object* CreateObjectFromFile(const char* fileName);

private:
	void ReadVertex(std::string& line, std::vector<Vertex>& vertVector);
	void ReadFaces(std::string& line, std::vector<Face>& faceVector);
};

#endif