//	Name: Vadim Osipov
//	Object.cpp

#include "Object.h"

Object::Object(std::vector<Vertex> vertexList, std::vector<Face> faceList)
{
	for (unsigned int i = 0; i < vertexList.size(); ++i)
	{
		m_vertexVector.push_back(vertexList[i].m_x);
		m_vertexVector.push_back(vertexList[i].m_y);
		m_vertexVector.push_back(vertexList[i].m_z);
	}

	for (unsigned int i = 0; i < faceList.size(); ++i)
	{
		m_faceVector.push_back(faceList[i].m_x);
		m_faceVector.push_back(faceList[i].m_y);
		m_faceVector.push_back(faceList[i].m_z);
	}
}

Object::~Object()
{
	delete m_indices;
	delete m_vertex;

	m_faceVector.clear();
	m_vertexVector.clear();
}