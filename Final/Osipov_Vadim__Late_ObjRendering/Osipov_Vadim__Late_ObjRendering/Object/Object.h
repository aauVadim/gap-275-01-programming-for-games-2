//	Name: Vadim Osipov
//	Object.h

#ifndef __OBJECT_H__
#define __OBJECT_H__

#include <vector>

struct Vertex
{
	float m_x;
	float m_y;
	float m_z;
};

struct Face
{
	int m_x;
	int m_y;
	int m_z;
};

class Object
{

private:
	float* m_vertex;
	int* m_indices;
	std::vector<float> m_vertexVector;
	std::vector<int> m_faceVector;

public:
	Object(std::vector<Vertex> vertexList, std::vector<Face> faceList);
	~Object();

	std::vector<float> GetVertices(){ return m_vertexVector; }
	std::vector<int> GetIndices(){ return m_faceVector; }
};

#endif