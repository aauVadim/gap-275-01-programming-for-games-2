// Name: Osipov Vadim
// Renderer.h

#ifndef __RENDERER_H__
#define __RENDERER_H__

#include <gl/glew.h>
#include <SDL.h>
#include <SDL_OpenGL.h>

class Object;

class Renderer
{
private:
	SDL_Window* m_pWindow;
	SDL_GLContext m_context;
	void SwitchModel();
	//Array of objects
	int m_numObjects;
	Object* m_pObjects[3];
	Object* m_pCurrentObject;
	int m_numCurrentObject;
	void CreateBuffer();
	GLuint m_triangleIndexBuffer;
	GLuint m_shaderProgram;
	GLuint m_vertArray;
	int m_indiceX;
	int m_indiceY;
	int m_indiceZ;


	static const int indices1[];
	static const int indices2[];
	int m_index;
public:
	Renderer();
	~Renderer();
	bool Update();
};

#endif