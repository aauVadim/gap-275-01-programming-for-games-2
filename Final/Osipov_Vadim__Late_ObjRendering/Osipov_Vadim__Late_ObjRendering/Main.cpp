//	Name: Osipov Vadim
//	Main.cpp

#include <SDL.h>
#include <gl/glew.h> //Must be here
#include <SDL_OpenGL.h>
#include <conio.h>

#include "../Osipov_Vadim__Late_ObjRendering/Renderer/Renderer.h"

int main(int argc, char* argv[])
{
	Renderer renderer;


	bool done = false;
	while (!done)
	{
		done = renderer.Update();
	}

	return 0;
}
