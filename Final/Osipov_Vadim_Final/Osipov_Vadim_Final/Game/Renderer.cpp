
#include "Renderer.h"

#include "SDL.h"
#include <gl/glew.h>	//Glew


#include "../Globals/Macros.h"
#include "../Globals/Globals.h"
#include "../Shaders/FragmentShader.h"
#include "../Shaders/VertexShader.h"
#include "../ObjReader/ObjReader.h"
#include "../Globals/Models.h"

Renderer::Renderer()
	: m_gameState(GameState::k_loading)
	, m_pWindow(nullptr)
	, m_gameCamera(nullptr)	
	, m_pShader(nullptr)
	, m_pSun(nullptr)
	, m_pMercury(nullptr)
	, m_pVenus(nullptr)
	, m_pEarth(nullptr)
	, m_pMoon(nullptr)
	, m_pMars(nullptr)
	, m_pJupiter(nullptr)
	, m_pSaturn(nullptr)
	, m_pUranus(nullptr)
	, m_pNeptune(nullptr)
{

}

Renderer::~Renderer()
{
	//camera goes
	_SAFE_DELETE_(m_gameCamera);
	_SAFE_DELETE_(m_pShader);
	//Planets go
	_SAFE_DELETE_(m_pSun);
	_SAFE_DELETE_(m_pMercury);
	_SAFE_DELETE_(m_pVenus);
	_SAFE_DELETE_(m_pEarth);
	_SAFE_DELETE_(m_pMoon);
	_SAFE_DELETE_(m_pMars);
	_SAFE_DELETE_(m_pJupiter);
	_SAFE_DELETE_(m_pSaturn);
	_SAFE_DELETE_(m_pUranus);
	_SAFE_DELETE_(m_pNeptune);

	SDL_GL_DeleteContext(m_context);
	SDL_DestroyWindow(m_pWindow);
	SDL_Quit();
}

//--------------------------------------------------------------------------------------------------------------------
//	Init()
//--------------------------------------------------------------------------------------------------------------------
void Renderer::Initialize()
{

	SDL_Init(SDL_INIT_VIDEO);
	// Minimally OpenGL 4.0!
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 3);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
	// Tell GL we want a context that supports debugging!
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_FLAGS, SDL_GL_CONTEXT_DEBUG_FLAG);

	m_pWindow = SDL_CreateWindow("OpenGL",
		SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED,
		800, 600,
		SDL_WINDOW_SHOWN | SDL_WINDOW_OPENGL);

	m_context = SDL_GL_CreateContext(m_pWindow);

	glewExperimental = GL_TRUE;
	glewInit();
	// After this point, the GL function pointers should have been set up by GLEW so they can
	// be called!

	SDL_Log("OpenGL Version: %s", glGetString(GL_VERSION));
	SDL_Log("GLEW Version: %s", glewGetString(GLEW_VERSION));

	glGetError(); // Glew tends to generate GL_INVALID_ENUM (https://www.opengl.org/wiki/OpenGL_Loading_Library)

	glDebugMessageControl(GL_DONT_CARE, GL_DONT_CARE, GL_DONT_CARE, 0, nullptr, GL_TRUE);


	m_pShader = new Shader(g_kFragmentShaderCode, g_kVertexShaderCode);
	m_gameCamera = new Camera();

	CreatePlanets();
}

void Renderer::Update()
{
	cml::matrix44f_c viewMatrix;
	viewMatrix.identity();
	cml::matrix44f_c projectionMatrix;
	projectionMatrix.identity();
	cml::matrix_perspective_xfov_RH(projectionMatrix, 70.0f, 800.0f / 600.0f, 0.1f, 1000.0f, cml::z_clip_neg_one);

	glClearColor(0.5f, 0.5f, 0.5f, 1.0f);
	glEnable(GL_DEPTH_TEST);
	glClearDepth(1.0f);

	//Culling front face
	while (m_gameState != GameState::k_over)
	{
		SDL_Event event;
		while (SDL_PollEvent(&event))
		{
			if ((event.type == SDL_WINDOWEVENT && event.window.event == SDL_WINDOWEVENT_CLOSE) || (event.type == SDL_KEYDOWN && event.key.keysym.sym == SDLK_ESCAPE))
			{
				m_gameState = GameState::k_over;
				break;
			}
			//Input
			HandleInput(event);
		}
		
		//Rotating planets
		OrbitPlanets();

		//Drawing things out
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
		cml::vector3f cameraPosition = m_gameCamera->GetCameraPosition();
		cml::matrix44f_c cameraRotation;
		cameraRotation.identity();
		//Camera rotation
		cml::matrix_rotate_about_local_x(cameraRotation, 0.0f);
		cml::vector4f cameraForward(0.0f, 0.0f, -1.0f, 0.0f);	//Unit Vector? WTF? 
		cml::vector3f cameraDirection = (cameraRotation * cameraForward).subvector(3);
		viewMatrix = m_gameCamera->GetTransform();

		//CAMERA POSITIONING
		//View Matrix, Eye position, Eye Direction
		cml::matrix_look_at_RH(viewMatrix, cameraPosition,
			cameraPosition + cameraDirection,
			cml::vector3f(0.0f, 1.0f, 0.0f));

		glUseProgram(m_pShader->GetShaderProgram());
		GLuint projectionMatrixUniform = glGetUniformLocation(m_pShader->GetShaderProgram(), "projectionMatrix");
		GLuint viewMatrixUniform = glGetUniformLocation(m_pShader->GetShaderProgram(), "viewMatrix");

		glProgramUniformMatrix4fv(m_pShader->GetShaderProgram(), projectionMatrixUniform, 1, GL_FALSE, projectionMatrix.data());
		glProgramUniformMatrix4fv(m_pShader->GetShaderProgram(), viewMatrixUniform, 1, GL_FALSE, viewMatrix.data());

		//rendering planets one by one
		RenderPlanets();

		SDL_GL_SwapWindow(m_pWindow);
	}

	SDL_GL_DeleteContext(m_context);
	SDL_DestroyWindow(m_pWindow);
	SDL_Quit();
}

void Renderer::HandleInput(SDL_Event& event)
{

	if (event.type == SDL_KEYDOWN)
	{
		if (event.key.keysym.sym == SDLK_w)
		{
			m_gameCamera->MoveForward(_CAMERA_MOVEMENT_SPEED_);
		}

		if (event.key.keysym.sym == SDLK_s)
		{
			m_gameCamera->MoveForward(-_CAMERA_MOVEMENT_SPEED_);
		}

		//Move Up/Down
		if (event.key.keysym.sym == SDLK_1)
			m_gameCamera->MoveUp(_CAMERA_MOVEMENT_SPEED_);
		if (event.key.keysym.sym == SDLK_2)
			m_gameCamera->MoveUp(-_CAMERA_MOVEMENT_SPEED_);

		//Move right/left
		if (event.key.keysym.sym == SDLK_d)
			m_gameCamera->MoveRight(_CAMERA_MOVEMENT_SPEED_);
		if (event.key.keysym.sym == SDLK_a)
			m_gameCamera->MoveRight(-_CAMERA_MOVEMENT_SPEED_);

		//Yaw
		if (event.key.keysym.sym == SDLK_z)
			m_gameCamera->Yaw(_CAMERA_MOVEMENT_SPEED_);
		if (event.key.keysym.sym == SDLK_x)
			m_gameCamera->Yaw(-_CAMERA_MOVEMENT_SPEED_);
		//Pitch
		if (event.key.keysym.sym == SDLK_e)
			m_gameCamera->Pitch(_CAMERA_MOVEMENT_SPEED_);
		if (event.key.keysym.sym == SDLK_q)
			m_gameCamera->Pitch(-_CAMERA_MOVEMENT_SPEED_);
		//Roll
		if (event.key.keysym.sym == SDLK_r)
			m_gameCamera->Roll(_CAMERA_MOVEMENT_SPEED_);
		if (event.key.keysym.sym == SDLK_t)
			m_gameCamera->Roll(-_CAMERA_MOVEMENT_SPEED_);
	}
}

void Renderer::RenderPlanets()
{
	m_pSun->GetPlanetBaseObject()->Render();
	m_pMercury->GetPlanetBaseObject()->Render();
	m_pVenus->GetPlanetBaseObject()->Render();
	m_pEarth->GetPlanetBaseObject()->Render();
	m_pMoon->GetPlanetBaseObject()->Render();
	m_pMars->GetPlanetBaseObject()->Render();
	m_pJupiter->GetPlanetBaseObject()->Render();
	m_pSaturn->GetPlanetBaseObject()->Render();
	m_pUranus->GetPlanetBaseObject()->Render();
	m_pUranus->GetPlanetBaseObject()->Render();
}

Object* Renderer::CreateObject(Object* object)
{
	ObjReader reader; 
	object = reader.CreateObjectFromFile(g_kSphereModel);

	return object;
}

void Renderer::OrbitPlanets()
{
	m_pSun->GetPlanetBaseObject()->PlusObjectOrbit		(0.0f, 0.01f, 0.0f);
	m_pMercury->GetPlanetBaseObject()->PlusObjectOrbit	(0.0f, 0.003f, 0.0f);
	m_pVenus->GetPlanetBaseObject()->PlusObjectOrbit	(0.0f, 0.008f, 0.0f);
	m_pEarth->GetPlanetBaseObject()->PlusObjectOrbit	(0.0f, 0.005f, 0.0f);
	m_pMoon->GetPlanetBaseObject()->PlusObjectOrbit		(0.0f, 0.01f, 0.01f);
	m_pMars->GetPlanetBaseObject()->PlusObjectOrbit		(0.0f, 0.008f, 0.0f);
	m_pJupiter->GetPlanetBaseObject()->PlusObjectOrbit	(0.0f, 0.003f, 0.0f);
	m_pSaturn->GetPlanetBaseObject()->PlusObjectOrbit	(0.0f, 0.006f, 0.0f);
	m_pUranus->GetPlanetBaseObject()->PlusObjectOrbit	(0.0f, 0.004f, 0.0f);
	m_pUranus->GetPlanetBaseObject()->PlusObjectOrbit	(0.0f, 0.003f, 0.0f);
}


void Renderer::CreatePlanets()
{
	//Sun
	Object* obj = nullptr;
	m_pSun = new Planet("Sun", CreateObject(obj));
	m_pSun->GetPlanetBaseObject()->Initialize(m_pShader->GetShaderProgram());
	m_pSun->GetPlanetBaseObject()->SetObjectPosition(0.0f, 0.0f, -300.0f);
	m_pSun->GetPlanetBaseObject()->SetObjectScale(10.0f, 10.0f, 10.0f);
	//Mercury
	Object* objMercury = nullptr;
	m_pMercury = new Planet("Earth", CreateObject(objMercury));
	m_pMercury->GetPlanetBaseObject()->Initialize(m_pShader->GetShaderProgram());
	m_pMercury->GetPlanetBaseObject()->SetObjectPosition(3.0f, 0.0f, 0.0f);
	m_pMercury->GetPlanetBaseObject()->SetObjectParent(m_pSun->GetPlanetBaseObject());
	m_pMercury->GetPlanetBaseObject()->SetObjectScale(0.1f, 0.1f, 0.1f);
	//Venus
	Object* objVenus = nullptr;
	m_pVenus = new Planet("Venus", CreateObject(objVenus));
	m_pVenus->GetPlanetBaseObject()->Initialize(m_pShader->GetShaderProgram());
	m_pVenus->GetPlanetBaseObject()->SetObjectPosition(7.0f, 0.0f, 0.0f);
	m_pVenus->GetPlanetBaseObject()->SetObjectParent(m_pSun->GetPlanetBaseObject());
	m_pVenus->GetPlanetBaseObject()->SetObjectScale(0.35f, 0.35f, 0.35f);

	//Earth
	Object* objEarth = nullptr;
	m_pEarth = new Planet("Earth", CreateObject(objEarth));
	m_pEarth->GetPlanetBaseObject()->Initialize(m_pShader->GetShaderProgram());
	m_pEarth->GetPlanetBaseObject()->SetObjectPosition(12.0f, 0.0f, 0.0f);
	m_pEarth->GetPlanetBaseObject()->SetObjectParent(m_pSun->GetPlanetBaseObject());
	m_pEarth->GetPlanetBaseObject()->SetObjectScale(0.3f, 0.3f, 0.3f);
	//Moon
	Object* objMoon = nullptr;
	m_pMoon = new Planet("Moon", CreateObject(objEarth));
	m_pMoon->GetPlanetBaseObject()->Initialize(m_pShader->GetShaderProgram());
	m_pMoon->GetPlanetBaseObject()->SetObjectPosition(3.0f, 0.0f, 0.0f);
	m_pMoon->GetPlanetBaseObject()->SetObjectParent(m_pEarth->GetPlanetBaseObject());
	m_pMoon->GetPlanetBaseObject()->SetObjectScale(0.3f, 0.3f, 0.3f);


	//Mars
	Object* objMars = nullptr;
	m_pMars = new Planet("Mars", CreateObject(objMars));
	m_pMars->GetPlanetBaseObject()->Initialize(m_pShader->GetShaderProgram());
	m_pMars->GetPlanetBaseObject()->SetObjectPosition(18.0f, 0.0f, 0.0f);
	m_pMars->GetPlanetBaseObject()->SetObjectParent(m_pSun->GetPlanetBaseObject());
	m_pMars->GetPlanetBaseObject()->SetObjectScale(0.1f, 0.1f, 0.1f);

	//Jupiter
	Object* objJupiter = nullptr;
	m_pJupiter = new Planet("Jupiter", CreateObject(objJupiter));
	m_pJupiter->GetPlanetBaseObject()->Initialize(m_pShader->GetShaderProgram());
	m_pJupiter->GetPlanetBaseObject()->SetObjectPosition(22.0f, 0.1f, 0.0f);
	m_pJupiter->GetPlanetBaseObject()->SetObjectParent(m_pSun->GetPlanetBaseObject());
	m_pJupiter->GetPlanetBaseObject()->SetObjectScale(0.5f, 0.5f, 0.5f);
	
	//Saturn
	Object* objSaturn = nullptr;
	m_pSaturn = new Planet("Saturn", CreateObject(objSaturn));
	m_pSaturn->GetPlanetBaseObject()->Initialize(m_pShader->GetShaderProgram());
	m_pSaturn->GetPlanetBaseObject()->SetObjectPosition(28.0f, 0.1f, 0.0f);
	m_pSaturn->GetPlanetBaseObject()->SetObjectParent(m_pSun->GetPlanetBaseObject());
	m_pSaturn->GetPlanetBaseObject()->SetObjectScale(0.4f, 0.4f, 0.4f);

	//Uranus
	Object* objUranus = nullptr;
	m_pUranus = new Planet("Uranus", CreateObject(objUranus));
	m_pUranus->GetPlanetBaseObject()->Initialize(m_pShader->GetShaderProgram());
	m_pUranus->GetPlanetBaseObject()->SetObjectPosition(32.0f, 0.1f, 0.0f);
	m_pUranus->GetPlanetBaseObject()->SetObjectParent(m_pSun->GetPlanetBaseObject());
	m_pUranus->GetPlanetBaseObject()->SetObjectScale(0.3f, 0.3f, 0.3f);

	//Neptune
	Object* objNeptune = nullptr;
	m_pNeptune = new Planet("Neptune", CreateObject(objNeptune));
	m_pNeptune->GetPlanetBaseObject()->Initialize(m_pShader->GetShaderProgram());
	m_pNeptune->GetPlanetBaseObject()->SetObjectPosition(38.0f, 0.1f, 0.0f);
	m_pNeptune->GetPlanetBaseObject()->SetObjectParent(m_pSun->GetPlanetBaseObject());
	m_pUranus->GetPlanetBaseObject()->SetObjectScale(0.3f, 0.3f, 0.3f);


}
