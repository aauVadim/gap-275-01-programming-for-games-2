//	Name: Osipov Vadim 
//	ObjReader.h

#ifndef __OBJREADER_H__
#define __OBJREADER_H__


#include <string>
#include <vector>

#include "../Globals/Globals.h"

class Object;

class ObjReader
{
public:
	Object* CreateObjectFromFile(const char* fileName);

private:
	void ReadVertex(std::string& line, std::vector<Vertex>& vertVector);
	void ReadFaces(std::string& line, std::vector<Face>& faceVector);
};

#endif