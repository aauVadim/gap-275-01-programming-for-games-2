#include "Camera.h"

Camera::Camera()
{
	m_cameraForward.set(0.0, 0.0, -1.0); // Look down Z
	m_cameraUp.set(0.0, 1.0, 0.0);
	m_cameraRight = cml::cross(m_cameraForward, m_cameraUp);
	m_cameraPosition.zero();
	m_cameraTransform.identity();
}

void Camera::MoveForward(float distance)
{
	m_cameraPosition += m_cameraForward * distance;

	UpdateTransform();
}

void Camera::MoveRight(float distance)
{
	m_cameraPosition += m_cameraRight * distance;
	
	UpdateTransform();
}

void Camera::MoveUp(float distance)
{
	m_cameraPosition += m_cameraUp * distance;

	UpdateTransform();
}

void Camera::Yaw(float degrees)
{
	cml::matrix44f_c yawMatrix;
	cml::matrix_rotation_axis_angle(yawMatrix, m_cameraUp, cml::rad(degrees));

	m_cameraForward = (yawMatrix * cml::vector4f(m_cameraForward, 0.0)).subvector(3);
	m_cameraForward.normalize();
	m_cameraRight = cml::cross(m_cameraForward, m_cameraUp);
	m_cameraRight.normalize();

	UpdateTransform();
}

void Camera::Pitch(float degrees)
{
	cml::matrix44f_c pitchMatrix;
	cml::matrix_rotation_axis_angle(pitchMatrix, m_cameraRight, cml::rad(degrees));

	m_cameraForward = (pitchMatrix * cml::vector4f(m_cameraForward, 0.0)).subvector(3);
	m_cameraForward.normalize();
	m_cameraUp = cml::cross(m_cameraRight, m_cameraForward);
	m_cameraUp.normalize();

	UpdateTransform();
}

void Camera::Roll(float degrees)
{
	cml::matrix44f_c rollMatrix;
	cml::matrix_rotation_axis_angle(rollMatrix, m_cameraForward, cml::rad(degrees));

	m_cameraRight = (rollMatrix * cml::vector4f(m_cameraRight, 0.0)).subvector(3);
	m_cameraRight.normalize();
	m_cameraUp = cml::cross(m_cameraRight, m_cameraForward);
	m_cameraUp.normalize();

	UpdateTransform();
}

void Camera::UpdateTransform()
{
	cml::matrix_look_at_RH(m_cameraTransform, m_cameraPosition, m_cameraPosition + m_cameraForward, m_cameraUp);
}
