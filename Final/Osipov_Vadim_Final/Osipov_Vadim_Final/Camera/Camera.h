#ifndef CAMERA_H
#define CAMERA_H

#include <cml/cml.h>

class Camera
{
public:
	Camera();

	const cml::matrix44f_c& GetTransform() const { return m_cameraTransform; }
	cml::vector3f GetCameraPosition() const { return m_cameraPosition; }

	void MoveForward(float distance);
	void MoveRight(float distance);
	void MoveUp(float distance);

	void Yaw(float degrees);
	void Pitch(float degrees);
	void Roll(float degrees);

private:
	void UpdateTransform();

	cml::vector3f m_cameraForward;
	cml::vector3f m_cameraRight;
	cml::vector3f m_cameraUp;
	cml::vector3f m_cameraPosition;
	cml::matrix44f_c m_cameraTransform;
};

#endif // CAMERA_H
