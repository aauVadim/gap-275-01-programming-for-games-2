
#ifndef __OBJECT_H__
#define __OBJECT_H__

#include <vector>
#include <string>

#include "../Globals/Globals.h"

#include <SDL.h>
#include <SDL_OpenGL.h>

#include <cml/cml.h>	//Math

class Object
{
	//-----------------------------------------------------------------------------------------------------------------
	//	Face and Vertex arrays
	//-----------------------------------------------------------------------------------------------------------------
	std::vector<Face> m_faces; // to get size num of items * size of item
	std::vector<Vertex> m_vertex;
	std::string m_fileName;

	//-----------------------------------------------------------------------------------------------------------------
	//	Buffers
	//-----------------------------------------------------------------------------------------------------------------
	//Object buffer
	GLuint m_objectVertexBuffer;
	GLuint m_objectIndexBuffer;

	//-----------------------------------------------------------------------------------------------------------------
	//	Transform Variables
	//-----------------------------------------------------------------------------------------------------------------
	float m_objPosX, m_objPosY, m_objPosZ;
	float m_objRotX, m_objRotY, m_objRotZ;
	float m_objScaleX, m_objScaleY, m_objScaleZ;
	//-----------------------------------------------------------------------------------------------------------------
	//	Transform Matricies
	//-----------------------------------------------------------------------------------------------------------------
	//Translation Component
	cml::matrix44f_c m_objectTranslation;
	//Rotation Component
	cml::matrix44f_c m_objectRotation;
	//Scale Component
	cml::matrix44f_c m_objectScale;
	// Object Transform - Translation * Rotation * Scale
	cml::matrix44f_c m_objectTransform;

	cml::matrix44f_c m_objectOrbitMatrix;

	//-----------------------------------------------------------------------------------------------------------------
	//	Vertex Array
	//-----------------------------------------------------------------------------------------------------------------
	GLuint m_vertexArray;
	GLuint m_shaderProgram;
	GLint m_transformMatrixUniform;

	//-----------------------------------------------------------------------------------------------------------------
	//	Vertex Array
	//-----------------------------------------------------------------------------------------------------------------
	Object* m_pParent;

public:
	Object(std::string myFileName, std::vector<Face> myFaces, std::vector<Vertex> myVertex);

	//Returns the array
	const std::vector<Face> GetObjectFaces() const { return m_faces; }
	const std::vector<Vertex> GetObjectVertex() const { return m_vertex; }

	const int GetObjectFaceArraySize() const { return m_faces.size() * sizeof(Face); }
	const int GetObjectVertexArraySize() const { return m_vertex.size() * sizeof(Vertex); }
	
	//Vertex buffer
	GLuint GetObjectVertexBuffer() const { return m_objectVertexBuffer; }
	void SetObjectVertexBuffer(GLuint& buffer) { m_objectVertexBuffer = buffer; }
	//Index buffer
	GLuint GetObjectIndexBuffer() const { return m_objectIndexBuffer; }
	//Transform Matrix
	GLuint GetVertexArray() const { return m_vertexArray; }
	
	cml::matrix44f_c GetObjectTransform();
	
	void SetObjectPosition(float posX, float posY, float posZ);
	
	void SetObjectScale(float scaleX, float scaleY, float scaleZ);
	
	void PlusObjectOrbit(float plusX, float plusY, float plusZ);
	
	void SetShaderProgramm(GLuint shaderProgram) { m_shaderProgram = shaderProgram; }
	
	void SetObjectParent(Object* pParent) { m_pParent = pParent; }
	
	//Main Renderer function
	void Render();
	void Initialize(GLuint shaderProgram);

private:
	void InitializeTransform();

};


#endif //__OBJECT_H__