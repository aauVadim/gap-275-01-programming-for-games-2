

#ifndef __MACROS_H__
#define __MACROS_H__

#define _SAFE_DELETE_(_ptr_) delete _ptr_; _ptr_ = nullptr

#define GLSL(x) "#version 400\n" #x

#endif