
#include <SDL.h>
#include <gl/glew.h>  // Needed before SDL_OpenGL.h!
#include <SDL_OpenGL.h>

#include "Shader.h"



Shader::Shader(const char* fragmentShader, const char* vertexShader)
{
	m_vertexShader = LoadShader(GL_VERTEX_SHADER, vertexShader);
	m_fragmentShader = LoadShader(GL_FRAGMENT_SHADER, fragmentShader);

	m_shaderProgram = glCreateProgram();
	glAttachShader(m_shaderProgram, m_vertexShader);
	glAttachShader(m_shaderProgram, m_fragmentShader);
	glLinkProgram(m_shaderProgram);
	GLint linkSuccess = GL_FALSE;
	glGetProgramiv(m_shaderProgram, GL_LINK_STATUS, &linkSuccess);
	if (linkSuccess == GL_FALSE)
	{
		char programLog[1024] = { 0 };
		glGetProgramInfoLog(m_shaderProgram, _ARRAYSIZE(programLog), nullptr, programLog);
		SDL_Log("Failed to link shaders: %s", programLog);
		__debugbreak();
		glDeleteProgram(m_shaderProgram);
	}
}

GLuint Shader::LoadShader(GLenum shaderType, const char* shaderCode)
{
	GLuint shader = glCreateShader(shaderType);
	glShaderSource(shader, 1, &shaderCode, nullptr);
	glCompileShader(shader);
	GLint success = GL_FALSE;
	glGetShaderiv(shader, GL_COMPILE_STATUS, &success);
	if (success == GL_FALSE)
	{
		char shaderLog[1024] = { 0 };
		glGetShaderInfoLog(shader, _ARRAYSIZE(shaderLog), nullptr, shaderLog);
		SDL_Log("Failed to compile shader: %s", shaderLog);
		__debugbreak();
		// Make sure to delete the shader object!
		glDeleteBuffers(1, &shader);
		return 0;
	}

	return shader;
}