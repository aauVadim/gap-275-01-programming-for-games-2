

#ifndef __SHADER_H__
#define __SHADER_H__

#include <cml/cml.h>	//Math


class Shader
{
	//-----------------------------------------------------------------------------------------------------------------
	//	Shaders
	//-----------------------------------------------------------------------------------------------------------------
	GLuint m_vertexShader;
	GLuint m_fragmentShader;
	GLuint m_shaderProgram;

public:
	Shader(const char* fragmentShader, const char* vertexShader);
	GLuint LoadShader(GLenum shaderType, const char* shaderCode);
	
	GLuint GetShaderProgram() const { return m_shaderProgram; }
	GLuint GetFragmentShader() const { return m_fragmentShader; }
	GLuint GetVertexShader() const { return m_vertexShader; }

};

#endif