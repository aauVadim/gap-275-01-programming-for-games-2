

#ifndef __FRAGMENTSHADER_H__
#define __FRAGMENTSHADER_H__

#include "../Globals/Macros.h"

static const char* g_kFragmentShaderCode = GLSL(
	in vec3 vertexPositionOut;
	in vec3 vertexNormalOut;
	out vec4 colorRGBA;
	uniform vec4 objectColor = vec4(1.0, 1.0, 1.0, 1.0);
	uniform sampler2D objectTexture;

	uniform vec3 lightColor = vec3(1, 1, 1);			//Color of the light
	uniform vec3 lightPosition = vec3(20, 0, 0);		//Position of light source
	uniform vec3 materialAmbient = vec3(0.1, 0.1, 0.1);	//RGB Color ambiean 
	uniform vec3 materialDiffuse = vec3(0, 0, 1);		//RGB Color. Blue 

	void main() {
		vec3 L = normalize(lightPosition.xyz - vertexPositionOut.xyz);
		vec3 N = normalize(vertexNormalOut);
		float lightAmount = max(dot(N, L), 0);
		vec3 diffuse = materialDiffuse * lightColor * lightAmount;
		colorRGBA = vec4(diffuse + materialAmbient, 1.0);
	}
);

#endif