

#ifndef __VERTEXSHADER_H__
#define __VERTEXSHADER_H__

#include "../Globals/Macros.h"

static const char* g_kVertexShaderCode = GLSL(
	in vec3 vertex; //Vertext position
	in vec3 vertexColor;	//Vertex coloring
	out vec3 vertexNormalOut;
	out vec3 vertexPositionOut;
	uniform mat4 transformMatrix = mat4(1.0);
	uniform mat4 viewMatrix = mat4(1.0);
	uniform mat4 projectionMatrix = mat4(1.0);

	void main() {
		mat4 worldViewMatrix = viewMatrix * transformMatrix;
		vec4 vertexPosition = worldViewMatrix * vec4(vertex, 1.0);
		gl_Position = projectionMatrix * vertexPosition;
		vertexPositionOut = vertexPosition.xyz;
		vertexNormalOut = (transpose(inverse(worldViewMatrix)) * vec4(normalize(vertex.xyz), 0)).xyz; //Outputing color for next shader
	}
);

#endif