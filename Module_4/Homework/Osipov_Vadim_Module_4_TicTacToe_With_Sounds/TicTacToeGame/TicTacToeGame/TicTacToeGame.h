#ifndef MEMORYGAME_H
#define MEMORYGAME_H

#pragma once

#define WIN32_LEAN_AND_MEAN
#include <Windows.h>
#include <windowsx.h>
#include "Tile.h"
#include "GameBoard.h"

#include "SDL.h"
#include "SDL_mixer.h"


class TicTacToeGame
{
private: 
	GameBoard* m_pTheBoard;
	bool m_isRunning;
	//Window handle
	HWND m_hwnd;
	//Brush to paint the background
	HBRUSH m_bgBrush;
	//Brush to paint the front
	//HBRUSH m_notActiveFieldBrush;
	//brush to paint the back 
	HBRUSH m_activeFieldBrush;

	//Placing sounds
	Mix_Chunk* m_pPlaceO;
	Mix_Chunk* m_pPlaceX;
	//Victory sounds
	Mix_Chunk* m_pVictory;
	//Pat sound
	Mix_Chunk* m_pPatSound;
	//BG Music
	Mix_Music* m_pBGMusic;

public:
	//Singleton! 
	static TicTacToeGame& GetInstance();
	//"Some useful comment"
	bool m_IsPlayerOne; 
	//Used to completely shut the app down
	bool m_isAppRunning;

	TicTacToeGame();
	~TicTacToeGame();

	bool StartUp();
	void Update();
	void Shutdown();
	//Used to go to after game state. Who won and whatnot
	bool IsAppRunning();


	void PlayPlaceSound(bool isX);

	//static - deletes 'this' pointer - needed for WindProc
	static LRESULT CALLBACK WindowProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam);

private:
	//Painting BG function 
	void PaintWindow();
	//Will paint the tile to ither X or O. On Mouse Click 
	void OnClick(POINT mousePosition);
	//Will paint the tile to ither X or O. On Mouse Click 
	void OnKeyboard(int posX, int posY);

	bool LoadSounds();
	
};

#endif //MEMORYGAME_H