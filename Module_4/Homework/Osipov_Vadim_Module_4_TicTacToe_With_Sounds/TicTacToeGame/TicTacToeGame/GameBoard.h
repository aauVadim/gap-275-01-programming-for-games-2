#pragma once

#include "Tile.h"

class GameBoard
{
	GameBoard* m_gameBoard;
	Tile** m_ppGameBoardTilesArray;
	Tile* m_winnerTileArray;
	HWND m_hwnd;

public: 
	//Used to stop the game
	bool m_isGameOver;
	//Used if none of the player won
	bool m_isPat;
	//Usual goodies
	GameBoard();
	~GameBoard();

	//Methods 
	Tile* GetTile(int posX, int posY) { return &m_ppGameBoardTilesArray[posY][posX]; }
	GameBoard* GetGameBoard() { return m_gameBoard; };
	bool PaintGameFieldsBackround(HWND hwnd, HBRUSH fieldBrush);
	void PaintGameFields(HWND hwnd, HBRUSH fieldBrush, Tile& field, int col, int row);
	void DrawFieldsGraphic(Tile& currentTile, HDC dc, RECT fieldRect, HBRUSH currentBrush, bool isPlayerOne);
	void DrawWinnerLine(Tile& first, Tile& second, Tile& third);
	//Counts tiles - if the lines meet - game over
	void CountTiles();
};