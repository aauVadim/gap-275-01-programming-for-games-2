
#include "TicTacToeGame.h"
#include <stdlib.h>
#include <wchar.h>


#include "SDL_mixer.h"


void PrintWindowsError()
{
	DWORD errorCode = ::GetLastError();
	wchar_t errorString[1024];

	::FormatMessage(FORMAT_MESSAGE_FROM_SYSTEM, NULL, errorCode, 0, errorString, 1024, NULL);

	OutputDebugString(errorString);
}

TicTacToeGame& TicTacToeGame::GetInstance()
{
	//static - only one istance of this variable in this function. Will always be the same
	static TicTacToeGame theGame;
	return theGame;
}


bool TicTacToeGame::StartUp()
{

	//Initializing SDL Audio
	if (SDL_Init(SDL_INIT_AUDIO) < 0)
	{
		//Print things out
		return false;
	}
	//Initializing SDL_Mix1
	if (Mix_Init(MIX_INIT_MP3) < 0)
		return false; 

	//Bigger buffer for music - Smaller buffer for sound effects
	if (Mix_OpenAudio(MIX_DEFAULT_FREQUENCY, MIX_DEFAULT_FORMAT, MIX_DEFAULT_CHANNELS, 1024) < 0) //4096
	{
		::OutputDebugString(L"Mix_OpenAudio() - Failed");
		return false;
	}

	if (!LoadSounds())
	{
		::OutputDebugString(L"LoadSounds() - Failed");
		return false;
	}
	else
	{
		//Start playing BG Music
		Mix_PlayMusic(m_pBGMusic, 1);
	}

	HINSTANCE hInstance = (HINSTANCE)::GetModuleHandle(NULL);

	//Contains the window class attributes that are registered by the RegisterClass function. @MSDN
	WNDCLASSEX windowClass;	//WNDCLASS - Old school. WNDCLASSEX - Modern API

	windowClass.cbSize = sizeof(windowClass);
	//::ZeroMemory(&windowClass, sizeof(windowClass));

	windowClass.style = 0;
	windowClass.lpfnWndProc = &WindowProc;
	windowClass.cbWndExtra = 0;
	windowClass.cbClsExtra = 0;
	windowClass.hInstance = hInstance;
	windowClass.hIcon = 0;
	windowClass.hCursor = ::LoadCursor(NULL, IDC_CROSS);;
	windowClass.hbrBackground = 0;
	windowClass.lpszMenuName = 0;
	windowClass.lpszClassName = L"TicTacToeGame";
	windowClass.hIconSm = 0;
	//Neat way to zero things out ^^^ 
	//::ZeroMemory(&windowClass, sizeof(windowClass));

	if (::RegisterClassEx(&windowClass) == 0)
	{
		PrintWindowsError();
		return false;
	}
	//Creates an overlapped, pop - up, or child window with an extended window style; 
	//otherwise, this function is identical to the CreateWindow function.For more information about creating a window and for full descriptions of the other parameters of CreateWindowEx, see CreateWindow.
	m_hwnd = ::CreateWindowEx(0, L"TicTacToeGame", L"TIC TAC TOE",
		WS_OVERLAPPEDWINDOW ^ WS_THICKFRAME, 0, 0, 505, 525, NULL, NULL, hInstance, NULL);	//Ductaped size of the window. Love 'em magical numbers! FIXME

	if (m_hwnd == NULL)
	{
		PrintWindowsError();
		return false;
	}
	//Sets the specified window's show state. @MSDN 
	::ShowWindow(m_hwnd, SW_SHOW);

	::OutputDebugString(L"Created window!");

	/*Creating the bruches*/
	m_isRunning = true;
	//Brush with which we are paining the window
	m_bgBrush = ::CreateSolidBrush(RGB(51, 0, 0));
	//m_notActiveFieldBrush = ::CreateSolidBrush(RGB(153, 255, 255));
	m_activeFieldBrush = ::CreateSolidBrush(RGB(204, 255, 255));

	//Game Board
	m_pTheBoard = new GameBoard();
	//Painting the fields
	if (m_pTheBoard->PaintGameFieldsBackround(m_hwnd, m_activeFieldBrush))
		return true;

	return true;
}
void TicTacToeGame::Update()
{
	MSG msg;
	if (::GetMessage(&msg, m_hwnd, 0, 0))
	{
		::DispatchMessage(&msg);
	}
}

bool TicTacToeGame::LoadSounds()
{

	//Initializing sounds vars - O Sound
	m_pPlaceO = Mix_LoadWAV("Sounds/OPlaceSound.wav");
	if (!m_pPlaceO)
	{
		::OutputDebugString(L"\n Could not load Sounds/OPlaceSound.wav \n");
		return false;
		// something
	}

	//Initializing sounds vars - X Sound 
	m_pPlaceX = Mix_LoadWAV("Sounds/XPlaceSound.wav");
	if (!m_pPlaceX)
	{
		::OutputDebugString(L"\n Could not load Sounds/XPlaceSound.wav \n");
		return false;
		// something
	}

	m_pBGMusic = Mix_LoadMUS("Sounds/BackgroundMusic.mp3");
	if (!m_pBGMusic)
	{
		::OutputDebugString(L"\n Could not load Sounds/BackgroundMusic.mp3 \n");
		return false;
		// something
	}

	m_pVictory = Mix_LoadWAV("Sounds/VictorySound.wav");
	if (!m_pVictory)
	{
		::OutputDebugString(L"\n Could not load Sounds/VictorySound.wav \n");
		return false;
		// something
	}

	m_pPatSound = Mix_LoadWAV("Sounds/PatSound.wav");
	if (!m_pPatSound)
	{
		::OutputDebugString(L"\n Could not load Sounds/PatSound.wav \n");
		return false;
		// something
	}
	return true;
}

void TicTacToeGame::Shutdown()
{
	//Deletes brush - stops memory leak 
	::DeleteObject(m_bgBrush);
	::DeleteObject(m_activeFieldBrush);
	//Sounds
	//Clearing the audio
	Mix_FreeChunk(m_pPlaceO);
	Mix_FreeChunk(m_pPlaceX);
	Mix_FreeChunk(m_pPatSound);
	Mix_FreeChunk(m_pVictory);
	Mix_FreeMusic(m_pBGMusic);
	//closing Mixer
	Mix_CloseAudio();
	Mix_Quit();
	//closing SDL
	SDL_Quit();
}


TicTacToeGame::TicTacToeGame()
	: m_isRunning(false)
	, m_pTheBoard(nullptr)
	, m_IsPlayerOne(true)
	, m_isAppRunning(true)
	, m_hwnd(NULL)
	, m_bgBrush(NULL)
	, m_activeFieldBrush(NULL)
	, m_pPlaceO(nullptr)
	, m_pPlaceX(nullptr)
	, m_pBGMusic(nullptr)
	, m_pVictory(nullptr)
	, m_pPatSound(nullptr)
{

}

TicTacToeGame::~TicTacToeGame()
{

}

//Message that we recive form windows 
//Event handler - THE BIG DEAL! 
LRESULT CALLBACK TicTacToeGame::WindowProc(
	HWND   hwnd,
	UINT   uMsg,
	WPARAM wParam,
	LPARAM lParam
	)
{
	switch (uMsg)
	{
	case WM_CLOSE:
		TicTacToeGame::GetInstance().m_isAppRunning = false;
		TicTacToeGame::GetInstance().Shutdown();
		return 0;
		break;
	case WM_PAINT:
		TicTacToeGame::GetInstance().PaintWindow();
		break;
	case WM_LBUTTONDOWN:
		POINT mousePos;
		mousePos.x = GET_X_LPARAM(lParam);
		mousePos.y = GET_Y_LPARAM(lParam);
		TicTacToeGame::GetInstance().OnClick(mousePos);
		break;
	case WM_KEYDOWN:
		switch (wParam)
		{
		//KEYS
		case 0x51:	//Q
			TicTacToeGame::GetInstance().OnKeyboard(0, 0);
			break;
		case 0x57:	//W 
			TicTacToeGame::GetInstance().OnKeyboard(1, 0);
			break;
		case 0x45:	//E
			TicTacToeGame::GetInstance().OnKeyboard(2, 0);
			break;
		case 0x41:	//A
			TicTacToeGame::GetInstance().OnKeyboard(0, 1);
			break;
		case 0x53:	//S 
			TicTacToeGame::GetInstance().OnKeyboard(1, 1);
			break;
		case 0x44:	//D
			TicTacToeGame::GetInstance().OnKeyboard(2, 1);
			break;
		case 0x5A:	//Z
			TicTacToeGame::GetInstance().OnKeyboard(0, 2);
			break;
		case 0x58:	//X 
			TicTacToeGame::GetInstance().OnKeyboard(1, 2);
			break;
		case 0x43:	//C
			TicTacToeGame::GetInstance().OnKeyboard(2, 2);
			break;
		//NUMS
		case 0x67:	//Num 7
			TicTacToeGame::GetInstance().OnKeyboard(0, 0);
			break;
		case 0x68:	//Num 8
			TicTacToeGame::GetInstance().OnKeyboard(1, 0);
			break;
		case 0x69:	//Num 9
			TicTacToeGame::GetInstance().OnKeyboard(2, 0);
			break;
		case 0x64:	//Num 4
			TicTacToeGame::GetInstance().OnKeyboard(0, 1);
			break;
		case 0x65:	//Num 5
			TicTacToeGame::GetInstance().OnKeyboard(1, 1);
			break;
		case 0x66:	//Num 6
			TicTacToeGame::GetInstance().OnKeyboard(2, 1);
			break;
		case 0x61:	//Num 1
			TicTacToeGame::GetInstance().OnKeyboard(0, 2);
			break;
		case 0x62:	//Num 2
			TicTacToeGame::GetInstance().OnKeyboard(1, 2);
			break;
		case 0x63:	//Num 3
			TicTacToeGame::GetInstance().OnKeyboard(2, 2);
			break;
		}
	}

	return ::DefWindowProc(hwnd, uMsg, wParam, lParam);
}



void TicTacToeGame::PaintWindow()
{
	PAINTSTRUCT pc;
	// HDC ?????????. Thats where we 
	HDC dc = ::BeginPaint(TicTacToeGame::GetInstance().m_hwnd, &pc);
	//rectangle
	RECT clientRect;
	//Referencing given rectangle
	::GetClientRect(TicTacToeGame::GetInstance().m_hwnd, &clientRect);
	//Fills the background
	::FillRect(dc, &clientRect, m_bgBrush);
	//Paint teh board fields
	m_pTheBoard->PaintGameFieldsBackround(m_hwnd, m_activeFieldBrush);
	::EndPaint(TicTacToeGame::GetInstance().m_hwnd, &pc);
}

//Do things on click
void TicTacToeGame::OnClick(POINT mousePosition)
{
	HDC dc = ::GetDC(m_hwnd);
	
	//Checking if the game is not over
	if (!m_pTheBoard->m_isGameOver)
	{
		for (int y = 0; y < 3; ++y)
		{
			for (int x = 0; x < 3; ++x)
			{
				Tile* tile = m_pTheBoard->GetTile(x, y);
				const RECT& fieldRect = tile->m_myRect;
				if (::PtInRect(&fieldRect, mousePosition))
				{
					//Switching players
					m_IsPlayerOne = !m_IsPlayerOne;
					//Checking if tile was not used before
					if (!m_pTheBoard->GetTile(x, y)->m_isPlayed)
					{
						m_pTheBoard->DrawFieldsGraphic(*tile, dc, fieldRect, m_activeFieldBrush, m_IsPlayerOne);
						PlayPlaceSound(m_IsPlayerOne);
					}
				}
			}
		}
	}
}
//Do things for keyboard input
void TicTacToeGame::OnKeyboard(int posX, int posY)
{
	HDC dc = ::GetDC(TicTacToeGame::GetInstance().m_hwnd);
	//Switching Players
	m_IsPlayerOne = !m_IsPlayerOne;
	//Checking if was played
	if (!m_pTheBoard->GetTile(posX, posY)->m_isPlayed)
	{
		//Draw call
		m_pTheBoard->DrawFieldsGraphic(
			*m_pTheBoard->GetTile(posX, posY),
			dc,
			m_pTheBoard->GetTile(posX, posY)->m_myRect,
			m_activeFieldBrush,
			m_IsPlayerOne);
	}
}

bool TicTacToeGame::IsAppRunning()
{
	if (m_pTheBoard->m_isGameOver)
	{
		Mix_PauseMusic();
		if (!m_pTheBoard->m_isPat)
		{
			Mix_PlayChannel(-1, m_pVictory, 0);
			//Bit of ducttape - Because I set this bool befor fenction. 
			//I have to check in reverce
			if (!m_IsPlayerOne)
			{
				::MessageBox(m_hwnd, (L"Player 1 Won!"), (L"Game Over"), MB_OK);
				return false;
			}
			else
			{
				::MessageBox(m_hwnd, (L"Player 2 Won!"), (L"Game Over"), MB_OK);
				return false;
			}
		}
		else
		{
			Mix_PlayChannel(-1, m_pPatSound, 0);
			::MessageBox(m_hwnd, (L"No one won! Go fight about it."), (L"Game Over"), MB_OK);
			return false;
		}
	}
	else
		return m_isAppRunning;
}

void TicTacToeGame::PlayPlaceSound(bool isX)
{
	//Well... I like one line things 
	(isX) ? (Mix_PlayChannel(-1, m_pPlaceX, 0)) : Mix_PlayChannel(-1, m_pPlaceO, 0);
}

