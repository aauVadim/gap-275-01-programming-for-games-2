
#include "TicTacToeGame.h"
#include <stdlib.h>
#include <wchar.h>


void PrintWindowsError()
{
	DWORD errorCode = ::GetLastError();
	wchar_t errorString[1024];

	::FormatMessage(FORMAT_MESSAGE_FROM_SYSTEM, NULL, errorCode, 0, errorString, 1024, NULL);

	OutputDebugString(errorString);
}

TicTacToeGame& TicTacToeGame::getInstance()
{
	//static - only one istance of this variable in this function. Will always be the same
	static TicTacToeGame theGame; 
	return theGame;
}


bool TicTacToeGame::StartUp()
{
	HINSTANCE hInstance = (HINSTANCE)::GetModuleHandle(NULL);

	//Contains the window class attributes that are registered by the RegisterClass function. @MSDN
	WNDCLASSEX windowClass;	//WNDCLASS - Old school. WNDCLASSEX - Modern API

	windowClass.cbSize = sizeof(windowClass);
	//::ZeroMemory(&windowClass, sizeof(windowClass));

	windowClass.style = 0;
	windowClass.lpfnWndProc = &WindowProc;
	windowClass.cbWndExtra = 0;
	windowClass.cbClsExtra = 0;
	windowClass.hInstance = hInstance;
	windowClass.hIcon = 0;
	windowClass.hCursor = 0;
	windowClass.hbrBackground = 0;
	windowClass.lpszMenuName = 0;
	windowClass.lpszClassName = L"Win32IntroApp";
	windowClass.hIconSm = 0;
	//Neat way to zero things out ^^^ 
	//::ZeroMemory(&windowClass, sizeof(windowClass));

	if (::RegisterClassEx(&windowClass) == 0)
	{
		PrintWindowsError();
		return false;
	}
	//Creates an overlapped, pop - up, or child window with an extended window style; 
	//otherwise, this function is identical to the CreateWindow function.For more information about creating a window and for full descriptions of the other parameters of CreateWindowEx, see CreateWindow.
	m_hwnd = ::CreateWindowEx(0, L"Win32IntroApp", L"win32 Intro App",
		WS_OVERLAPPEDWINDOW, 0, 0, 505, 525, NULL, NULL, hInstance, NULL);	//Ductaped size of the window. Love 'em magical numbers! FIXME

	if (m_hwnd == NULL)
	{
		PrintWindowsError();
		return false;
	}
	//Sets the specified window's show state. @MSDN 
	::ShowWindow(m_hwnd, SW_SHOW);

	::OutputDebugString(L"Created window!");

	/*Creating the bruches*/
	m_isRunning = true;
	//Brush with which we are paining the window
	m_bgBrush = ::CreateSolidBrush(RGB(51, 0, 0));
	m_notActiveFieldBrush = ::CreateSolidBrush(RGB(153, 255, 255));
	m_activeFieldBrush = ::CreateSolidBrush(RGB(204, 255, 255));

	return true;
}
void TicTacToeGame::Update()
{
	MSG msg;
	if (::GetMessage(&msg, m_hwnd, 0, 0))
	{
		::DispatchMessage(&msg);
	}
/*
	RECT rect;
	::GetClientRect(TicTacToeGame::getInstance().m_hwnd, &rect);
	::InvalidateRect(m_hwnd, &rect, TRUE);*/
}
void TicTacToeGame::Shutdown()
{
	//Deletes brush - stops memory leak 
	::DeleteObject(m_bgBrush);
	::DeleteObject(m_notActiveFieldBrush);
}


TicTacToeGame::TicTacToeGame()
	: m_isRunning(false)
	, m_hwnd(NULL)
	, m_bgBrush(NULL)
	, m_notActiveFieldBrush(NULL)
{

}

TicTacToeGame::~TicTacToeGame()
{

}

//Message that we recive form windows 
//Event handler - THE BIG DEAL! 
LRESULT CALLBACK TicTacToeGame::WindowProc(
	HWND   hwnd,
	UINT   uMsg,
	WPARAM wParam,
	LPARAM lParam
	)
{
	switch (uMsg)
	{
	case WM_CLOSE:
		TicTacToeGame::getInstance().m_isRunning = false;
		return 0;
		break;
	case WM_PAINT:
		TicTacToeGame::getInstance().PaintWindow();
		break;
	case WM_LBUTTONDOWN:
		::OutputDebugString(L"\nClicked Left Mouse Button\n");
		POINT mousePoint; 
		if (GetCursorPos(&mousePoint))
		{
			if (ScreenToClient(hwnd, &mousePoint))
			{

			}
		}
		break;
	}
	
	return ::DefWindowProc(hwnd, uMsg, wParam, lParam);
}

void TicTacToeGame::PaintWindow()
{
	PAINTSTRUCT pc;
	// HDC ?????????. Thats where we 
	HDC dc = ::BeginPaint(TicTacToeGame::getInstance().m_hwnd, &pc);
	//Brush with which we are paining the window
	//HBRUSH bgBrush = ::CreateSolidBrush(RGB(0, 255, 0)); 
	//rectangle
	RECT clientRect;
	//Referencing given rectangle
	::GetClientRect(TicTacToeGame::getInstance().m_hwnd, &clientRect);
	//Fills the background
	::FillRect(dc, &clientRect, m_bgBrush);
	//Deletes brush - stops memory leak 
	//::DeleteObject(m_bgBrush);
	//Stopping the painting process

	
	/*for (int i = 0; i < 6; ++i)
	{
		if (i >= 3)
			m_cards[i].m_isCross = true;

		PaintGameFields(m_cards[i], i, 0);
	}*/

	for (int x = 0; x < 3; ++x)
	{
		for (int y = 0; y < 3; ++y)
		{
			//Ductape - big fucking chunk


			if (y % 2 == 0)
				m_cards[y].m_isCross = true;
			PaintGameFields(m_cards[y], x, y);
		}
	}

// Just messing around
//	::DrawTextEx(dc, (L"TEXT"), 5, &clientRect, NULL, NULL);
//	::TextOut(dc, 20, 20, (L"TEXT 2"), 10);
	::EndPaint(TicTacToeGame::getInstance().m_hwnd, &pc);
}

void TicTacToeGame::PaintGameFields(GameField& field, int col, int row)
{
	const int fieldWith = 150;
	const int fieldHeight = 150;

	const int offset = 5;

	RECT fieldRect;
	//Positionning
	fieldRect.left = 10 + (fieldWith + 10) * col;
	fieldRect.top = 10 + (fieldHeight + 10) * row;
	//Size of the fields
	fieldRect.right = fieldRect.left + 150;
	fieldRect.bottom = fieldRect.top + 150;

	//Graphic thing, still don't fully get it
	HDC dc = ::GetDC(m_hwnd);

	//Saving position of offset corners
	//Top-Left point with 5px offset in
	field.m_myPoints[0].x = fieldRect.left + offset;
	field.m_myPoints[0].y = fieldRect.top + offset;
	//Bottom-Right point with 5px offset in
	field.m_myPoints[1].x = fieldRect.right - offset;
	field.m_myPoints[1].y = fieldRect.bottom - offset;
	//Top-Right point with 5px offset in
	field.m_myPoints[2].x = fieldRect.left + (fieldWith - offset);
	field.m_myPoints[2].y = fieldRect.top + offset;
	//Bottom-Left point with 5px offset in
	field.m_myPoints[3].x = fieldRect.left + offset;
	field.m_myPoints[3].y = fieldRect.top + (fieldHeight - offset);

	//Drawing the graphics X and O
	DrawFieldsGraphic(field, dc, fieldRect, m_activeFieldBrush);
}
//Do things on click
void TicTacToeGame::OnClick(GameField& field)
{

}

void TicTacToeGame::DrawFieldsGraphic(GameField& field, HDC dc, RECT fieldRect, HBRUSH currentBrush)
{
	//Drawing thingies
	if (field.m_isCross)
	{
		//Make Crosses --- 

		//Fill background
		::FillRect(dc, &fieldRect, currentBrush);
		//Creating a pen
		HPEN linePen;
		//Pen Color
		COLORREF lineColor = RGB(255, 0, 0);
		//Creating actial pen
		linePen = ::CreatePen(PS_SOLID, 7, lineColor);
		//Selecting it
		HPEN penOld = (HPEN)::SelectObject(dc, linePen);

		//Moving to first pos
		::MoveToEx(dc, field.m_myPoints[0].x, field.m_myPoints[0].y, NULL);
		//Drawing to corner
		::LineTo(dc, field.m_myPoints[1].x, field.m_myPoints[1].y);

		//Moving to second pos
		::MoveToEx(dc, field.m_myPoints[2].x, field.m_myPoints[2].y, NULL);
		//Drawing to corner
		::LineTo(dc, field.m_myPoints[3].x, field.m_myPoints[3].y);


		//Cleaning up? 
		::DeleteObject(linePen);
		::DeleteObject(penOld);
	}
	else
	{
		//Make circles ---

		//Filling rectangle with bg brush
		::FillRect(dc, &fieldRect, currentBrush);
		//Making circles
		//Creating a pen
		HPEN linePen;
		//Color of the pen
		COLORREF lineColor = RGB(255, 0, 0);
		//Creating it
		linePen = ::CreatePen(PS_SOLID, 7, lineColor);
		//Selecting the pen
		HPEN penOld = (HPEN)::SelectObject(dc, linePen);
		//Making a circle
		::Arc(dc, field.m_myPoints[0].x, field.m_myPoints[0].y, field.m_myPoints[1].x, field.m_myPoints[1].y, 0, 0, 0, 0);

		//Cleaning up? 
		::DeleteObject(linePen);
		::DeleteObject(penOld);
	}
}
