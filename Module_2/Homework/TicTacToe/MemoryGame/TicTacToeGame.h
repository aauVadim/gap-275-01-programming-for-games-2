#ifndef MEMORYGAME_H
#define MEMORYGAME_H

#pragma once

#define WIN32_LEAN_AND_MEAN
#include <Windows.h>

struct GameField
{
	//Utility bools
	bool m_isPlayed;
	bool m_isCross;
	//Referencing its offset
	POINT m_myPoints[4];

	GameField()
		: m_isPlayed(false)
		, m_isCross(false)
	{

	}
};

class TicTacToeGame
{
public:
	static TicTacToeGame& getInstance();

	TicTacToeGame();
	~TicTacToeGame();

	bool StartUp();
	void Update();
	void Shutdown();

	bool isRunning() const { return m_isRunning; };


	//static - deletes 'this' pointer - needed for WindProc
	static LRESULT CALLBACK WindowProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam);

private:
	//Painting BG function 
	void PaintWindow();
	void PaintGameFields(GameField& card, int col, int row);
	void DrawFieldsGraphic(GameField& field, HDC dc, RECT fieldRect, HBRUSH currentBrush);
	//Will paint the tile to ither X or O 
	void OnClick(GameField& field);

	GameField m_cards[9];

	bool m_isRunning; 
	//Window handle
	HWND m_hwnd;
	//Brush to paint the background
	HBRUSH m_bgBrush; 
	//Brush to paint the front
	HBRUSH m_notActiveFieldBrush;
	//brush to paint the back 
	HBRUSH m_activeFieldBrush;
};

#endif //MEMORYGAME_H