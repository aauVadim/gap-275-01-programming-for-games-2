#include <SDL.h>
#include <gl/glew.h>  // Needed before SDL_OpenGL.h!
#include <SDL_OpenGL.h>

#include <cml/cml.h>

void CheckGLError()
{
	GLenum error = glGetError();
	if (error != 0)
	{
		SDL_Log("GL Error: %d %s", error, glewGetErrorString(error));
		__debugbreak();
	}
}

GLuint LoadShader(GLenum shaderType, const char* shaderCode)
{
	GLuint shader = glCreateShader(shaderType);
	glShaderSource(shader, 1, &shaderCode, nullptr);
	glCompileShader(shader);
	GLint success = GL_FALSE;
	glGetShaderiv(shader, GL_COMPILE_STATUS, &success);
	if (success == GL_FALSE)
	{
		char shaderLog[1024] = { 0 };
		glGetShaderInfoLog(shader, _ARRAYSIZE(shaderLog), nullptr, shaderLog);
		SDL_Log("Failed to compile shader: %s", shaderLog);
		__debugbreak();
		// Make sure to delete the shader object!
		glDeleteBuffers(1, &shader);
		return 0;
	}

	return shader;
}

void GLAPIENTRY PrintGLDebugMessage(GLenum source, GLenum type, GLuint id, GLenum severity, GLsizei length,
	const GLchar* message, const void* userParam)  // must add const to userParam, documentation is incorrect!
{
	SDL_Log("GL Error: %d %s", id, message);
	if (type == GL_DEBUG_TYPE_ERROR)
		__debugbreak();
}

int main(int argc, char* argv[])
{
	SDL_Init(SDL_INIT_VIDEO);

	// Minimally OpenGL 4.0!
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 3);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
	// Tell GL we want a context that supports debugging!
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_FLAGS, SDL_GL_CONTEXT_DEBUG_FLAG);

	SDL_Window* window = SDL_CreateWindow("OpenGL",
		SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED,
		800, 600,
		SDL_WINDOW_SHOWN | SDL_WINDOW_OPENGL);

	SDL_GLContext context = SDL_GL_CreateContext(window);

	glewExperimental = GL_TRUE;
	glewInit();
	// After this point, the GL function pointers should have been set up by GLEW so they can
	// be called!

	SDL_Log("OpenGL Version: %s", glGetString(GL_VERSION));
	SDL_Log("GLEW Version: %s", glewGetString(GLEW_VERSION));

	glGetError(); // Glew tends to generate GL_INVALID_ENUM (https://www.opengl.org/wiki/OpenGL_Loading_Library)

	glDebugMessageCallback(&PrintGLDebugMessage, nullptr);
	glDebugMessageControl(GL_DONT_CARE, GL_DONT_CARE, GL_DONT_CARE, 0, nullptr, GL_TRUE);

	/*

	  H+-----+G
	  /|    /|
	D+-----+C|
	 |E+---|-+F  + +Z
	 |/    |/   /
	 +-----+   /
	 A     B  + -Z


	*/

	const float triangle[] =
	{
		// X    Y    Z
		-1.0f, -1.0f,  1.0f,		//A
		 1.0f, -1.0f,  1.0f,		//B
		 1.0f,  1.0f,  1.0f,		//C
		-1.0f,  1.0f,  1.0f,	//D
		-1.0f, -1.0f, -1.0f,	//E
		 1.0f, -1.0f, -1.0f,		//F
		 1.0f,  1.0f, -1.0f,		//G
		-1.0f,  1.0f, -1.0f,		//H
	};

	enum
	{
		A, B, C, D, E, F, G, H
	};

	GLuint triangleBuffer;
	// Use glGenBuffers if glCreateBuffers crashes (OpenGL 4.5 not available)
	glCreateBuffers(1, &triangleBuffer);
	glBindBuffer(GL_ARRAY_BUFFER, triangleBuffer);
	glBufferData(GL_ARRAY_BUFFER, sizeof(triangle), triangle, GL_STATIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	// Fake an error by binding a bogus buffer!
	//glBindBuffer(GL_ARRAY_BUFFER, 84783);

	const int indices[] =
	{
		A, B, C,	A, C, D, //Front 
		B, F, G,	B, G, C, //Right
		E, A, D,	E, D, H, //Left
		F, E, H,	F, H, G, //Back
		A, E, B,	B, E, F, //Bottom
		C, H, D,	C, G, H, //Top
	};

	

	GLuint triangleIndexBuffer;
	glCreateBuffers(1, &triangleIndexBuffer);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, triangleIndexBuffer);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);

	const char* vertexShaderCode =
	{
		"#version 400\n" // This is the only line that requires a newline, all others do not need it!
		"in vec3 vertex;\n"
		"uniform mat4 transformMatrix = mat4(1.0);"
		"uniform mat4 viewMatrix = mat4(1.0);"
		"uniform mat4 projectionMatrix = mat4(1.0);"
		"void main() {\n"
		"  gl_Position = projectionMatrix * viewMatrix * transformMatrix * vec4(vertex, 1.0);\n"	//Rotation and positioning. Order of things matters
		"}\n"
	};

	GLuint vertexShader = LoadShader(GL_VERTEX_SHADER, vertexShaderCode);
	if (!vertexShader)
	{
		return 0;
	}

	const char* fragmentShaderCode =
	{
		"#version 400\n" // This is the only line that requires a newline, all others do not need it!
		"out vec4 colorRGBA;"
		"uniform vec4 objectColor = vec4(1.0, 0.0, 0.0, 1.0);"
		"void main() {"
		"  colorRGBA = objectColor;"
		"}"
	};

	GLuint fragmentShader = LoadShader(GL_FRAGMENT_SHADER, fragmentShaderCode);

	GLuint shaderProgram = glCreateProgram();
	glAttachShader(shaderProgram, vertexShader);
	glAttachShader(shaderProgram, fragmentShader);
	glLinkProgram(shaderProgram);
	GLint linkSuccess = GL_FALSE;
	glGetProgramiv(shaderProgram, GL_LINK_STATUS, &linkSuccess);
	if (linkSuccess == GL_FALSE)
	{
		char programLog[1024] = { 0 };
		glGetProgramInfoLog(shaderProgram, _ARRAYSIZE(programLog), nullptr, programLog);
		SDL_Log("Failed to link shaders: %s", programLog);
		__debugbreak();
		glDeleteProgram(shaderProgram);
		return 0;
	}

	GLuint vertArray;
	glGenVertexArrays(1, &vertArray);
	glBindVertexArray(vertArray);
	glEnableVertexAttribArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, triangleBuffer);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, triangleIndexBuffer);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);

	//Going into the Shader and get this value
	GLint objectColorUniform = glGetUniformLocation(shaderProgram, "objectColor");
	if (objectColorUniform == -1)
	{
		SDL_Log("Failed to find uniform!");
	}
	//Going into the Shader and get this value
	GLint transformMatrixUniform = glGetUniformLocation(shaderProgram, "transformMatrix");
	if (transformMatrixUniform == -1)
	{
		SDL_Log("Failed to find uniform!");
	}
	//Going into the Shader and get this value
	GLint viewMatrixUniform = glGetUniformLocation(shaderProgram, "viewMatrix");
	if (viewMatrixUniform == -1)
	{
		SDL_Log("Failed to find uniform!");
	}
	//Going into the Shader and get this value
	GLint projectionMatrixUniform = glGetUniformLocation(shaderProgram, "projectionMatrix");
	if (projectionMatrixUniform == -1)
	{
		SDL_Log("Failed to find uniform!");
	}
	//???
	float value = 0.0f;
	
	//Positioning of the triangle
	cml::matrix44f_c transformMatrix;
	transformMatrix.identity();
	//Range: -1 to 1 Called: "Clip Space"


	cml::vector3f cameraPosition(5.0f, 5.0f, 0.0f);
	
	float cameraPith = 0.0f, cameraYaw = 0.0f, cameraRoll = 0.0f;



	cml::matrix44f_c viewMatrix;
	viewMatrix.identity();
	//CAMERA POSITIONING
	//View Matrix, Eye position, Eye Direction
	//cml::matrix_look_at_RH(viewMatrix,	cameraPosition,
	//									cameraPosition + cameraDirection,
	//									cml::vector3f(0.0f, 1.0f,  0.0f));

	//OBJECT POSITIONING
	cml::matrix44f_c projectionMatrix;
	projectionMatrix.identity();
	//Setting up Perspectiove matrix
	//(matrix, field of view degrees, aspect ratio, near plane, far plane, z clip)
	cml::matrix_perspective_xfov_RH(projectionMatrix, 90.0f, 800.0f / 600.0f, 0.1f, 1000.0f, cml::z_clip_neg_one);



	bool done = false;
	while (!done)
	{
		SDL_Event event;
		while (SDL_PollEvent(&event))
		{
			if (event.type == SDL_WINDOWEVENT && event.window.event == SDL_WINDOWEVENT_CLOSE)
			{
				done = true;
				break;
			}
		}


		//Drawing things out
		glClearColor(0.0f, 1.0f, 0.0f, 1.0f);
		glClear(GL_COLOR_BUFFER_BIT);

		//Color changing
		if (value > 1.0f)
			value = 0.0f;
		else
			value += 0.01f;
		

		//cameraPith = value * 90.0 + 45.0f;

		cml::matrix44f_c cameraRotation;
		cml::matrix_rotation_euler(cameraRotation, cameraPith, cameraYaw, cameraRoll, cml::euler_order_xyz);
		cml::vector4f cameraForward(0.0f, 0.0f, -1.0f, 0.0f);	//Unit Vector? WTF? 
		
		cml::vector3f cameraDirection = (cameraRotation * cameraForward).subvector(3);

		cml::matrix_rotate_about_local_x(cameraRotation, 180.0f * value);

		//CAMERA POSITIONING
		//View Matrix, Eye position, Eye Direction
		cml::matrix_look_at_RH(viewMatrix, cameraPosition,
			cameraPosition + cameraDirection,
			cml::vector3f(0.0f, 1.0f, 0.0f));

		//Translation 
		//Rotation

		//Transfor matrix: Translation * Rotation 

		cml::matrix44f_c objectRotation;
		cml::matrix44f_c objectTranslation;
		objectRotation.identity();

		cml::matrix_rotate_about_local_y(objectRotation, 1.5f * value);
		cml::matrix_translation(objectTranslation, 0.1f, 0.5f, -5.0f);

		transformMatrix = objectTranslation * objectRotation;

		//Possitioning the object

		//LINES! Wireframe mode
		glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

		glUseProgram(shaderProgram);
		glBindVertexArray(vertArray);
		//glProgramUniform4f(shaderProgram, objectColorUniform, value, 0.0f, 1.0f, 1.0f);
		glProgramUniformMatrix4fv(shaderProgram, transformMatrixUniform, 1, GL_FALSE, transformMatrix.data());
		glProgramUniformMatrix4fv(shaderProgram, projectionMatrixUniform, 1, GL_FALSE, projectionMatrix.data());
		glProgramUniformMatrix4fv(shaderProgram, viewMatrixUniform, 1, GL_FALSE, viewMatrix.data());
		//glDrawArrays(GL_TRIANGLES, 0, _ARRAYSIZE(indices));
		glDrawElements(GL_TRIANGLES, _ARRAYSIZE(indices), GL_UNSIGNED_INT, 0);

		SDL_GL_SwapWindow(window);
	}

	SDL_GL_DeleteContext(context);
	SDL_DestroyWindow(window);
	SDL_Quit();

	return 0;
}