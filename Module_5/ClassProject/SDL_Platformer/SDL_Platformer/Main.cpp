#include <SDL.h>
#include <SDL_image.h>


SDL_Window* g_window = nullptr; 
SDL_Renderer* g_renderer = nullptr;

SDL_Surface* g_bmp = nullptr;
SDL_Texture* g_bmpTexture = nullptr;
SDL_Texture* g_bmpTexture2 = nullptr;


bool g_isRunning = false;

const int g_windowWidth = 640;
const int g_windowHeight = 480;

int main(int args, char* argv[])
{ 
	if (SDL_Init(SDL_INIT_VIDEO) < 0)
	{
		return -1;
	}
	//Creating the window
	g_window = SDL_CreateWindow("Platformer", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, g_windowWidth, g_windowHeight, SDL_WINDOW_SHOWN);
	//Creating renderer
	g_renderer = SDL_CreateRenderer(g_window, -1, SDL_RENDERER_ACCELERATED);
	const char* bmp_file = "Images/img-thing_bmp.bmp";
	g_bmp = SDL_LoadBMP(bmp_file);
	if (!g_bmp)
	{
		//"Failed to load some "
		SDL_Log("Failed to load bit map %s: %s", bmp_file, SDL_GetError());
	}

	g_bmpTexture = SDL_CreateTextureFromSurface(g_renderer, g_bmp);
	if (!g_bmpTexture)
	{
		//"Failed to load some "
		SDL_Log("Failed to load bit map %s: %s", g_bmpTexture, SDL_GetError());
	}
	//Delay
	//SDL_Delay(3000);



	g_isRunning = true;

	SDL_Event event; 



	while (g_isRunning)
	{
		//Event reciever
		SDL_PumpEvents();

		while (SDL_PollEvent(&event))
		{
			if (event.type == SDL_WINDOWEVENT)
			{
				if (event.window.event == SDL_WINDOWEVENT_CLOSE)
				{
					g_isRunning = false;
				}
			}
			else if (event.type == SDL_KEYDOWN)
			{
				if (event.key.keysym.sym == SDLK_q) //'q'
				{
					g_isRunning = false;
				}
			}
			else if (event.type == SDL_MOUSEBUTTONDOWN)
			{
				if (event.button.button == SDL_BUTTON_RIGHT)
				{
					g_isRunning = false;
				}
			}
		}

		//Usually you want to clear the screen first - when dealing with ACCELERATED renderer
		//Color
		SDL_SetRenderDrawColor(g_renderer, 0, 0, 0, 255);
		//Clearing screen
		SDL_RenderClear(g_renderer);
		
		
		//Texture?
		//Getting the size of the image
		SDL_Rect bmpRect;
		bmpRect.x = 10;
		bmpRect.y = 10;
		bmpRect.w = g_bmp->w;
		bmpRect.h = g_bmp->h;
		//Getting screen rect - never used it. 
		SDL_Rect screenRect;
		//Draw the texture - RenderCopy???? WTF?
		SDL_RenderCopy(g_renderer, g_bmpTexture, NULL, &bmpRect);
		//

		//Box - quick learn
		SDL_Rect box; 
		box.x = 10;
		box.y = 10;
		box.w = g_windowWidth - 20;
		box.h = g_windowHeight - 20;
		SDL_SetRenderDrawColor(g_renderer, 0, 255, 0, 255);
		SDL_RenderDrawRect(g_renderer, &box);
		
		
		
		//Drawing from back buffer
		SDL_RenderPresent(g_renderer);





	}

	//Cleaning up
	SDL_DestroyRenderer(g_renderer);
	SDL_DestroyWindow(g_window);
	SDL_Quit();

	return 0;
}