#include "GameObjectFactory.h"

//Tiles
#include "../Tiles/TileGrass.h"
#include "../Tiles/TileRoad.h"
#include "../Tiles/TileCastle.h"
#include "../Tiles/TileBuildable.h"
#include "../Tiles/TileSpawn.h"

//Components
#include "../Components/InteractibleComp.h"

//Structures
#include "../Structures/Structure.h"
#include "../Structures/TowerStructure.h"

#include "../Game_Field/GameField.h"

void GameObjectFactory::CreateGroundTile(char tileChar, GameField* pGrid, int posX, int posY)
{
	//To Rez: Hashtable? Should I use it for texture loadings?

	//Textures for tiles
	char* grassTextureLocation		= "Textures/Ground/Texture_Grass.bmp";
	char* groundTextureLocation		= "Textures/Ground/Texture_Road.bmp";
	char* castleTextureLocation		= "Textures/Ground/Texture_Castle.bmp";
	char* buildableTextureLocation	= "Textures/Ground/Texture_TowerBase.bmp";
	char* spawnTextureLocation		= "Textures/Ground/Texture_Spawn.bmp";


	int index = (posY * pGrid->GetSizeX()) + posX;
	
	switch (tileChar)
	{
	case 'X':
		pGrid->CreateTile(new TileGrass(posX, posY, grassTextureLocation, 0), index);
		break;
	case 'O':
		pGrid->CreateTile(new TileRoad(posX, posY, groundTextureLocation, 0), index);
		break;
	case 'C':
		pGrid->CreateTile(new TileCastle(posX, posY, castleTextureLocation, 0), index);
		break;
	case 'T':
		pGrid->CreateTile(new TileBuildable(posX, posY, buildableTextureLocation, 0), index);
		break;
	case 'S':
		pGrid->CreateTile(new TileSpawn(posX, posY, spawnTextureLocation, 0), index);
		break;
	default:
		break;
	}

    // [rez] 
    //Tile* pTile = nullptr;

    //switch (tileChar)
    //{
    //    case 'X':
    //        pTile = new Tile(posX, posY, grassTextureLocation, 0);
    //        pTile->AddRenderComponent(new RendererComp(0, pTextureLocation, x, y));
    //        break;

    //    case 'T':
    //        pTile = new TileBuildable(posX, posY, buildableTextureLocation, 0);
    //        pTile->AddRenderComponent(new RendererComp(0, pTextureLocation, x, y));
    //        pTile->AddInteractableComponent(new InteractibleComp(x, y, m_pMyRendererComp->GetRect()));
    //        break;
    //}

    //return pTile;

}

void GameObjectFactory::CreateStructure(InteractibleComp* pObjectBase)
{
	//Textures
	char* castleTextureLocation = "Textures/Structures/Texture_Structure.bmp";


	pObjectBase->PlaceStructure(new TowerStructure(pObjectBase, pObjectBase->GetPosX(), pObjectBase->GetPosY(), castleTextureLocation));
}