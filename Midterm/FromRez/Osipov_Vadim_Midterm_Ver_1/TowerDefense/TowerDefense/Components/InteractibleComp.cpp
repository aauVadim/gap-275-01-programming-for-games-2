#include "InteractibleComp.h"

#include "../SDL2/include/SDL_rect.h"
#include "../Factory/GameObjectFactory.h"


InteractibleComp::InteractibleComp(int posX, int posY, SDL_Rect* pRect)
	:Component(posX, posY)
	, m_pRect(pRect)
{
	AddToManager(posX, posY);
}
void InteractibleComp::AddToManager(int x, int y)
{
	EventHandler::GetInstance()->AddObject(this);
}

void InteractibleComp::ProcessInput(SDL_Point* pPoint)
{
	if (!m_pRect)
	{
		SDL_Log("InteractibleComp::ProcessInput(SDL_Point* pPoint) - m_pRect is NULL");
		return;
	}

	if (SDL_PointInRect(pPoint, m_pRect))
	{
		//Checking if we have a structure already 
		if (m_pStructure == nullptr)
			GameObjectFactory::GetInstance()->CreateStructure(this);
	}
}

