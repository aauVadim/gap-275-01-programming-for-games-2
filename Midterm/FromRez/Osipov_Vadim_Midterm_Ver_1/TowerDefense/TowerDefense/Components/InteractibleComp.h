//	InteractibleComp.h
//	Name: Vadim Osipov
//	Date: 10/17

#pragma once

#include "Component.h"

#include "../System/EventHandler.h"

class Structure;

//Determans if the object can be clicked on
class InteractibleComp : public Component<InteractibleComp>
{
	Structure* m_pStructure;
	SDL_Rect* m_pRect;

public: 
	InteractibleComp(int posX, int posY, SDL_Rect* pRect);

	virtual InteractibleComp* GetComponent() { return this; }
	virtual void AddToManager(int x, int y);

	void ProcessInput(SDL_Point* pPoint);


	//Placing the structure ontop of the tile
	void PlaceStructure(Structure* pNewStructure) { m_pStructure = pNewStructure; }
};