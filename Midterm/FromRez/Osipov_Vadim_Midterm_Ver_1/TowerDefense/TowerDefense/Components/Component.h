//	Component.h
//	Name: Vadim Osipov 
//	Date: 10/17

#pragma once

#include "../SDL2/include/SDL.h"

template <typename ObjectComponent>
class Component
{
protected:
	int m_posX, m_posY;
	SDL_Rect* m_pRect;

public: 
	
	Component(int posX, int posY) : m_posX(posX), m_posY(posY), m_pRect(nullptr) { }
	virtual ~Component() { delete m_pRect; }

	//Position getters
	int GetPosX() const { return m_posX; }
    int GetPosY() const { return m_posY; }

	SDL_Rect* GetRect() const { return m_pRect; }

	virtual void AddToManager(int x, int y) = 0;

	//To Rez: How can I get rid of the template?
	virtual ObjectComponent* GetComponent() = 0;
};