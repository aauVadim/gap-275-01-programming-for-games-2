#include "TileSpawn.h"

#include "../Renderer/Renderer.h"

TileSpawn::TileSpawn(int x, int y, char* pTextureLocation, int zOrder)
	: Tile()
	, m_pMyRendererComp(new RendererComp(0, pTextureLocation, x, y))
{

}
