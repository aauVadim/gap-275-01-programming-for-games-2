
#pragma once

#include "Tile.h"

#include "../Renderer/RendererComp.h"
#include "../Components/InteractibleComp.h"


//Inherits from: Tile, RenderComp
class TileBuildable : public Tile
{

	RendererComp* m_pMyRendererComp;
	InteractibleComp* m_pMyIntercactibleComp;

public:
	TileBuildable(int x, int y, char* pTextureLocation, int zOrder);

};