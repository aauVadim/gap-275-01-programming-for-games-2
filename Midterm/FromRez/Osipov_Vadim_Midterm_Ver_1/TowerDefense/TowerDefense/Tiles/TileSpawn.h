
#pragma once

#include "Tile.h"
#include "../Renderer/RendererComp.h"

//Inherits from: Tile, RenderComp
class TileSpawn : public Tile
{
	RendererComp* m_pMyRendererComp;
public:
	TileSpawn(int x, int y, char* pTextureLocation, int zOrder);
};