//	TileCastle.h
//	Name: Vadim Osipov 
//	Date: 10/17

#pragma once

#include "Tile.h"
#include "../Renderer//RendererComp.h"

//Inherits from: Tile, RenderComp
class TileCastle : public Tile
{
	RendererComp* m_pMyRendererComp;

public:
	TileCastle(int x, int y, char* pTextureLocation, int zOrder);

};