
#pragma once

#include "Tile.h"
#include "../Renderer//RendererComp.h"

//Inherits from: Tile, RenderComp
class TileRoad : public Tile
{
	RendererComp* m_pMyRendererComp;
public:
	TileRoad(int x, int y, char* pTextureLocation, int zOrder);
};