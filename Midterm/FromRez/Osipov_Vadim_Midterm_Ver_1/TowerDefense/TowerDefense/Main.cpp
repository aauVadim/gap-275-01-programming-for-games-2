#include <vld.h>

#include "SDL.h"

#include "Renderer\Renderer.h"
#include "System\EventHandler.h"
#include "Game_Field\GameField.h"
#include "World\World.h"

//	To Rez: 
//		Enemies: I'm thinking to have them to be an Object and Tower/s to be an observer - to know when to shoot at them.
//		Towers building is supposed to be limited. I'm not sure how to better limit it without resource system of some sort. 


int main(int args, char* argv[])
{
	if(!Renderer::GetInstance()->Initialize())
		SDL_Log("Main() - Cannot initialize Renderer.");

	SDL_Event event;

	//Initializing the world
	World::GetInstance()->Initialize();

	while (EventHandler::GetInstance()->GetIsAppRunning())
	{
		SDL_PumpEvents();
		while (SDL_PollEvent(&event))
		{
			EventHandler::GetInstance()->HandleEvent(event);
			Renderer::GetInstance()->UpdateScreen();
		}
	}

	SDL_Log("Main() - Exiting.");
	return 0;
}