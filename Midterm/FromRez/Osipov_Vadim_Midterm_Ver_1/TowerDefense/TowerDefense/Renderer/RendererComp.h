//	RendererComp.h
//	Name: Vadim Osipov 
//	Date: 10/15/15

#pragma once

#include "../SDL2_image/include/SDL_image.h"

#include "../Components/Component.h"

class RendererComp : public Component<RendererComp>
{
	int m_zOrder;

	SDL_Texture* m_pTexture;
	SDL_Surface* m_pSurface; 

public: 
	RendererComp(int zOrder, char* pTextureLocation, int posX, int posY);
    virtual ~RendererComp();

	//Has to be on the child
	virtual void AddToManager(int x, int y);
	

	virtual RendererComp* GetComponent() override { return this; }

	// Returns ZOrder of this object 
	int GetZOrder() { return m_zOrder; }
	//Returns Texture of this object
	SDL_Texture* GetTexture() { return m_pTexture; }

	//SDL_Rect* GetRect() { return m_pRect; }
	SDL_Rect* CreateRect(int x, int y);

public: 
	bool LoadTexture(char* textureLocation);
};