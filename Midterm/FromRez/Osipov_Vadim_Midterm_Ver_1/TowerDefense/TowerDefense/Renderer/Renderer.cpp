#include "../SDL2/include/SDL.h"

#include "Renderer.h"

//#include <deque>

Renderer::~Renderer()
{
	for (RendererComp* comp : m_renderableObjects)
	{
		delete comp;
	}
	m_renderableObjects.clear();

	//Cleaning Renderer Up
	SDL_DestroyRenderer(m_pRenderer);
	SDL_DestroyWindow(m_pWindow);
	SDL_Quit();
}

bool Renderer::Initialize()
{
	//Initializing Video 
	if (SDL_Init(SDL_INIT_VIDEO) < 0)
	{
		SDL_Log("Something went wrong in SDL_Init()");
		return false;
	}

	//Creating Windows - Window 
	m_pWindow = SDL_CreateWindow("Vadim Osipov - Tower Defense Game", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, 800, 600, SDL_WINDOW_SHOWN);
	//Creating Renderer
	m_pRenderer = SDL_CreateRenderer(m_pWindow, -1, SDL_RENDERER_ACCELERATED);
	//Sucsess
	SDL_Log("\nLoaded SDL_VIDEO - Sucsessfully.\n");
	return true; 
}

void Renderer::UpdateScreen()
{
	//Renderig the scene
	RenderScene();
	
}

void Renderer::AddObject(RendererComp* pObject)
{
	m_renderableObjects.push_back(pObject);
}

void Renderer::RenderScene()
{
	//Drawing first BG Color
	SDL_SetRenderDrawColor(m_pRenderer, 0, 13, 0, 255);

	//Clearing screen
	SDL_RenderClear(m_pRenderer);

	for (RendererComp* comp : m_renderableObjects)
	{
		SDL_RenderCopy(m_pRenderer, comp->GetTexture(), NULL, comp->GetRect());

		//Quick'n'dirty outline - might keep it
		SDL_Rect outlineRect; 
		SDL_Rect textureRect = *comp->GetRect();
		outlineRect.x = textureRect.x;
		outlineRect.y = textureRect.y;
		outlineRect.w = 40;
		outlineRect.h = 40;
		SDL_SetRenderDrawColor(m_pRenderer, 0, 0, 0, 255);
		SDL_RenderDrawRect(m_pRenderer, &outlineRect); 
	}

	//Presenting from back buffer
	SDL_RenderPresent(m_pRenderer);
}
