#include "../SDL2_image/include/SDL_image.h"

#include "RendererComp.h"
#include "Renderer.h"

RendererComp::RendererComp(int zOrder, char* pTextureLocation, int posX, int posY)
	: Component(posX, posY)
	, m_zOrder(zOrder)
	, m_pSurface(nullptr)
{
	if (!LoadTexture(pTextureLocation))
		SDL_Log("RendererComp(): Couldn't load texture for: %s", pTextureLocation);
	else
		AddToManager(posX, posY);
}

RendererComp::~RendererComp()
{
    // clean up here
}

bool RendererComp::LoadTexture(char* textureLocation)
{
	//Creating surface
	m_pSurface = SDL_LoadBMP(textureLocation);
	if (!m_pSurface)
	{
		SDL_Log("Couldn't load: %s", textureLocation);
		return false;
	}


	//Creating texture
	m_pTexture = SDL_CreateTextureFromSurface(Renderer::GetInstance()->GetRenderer(), m_pSurface);
	if (!m_pTexture)
	{
		SDL_Log("Failed to create a texture for: %s", textureLocation);
		return false;
	}

	//Sucsess 
	return true;
}


void RendererComp::AddToManager(int x, int y)
{
	CreateRect(x, y);
	Renderer::GetInstance()->AddObject(this);
}

SDL_Rect* RendererComp::CreateRect(int x, int y)
{
	//Creating new SDL_rect for us
    if (m_pRect)
        delete m_pRect;
	m_pRect = new SDL_Rect;

	//Setting up rect
	m_pRect->x = x * m_pSurface->w;
	m_pRect->y = y * m_pSurface->h;
	m_pRect->h = m_pSurface->h;
	m_pRect->w = m_pSurface->w;

	return m_pRect;
}