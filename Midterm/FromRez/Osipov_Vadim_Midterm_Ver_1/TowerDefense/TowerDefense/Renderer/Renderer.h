//	Renderer.h
//	Name: Vadim Osipov 
//	Date: 10/15/15

#pragma once

#include <deque>

#include "..\Utility\Singleton.h"

#include "RendererComp.h"

class Renderer : public Singleton<Renderer>
{
	//SDL Window variables
	SDL_Window* m_pWindow; 
	SDL_Renderer* m_pRenderer;

	std::deque<RendererComp*> m_renderableObjects;
public:
	
	bool Initialize();

	void UpdateScreen();

	SDL_Renderer* GetRenderer() { return m_pRenderer; }

	//Adding object to the queue
	void AddObject(RendererComp* pObject);

	//Removing object from the queue - OFF FOR NOW
	std::deque<RendererComp*> RemoveObject(RendererComp* pObject) { };
	
	//Will render the queue in Z-Order of objects 
	void RenderScene();

private:
	~Renderer();
	//Utility function to sort the queue. OFF FOR NOW 
	std::deque<RendererComp*> SortQueue(std::deque<RendererComp*> queue) { };
};