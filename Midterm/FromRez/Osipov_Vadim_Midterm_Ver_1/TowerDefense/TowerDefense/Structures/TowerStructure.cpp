
#include "TowerStructure.h"

TowerStructure::TowerStructure(InteractibleComp* pBase, int posX, int posY, char* pTextureLocation)
	:Structure(pBase, posX, posY)
	, m_pMyRendererComp(new RendererComp(1, pTextureLocation, m_posX, m_posY))
	//, m_pMyInteractibleComp(new InteractibleComp(m_posX, m_posY, m_pMyRendererComp->GetRect()))
	, m_pMyInteractibleComp(nullptr)
{
	SDL_Log("CREATED A STRUCTURE");
}