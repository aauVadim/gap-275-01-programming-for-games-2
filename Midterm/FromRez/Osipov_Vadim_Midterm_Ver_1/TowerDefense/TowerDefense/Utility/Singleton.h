//	Singleton.h
//	Name: Vadim Osipov
//	Date: 10/15/15

#pragma once

template <typename Object>
class Singleton
{
	static Object* m_pInstance;

public: 
	static Object* GetInstance() { if (!m_pInstance) m_pInstance = new Object ; return m_pInstance; };

protected:
	Singleton() { };
	virtual ~Singleton() { };
};

template <typename Object>
Object* Singleton<Object>::m_pInstance = nullptr;
