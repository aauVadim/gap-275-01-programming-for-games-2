//	World.cpp
//	Name: Vadim Osipov 
//	Date: 10/17/15

#include "World.h"

void World::Initialize()
{
	m_gridHeight = 15;
	m_gridWidth = 20;

	//Creatin the grid
	m_pGameField = new GameField(m_gridWidth, m_gridHeight);

	m_pGameField->Initialize();
}