
#include "EventHandler.h"

#include "../Components/InteractibleComp.h"


void EventHandler::HandleEvent(SDL_Event windowEvent)
{
	//Not cool
	if (m_pMousePoint)
		delete m_pMousePoint;
	//Not cool 
	m_pMousePoint = new SDL_Point;


	//Direct window callbacks - System? 
	if (windowEvent.type == SDL_WINDOWEVENT)
	{
		//Handling window closure
		if (windowEvent.window.event == SDL_WINDOWEVENT_CLOSE)
		{
			m_isAppRunning = false;
		}
	}
	else if (windowEvent.type == SDL_MOUSEBUTTONDOWN)
	{
		if (windowEvent.button.button == SDL_BUTTON_LEFT)
		{
			SDL_GetMouseState(&m_pMousePoint->x, &m_pMousePoint->y);
			NotifyInteractibles(m_pMousePoint);
		}
	}
}

void EventHandler::AddObject(InteractibleComp* pObject)
{
	m_interactibleObjects.push_back(pObject);
}

void EventHandler::NotifyInteractibles(SDL_Point* pPoint)
{
//	SDL_Log("Mouse Position: X: %i, Y: %i", m_pMousePoint->x, m_pMousePoint->y);
	for (InteractibleComp* comp : m_interactibleObjects)
	{
		comp->ProcessInput(pPoint);
	}
}