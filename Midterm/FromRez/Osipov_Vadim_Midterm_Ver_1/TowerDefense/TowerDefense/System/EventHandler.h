//	EventHandler.h
//	Name: Vadim Osipov 
//	Date: 10/15/15

#pragma once

#include "../SDL2/include/SDL.h"
#include "../Utility/Singleton.h"

//#include "../Components/InteractibleComp.h"
class InteractibleComp;
#include <deque>

//To Rez: Is this class doing to much? It intended to be just an event handler; but now its turning out to be Interactible Manager as well. 

class EventHandler : public Singleton<EventHandler>
{
	bool m_isAppRunning = true; 
	SDL_Point* m_pMousePoint;

	std::deque<InteractibleComp*> m_interactibleObjects; 

public:
	void AddObject(InteractibleComp* pObject);
	void HandleEvent(SDL_Event windowEvent);
	//Getter, nothinng more.
	bool GetIsAppRunning() { return m_isAppRunning; }
private:
	void NotifyInteractibles(SDL_Point* pPoint);
};