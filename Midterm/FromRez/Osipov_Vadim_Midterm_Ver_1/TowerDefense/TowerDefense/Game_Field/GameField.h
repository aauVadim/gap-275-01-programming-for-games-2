
#pragma once

#include "../Tiles/Tile.h"

class GameField
{
	//Size of the map
	int m_sizeX, m_sizeY;

	Tile** m_ppGameField;
	//Tile* m_pGameField; 
	char* m_tempCharArray; 
public:  // [rez] Default to private
	//TODO: FIXME: Returns char.
	char GetChar(int index) const { return m_tempCharArray[index]; }
	
	//Creating new tile
	void CreateTile(Tile* newTile, int index) { m_ppGameField[index] = newTile; }

	Tile* GetTile(int index) const { return m_ppGameField[index]; }

	//Size getters
	int GetSizeX() const { return m_sizeX; }
    int GetSizeY() const { return m_sizeY; }

	GameField(int sizeX, int sizeY);
	~GameField();
	void Initialize();

private:
	void LoadMap();
};