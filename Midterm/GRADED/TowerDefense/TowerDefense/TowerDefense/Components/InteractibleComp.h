//	InteractibleComp.h
//	Name: Vadim Osipov
//	Date: 10/17

#pragma once

#include "Component.h"

#include "../System/EventHandler.h"
#include "../SDL2/include/SDL.h"

class Structure;
class GamePiece;
#include <deque>

//Determans if the object can be clicked on
class InteractibleComp : public Component
{
	//Structure* m_pStructure;
	/// [josh] Component has a rectangle pointer that could be used?
	SDL_Rect* m_pRect;
	GamePiece* m_pMyGamePiece;

public: 
	InteractibleComp(int posX, int posY, SDL_Rect* pRect, GamePiece* pMyGamePiece);
	~InteractibleComp();

	virtual void AddToManager() override;

	void OnClick(SDL_Point* pPoint);


};