#include "InteractibleComp.h"

#include "../SDL2/include/SDL_rect.h"
#include "../Factory/GameObjectFactory.h"

#include "../Structures/Structure.h"

#include <assert.h>

InteractibleComp::InteractibleComp(int posX, int posY, SDL_Rect* pRect, GamePiece* pMyGamePiece)
	:Component(posX, posY)
	, m_pRect(pRect)
	, m_pMyGamePiece(pMyGamePiece)
{
	AddToManager();
}

InteractibleComp::~InteractibleComp()
{
	if (m_pRect)
		delete m_pRect;

	if (m_pMyGamePiece)
		delete m_pMyGamePiece;
}

void InteractibleComp::AddToManager()
{
	InputManager::GetInstance()->AddObject(this);
}

void InteractibleComp::OnClick(SDL_Point* pPoint)
{
	assert(m_pRect);

	if (!m_pRect)
	{
		SDL_Log("InteractibleComp::ProcessInput(SDL_Point* pPoint) - m_pRect is NULL");
		return;
	}

	if (SDL_PointInRect(pPoint, m_pRect))
	{
		m_pMyGamePiece->Execute();
	}
}

