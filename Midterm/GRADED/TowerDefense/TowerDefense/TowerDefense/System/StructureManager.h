//	StructureManager.cpp
//	Name: Vadim Osipov 
//	Date: 10/24

#pragma once

//Inherits
#include "../Utility/Singleton.h"
#include "../Base_Classes/Manager.h"

//SDL
#include "../SDL2/include/SDL.h"

//Platform
#include <deque>
#include <map>
#include "windows.h"

class ArcherTowerBehavior;
class ProjectileBehavior;

class StructureManager : public Singleton<StructureManager>, public Manager
{
	//Archer towers
	std::deque<ArcherTowerBehavior*> m_archerTowers;
	//Storing active projectiles to move them on update
	std::map<DWORD, ProjectileBehavior*> m_activeProjectiles; 

	GUID* m_myGUID = nullptr;

public:
	void AddStructure(ArcherTowerBehavior* pStructure);

	void AddProjectile(ProjectileBehavior* pProjectile); 
	void RemoveProjectile(ProjectileBehavior* pProjectile);

	//timePast - defaulted to 0, we are not using it so far in this manager
	virtual void Update(Uint32 timePast = 0) override;

	virtual void OnInitialize() override;
	virtual void OnShutdown() override;
	virtual void DeleteGamePiece(GamePiece* pGamePiece) override;
	virtual GUID* GetManagerGUID() override;

	//DEBUG! 
	void StructuresShoot(SDL_Point* pPoint);

private:
	void UpdateProjectiles();
	~StructureManager();

};