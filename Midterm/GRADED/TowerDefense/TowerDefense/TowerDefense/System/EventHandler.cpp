
#include "EventHandler.h"

#include "../Components/InteractibleComp.h"

#include "EnemyManager.h"

#include "InputManager.h"


#include "../World/World.h"

void EventHandler::HandleEvent(SDL_Event windowEvent)
{
	//Can be used to temporary store points of interest on the screen 
	/// [josh] You don't need to allocate this on the heap, it would be faster
	/// on the stack.
	SDL_Point* pTempMousePoint = new SDL_Point;

	
	//Direct window callbacks - System? 
	if (windowEvent.type == SDL_WINDOWEVENT)
	{
		//Handling window closure
		if (windowEvent.window.event == SDL_WINDOWEVENT_CLOSE)
		{
			m_isAppRunning = false;
			World::GetInstance()->OnShutdown();
		}
	}
	else if (windowEvent.type == SDL_MOUSEBUTTONDOWN)
	{
		if (windowEvent.button.button == SDL_BUTTON_LEFT)
		{
			/// [josh] You can get at the mouse position via windowEvent.button.x and .y
			SDL_GetMouseState(&pTempMousePoint->x, &pTempMousePoint->y);
			InputManager::GetInstance()->NotifyInteractibles(pTempMousePoint);
		}
	}
	//Used purely for debuggin'
	else if (windowEvent.type == SDL_KEYDOWN)
	{
		//if (windowEvent.key.keysym.sym == SDLK_SPACE)
		//{
		//	SDL_Log("Space bar pressed. Time sinse game started: %d", m_timePast);
		//}
		////Can be used for debugging things
		//else if (windowEvent.key.keysym.sym == SDLK_q)
		//{
		//}
	}

	delete pTempMousePoint;
	pTempMousePoint = nullptr;
}

void EventHandler::Initialize()
{
	m_startTime = SDL_GetTicks();
	m_timePast = 0;
}
