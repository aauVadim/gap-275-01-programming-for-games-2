


#include "EnemyManager.h"

#include "../SDL2/include/SDL.h"

#include <assert.h>

#include <iostream>
#include <deque>
#include <algorithm>

#include "../Factory/GameObjectFactory.h"

#include "../Enemies/EnemyBehavior.h"

#include "../Tiles/Behaviors/TileRoadBehavior.h"

#include "../Utility/UtilityFunctions.h"

EnemyManager::~EnemyManager()
{
	for (TileRoadBehavior* pTileRoad : m_roadTiles)
		delete pTileRoad;
	m_roadTiles.clear();

	for (Tile* pSpawn : m_spawnLocations)
		delete pSpawn;
	m_spawnLocations.clear();

	m_enemies.clear();

	if (m_myGUID)
		delete m_myGUID;
	m_myGUID = nullptr;
}

void EnemyManager::AddSpawnPoint(Tile* pSpawnPoint)
{
	assert(pSpawnPoint);

	m_spawnLocations.push_back(pSpawnPoint);
}

void EnemyManager::SpawnEnemy(Uint32 timePast)
{
	if (CanAct(m_individualEnemySpawnInterval, m_lastSpawnTime))
	{
		//If you spawn this second - do not spawn it again
		m_lastSpawnTime = timePast;

		SDL_Log("SPAWNING INDIVIDUAL ENEMY. Time Passed: %d", timePast);
		//Choose random 
		int dequeSize = m_spawnLocations.size();
		int random = (rand() % dequeSize);
		GameObjectFactory::GetInstance()->CreateEnemy(m_spawnLocations.at(random));
	}

}

void EnemyManager::MoveEnemies()
{
	auto it = m_enemies.begin();
	while (it != m_enemies.end())
	{
		auto tempIt = it;
		++it;
		tempIt->second->Execute();
	}
}

void EnemyManager::AddEnemy(EnemyBehavior* pEnemy)
{
	assert(pEnemy);
	m_enemies[pEnemy->GetGamePiece()->GetGUID().Data1] = pEnemy;
}


void EnemyManager::AddRoadBeahvior(TileRoadBehavior* pRoadTile)
{
	//Addin a tile
	m_roadTiles.push_back(pRoadTile);
}

void EnemyManager::Update(Uint32 timePast)
{
	SpawnEnemy(timePast);
	MoveEnemies();
	CheckPoints();
}
//Warning: n^2 Function
//Basic collision function - Checks if enemy point matches with road tile point
void EnemyManager::CheckPoints()
{
	auto it = m_enemies.begin(); 

	//for (std::map<DWORD, EnemyBehavior*>::iterator it = m_enemies.begin(); it != m_enemies.end(); ++it)
	while (it != m_enemies.end())
	{
		auto tempIt = it;
		++it;

		SDL_Rect* pEnemyRect = tempIt->second->GetBaseRect();

		for (TileRoadBehavior* pRoadTile : m_roadTiles)
		{
			SDL_Rect* pTileRect = pRoadTile->GetBaseRect();

			if ((pTileRect->x == pEnemyRect->x) && (pTileRect->y == pEnemyRect->y))
			{
				tempIt->second->SetMovingDirection(pRoadTile->GetTileDirection());
			}
		}
	}
}

void EnemyManager::RemoveEnemy(EnemyBehavior* pEnemy)
{
	m_enemies.erase(pEnemy->GetGamePiece()->GetGUID().Data1);
}

void EnemyManager::OnInitialize()
{
	//Add yourself to the world 
	World::GetInstance()->AddManager((Manager*)this);

}
void EnemyManager::OnShutdown()
{
	//Remove yourself from the world
	World::GetInstance()->RemoveManager((Manager*)this);

}
void EnemyManager::DeleteGamePiece(GamePiece* pGamePiece)
{
	//Delete GamePiece you have
}

GUID* EnemyManager::GetManagerGUID()
{
	if (m_myGUID == nullptr)
		m_myGUID = &CreateGIUD();

	return m_myGUID;
}

