//	InputManager.cpp
//	Name: Vadim Osipov
//	Date: 10/19/15

#pragma once

#include "../Utility/Singleton.h"
#include "../Base_Classes/Manager.h"

#include "../SDL2/include/SDL.h"
#include "windows.h"

#include <deque>
class InteractibleComp;

class InputManager : public Singleton <InputManager>, public Manager
{
	//Storing all of the components that care for input
    // [rez] Good.
	std::deque<InteractibleComp*> m_interactibleObjects;

	GUID* m_myGUID;

public:

	void AddObject(InteractibleComp* pObject);
	void RemoveObject(InteractibleComp* pObject);

	//Blank - Currently not using
	virtual void Update(Uint32 timePast = 0) override { }

	virtual void OnInitialize() override;
	virtual void OnShutdown() override;
	virtual void DeleteGamePiece(GamePiece* pGamePiece) override;
	virtual GUID* GetManagerGUID() override;

	void NotifyInteractibles(SDL_Point* pPoint);
private: 
	~InputManager();
};