//	ObserverManager.cpp
//	Name: Vadim Osipov 
//	Date: 10/25

#include "ObserverManager.h"

#include "../Base_Classes/Observable.h"
#include "../Base_Classes/Observer.h"

#include <assert.h>
#include "../Utility/UtilityFunctions.h"

#include "../World/World.h"

ObserverManager::~ObserverManager()
{
	m_observables.clear();
	m_observers.clear();

	if (m_myGUID)
		delete m_myGUID;
	m_myGUID = nullptr;
}

void ObserverManager::AddObserver(Observer* pObserver)
{
	assert(pObserver);
	m_observers[pObserver->GetGUID().Data1] = pObserver;
}
void ObserverManager::RemoveObserver(Observer* pObserver)
{
	assert(pObserver);
	m_observers.erase(pObserver->GetGUID().Data1);
}
void ObserverManager::AddObservable(Observable* pObservable)
{
	assert(pObservable);
	m_observables[pObservable->GetGUID().Data1] = pObservable;
}
void ObserverManager::RemoveObservable(Observable* pObservable)
{
	assert(pObservable);
	m_observables.erase(pObservable->GetGUID().Data1);
}

void ObserverManager::GetUpdates(Observable* pObservable)
{
	//Updating Observers
	//for (std::map<DWORD, Observer*>::iterator it = m_observers.begin(); it != m_observers.end(); ++it)
	auto it = m_observers.begin();
	while (it != m_observers.end())
	{
		auto tempIt = it;
		++it;

		if (pObservable->IsDestroyed())
			break;
		
		tempIt->second->Update(pObservable);
	}
}


void ObserverManager::OnInitialize()
{
	//Add yourself to the world 
	World::GetInstance()->AddManager((Manager*)this);
}
void ObserverManager::OnShutdown()
{
	//Remove yourself from the world
	World::GetInstance()->RemoveManager((Manager*)this);
}
void ObserverManager::DeleteGamePiece(GamePiece* pGamePiece)
{
	//Delete GamePiece you have
}

GUID* ObserverManager::GetManagerGUID()
{
	if (m_myGUID == nullptr)
		m_myGUID = &CreateGIUD();

	return m_myGUID;
}