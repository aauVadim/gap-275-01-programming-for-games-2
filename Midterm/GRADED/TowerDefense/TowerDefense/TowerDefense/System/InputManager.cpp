//	InputManager.cpp
//	Name: Vadim Osipov
//	Date: 10/19/15

#include "InputManager.h"

#include "../World/World.h"

#include <assert.h>
#include "../Components/InteractibleComp.h"

#include "../System/StructureManager.h"
#include "../Utility/UtilityFunctions.h"


InputManager::~InputManager()
{
	for (InteractibleComp* pComp : m_interactibleObjects)
		delete pComp;
	m_interactibleObjects.clear();

	if (m_myGUID)
		delete m_myGUID;
	m_myGUID = nullptr;
}

void InputManager::AddObject(InteractibleComp* pObject)
{
	m_interactibleObjects.push_back(pObject);
}
void InputManager::RemoveObject(InteractibleComp* pObject)
{

}

/// [josh] pPoint should be marked const.  You can also make it a reference
/// and avoid the need for the assertion.
void InputManager::NotifyInteractibles(SDL_Point* pPoint)
{
	//Making sure that the point is not null
	assert(pPoint);

	//	SDL_Log("Mouse Position: X: %i, Y: %i", m_pMousePoint->x, m_pMousePoint->y);
	for (InteractibleComp* pComp : m_interactibleObjects)
	{
		pComp->OnClick(pPoint);
	}
}

void InputManager::OnInitialize()
{
	//Add yourself to the world 
	World::GetInstance()->AddManager((Manager*)this);

}
void InputManager::OnShutdown()
{
	//Remove yourself from the world
	World::GetInstance()->RemoveManager((Manager*)this);
}
void InputManager::DeleteGamePiece(GamePiece* pGamePiece)
{
	//Delete GamePiece you have
}

GUID* InputManager::GetManagerGUID()
{
	if (m_myGUID == nullptr)
		m_myGUID = &CreateGIUD();

	return m_myGUID;
}
