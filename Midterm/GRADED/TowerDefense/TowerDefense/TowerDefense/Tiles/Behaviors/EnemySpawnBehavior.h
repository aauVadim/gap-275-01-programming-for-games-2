//	EnemySpawnBehavior.h
//	Name: Vadim Osipov 
//	Date: 10/20

#pragma once

#include "../../Base_Classes/Behavior.h"

class GamePiece;

class EnemySpawnBehavior : public Behavior
{
public: 

	EnemySpawnBehavior(GamePiece* pMyBase);
	~EnemySpawnBehavior() { };

private:
	void AddToManager();
};