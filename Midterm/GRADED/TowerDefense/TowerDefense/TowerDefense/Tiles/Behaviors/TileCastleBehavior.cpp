#include "TileCastleBehavior.h"

#include "../../System/EnemyManager.h"

#include "../../World/World.h"

TileCastleBehavior::TileCastleBehavior(GamePiece* pMyBase, int posX, int posY, Direction myDirection)
	: Behavior(pMyBase)
	, m_myDirection(myDirection)
{

}

void TileCastleBehavior::OnEnterCenterOfTile(GamePiece* pPiece)
{

}

void TileCastleBehavior::GetPosition(Uint32& x, Uint32& y)
{
	Uint32 tempX, tempY;
	m_pMyBase->GetRectPosition(tempX, tempY);
	x = tempX;
	y = tempY;
}