//	Projectile.h
//	Name: Vadim Osipov 
//	Date: 10/24

#include "Projectile.h"
#include "../../Renderer/RendererComp.h"
#include "../../Base_Classes/GamePiece.h"

#include "../../System/StructureManager.h"
#include "../../System/ObserverManager.h"

#include "../../Base_Classes/Observable.h"


ProjectileBehavior::ProjectileBehavior(SDL_Point* pTarget, GamePiece* myBase, float lerpPercent)
	: Behavior(myBase)
	, m_lerpPercent(lerpPercent)
	, m_pTarget(pTarget)
	, m_shouldDestroy(false)
{
	OnSpawn();
}
ProjectileBehavior::~ProjectileBehavior()
{
	if (m_pTarget)
		delete m_pTarget;
}
void ProjectileBehavior::Execute()
{
	m_pMyBase->LerpTo(m_pTarget, m_lerpPercent);
}

//Observer - pure virtuals
void ProjectileBehavior::OnSpawn()
{
	StructureManager::GetInstance()->AddProjectile(this);
	ObserverManager::GetInstance()->AddObserver((Observer*)this);
}
void ProjectileBehavior::OnDestroy()
{
	//Call base to destroy itself 
	m_pMyBase->DestroyItself();
	
	ObserverManager::GetInstance()->RemoveObserver((Observer*)this);
	StructureManager::GetInstance()->RemoveProjectile(this);

	delete this;
}

void ProjectileBehavior::Update(Observable* pObservable)
{
	//Check collision
	SDL_Point* myCenterPoint = new SDL_Point;

	myCenterPoint->x = m_pMyBase->GetRect()->x + m_pMyBase->GetRect()->w / 2;
	myCenterPoint->y = m_pMyBase->GetRect()->y + m_pMyBase->GetRect()->h / 2;

	if (SDL_PointInRect(myCenterPoint, pObservable->GetThisRect()))
	{
		pObservable->ReceiveMessage(-50);
		OnDestroy();
	}
	//TODO: Apply damage 
	
}

const GUID ProjectileBehavior::GetGUID()
{
	return m_pMyBase->GetGUID();
}
