#include "SDL.h"
#include "vld.h"

#include "Renderer\Renderer.h"
#include "System\EventHandler.h"
#include "Game_Field\GameField.h"
#include "World\World.h"

#include "System\EnemyManager.h"
#include "System\StructureManager.h"

#include <iostream>
#include <time.h>

//---------------------------------------------------------------------------------------------------------------------
//	To Rez and Josh: 
//		- I have shot myself in the leg trying to chasse a perfect code design for this game. I have not delivered a lot of things I wanted and ended up using same pattern over and 
//		over again. Which I'm sad for :( There is a lot of things I should have done differentely and if I had enough time I'd surely make it better.
//
//		Main Idea is that I have GamePiece base class for all the things(Tile, Enemy, Tower) in the game and I have corresponding Singleton Managers for them;
//		Each of GamePieces has to add itseld to its managers.
//		
//---------------------------------------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------------------------------------
//	This is what I have so far on VLD. I have such shitty architecture that things just go sideways... 
//	I do understand that this will affect my grade. I had no time to fix it between yesterday and today. 
//	for some reason I'm not deleting Components properly and I cannot understand how to do it right. 
//
//---------------------------------------------------------------------------------------------------------------------
//	Visual Leak Detector detected 194 memory leaks(10512 bytes).
//	Largest number used : 113988 bytes.
//	Total allocations : 120732 bytes.
//	Visual Leak Detector is now exiting.
//	The program '[7820] TowerDefense.exe' has exited with code 0 (0x0).
//---------------------------------------------------------------------------------------------------------------------

// [rez] Grade: B+
//     - You have a pretty nice folder structure.
//     - Your files for components are spread all over the place, but it actually appears that they're 
//       valid on any type of GamePiece.  Is that correct?  If so, I would consider putting them into 
//       a single folder so you can combine them in interesting ways.  If they're not mixable, then you 
//       should strive to make them so.
//     - Looking a bit deeper, I think some of these behaviors might be able to be made more generic, 
//       but the system looks good.
//     - It seems like you tried to make you Observer and Observable nice and generic, but they ended 
//       up being rather specific.  I think that's the biggest problem with the architecture; you tend 
//       to let bad things sneak in.  Overall, your ideas are sound.

/// [josh] Grade: B-
/// - Code and project organized well.
/// - Renderables aren't removed when the object is destroyed. :[
/// - A number of places pass by non-const pointer where they should!
/// - Using override keyword and good attempts at incorporating assert()

int main(int args, char* argv[])
{
	//Seeding random;
	srand(unsigned int(time(NULL)));
	
	SDL_Event event;

	//Initializing the world
	World::GetInstance()->Initialize();

	Uint64 frequency = SDL_GetPerformanceFrequency();
	Uint64 lastCounter = SDL_GetPerformanceCounter();

	float accumTimesinceLastFrameChange = 0.0f;

	//Limiting to 60 frames - 33.3 Mileseconds - I hope my math was right.
	float timeBetweenFrames = 33.3f;

	Uint32 startTime = SDL_GetTicks();
	//So - the AppRunning bool was suppesed to be used to stop the gameplay and bring the Game Over screen up. 
	//due to running out of time - I had to disable that.
	while (EventHandler::GetInstance()->GetIsAppRunning())
	{
		//when 5 enemies make it to the end - kill the game
		if (!World::GetInstance()->IsGameOver())
		{
			Uint64 currentCounter = SDL_GetPerformanceCounter();
			float delta = (currentCounter - lastCounter) / static_cast<float>(frequency)* 1000;
			lastCounter = currentCounter;

			accumTimesinceLastFrameChange += delta;

			/// [josh] Why use SDL_GetTicks when you have delta available?
			/// Its not common to see performance counters and ticks used.
			Uint32 timePast = (SDL_GetTicks() - startTime) / 1000;

			//Limiting time
			if (accumTimesinceLastFrameChange > timeBetweenFrames)
			{
				//Updating world managers
				World::GetInstance()->Update(timePast);

				//Getting events
				SDL_PumpEvents();
				while (SDL_PollEvent(&event)/* && !World::GetInstance()->IsGameOver()*/)
				{
					EventHandler::GetInstance()->HandleEvent(event);
				}
				//Setting it back
				accumTimesinceLastFrameChange = 0.0f;
			}
		}
		else
		{
			SDL_Log("Exiting the game");
			break;
		}
	}

	SDL_Log("GAME OVER - Do things");
	return 0;
}