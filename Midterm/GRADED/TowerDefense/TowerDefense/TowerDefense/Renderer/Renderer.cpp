#include "../SDL2/include/SDL.h"

#include "Renderer.h"
#include "../Utility/UtilityFunctions.h"
//#include <deque>


Renderer::~Renderer()
{
	for (RendererComp* pComp : m_renderableObjects)
	{
		delete pComp;
	}
	m_renderableObjects.clear();

	//Cleaning Renderer Up
	SDL_DestroyRenderer(m_pRenderer);
	SDL_DestroyWindow(m_pWindow);
	SDL_Quit();
}

bool Renderer::Initialize()
{
	//Initializing Video 
	if (SDL_Init(SDL_INIT_VIDEO) < 0)
	{
		SDL_Log("Something went wrong in SDL_Init()");
		return false;
	}

	//Creating Windows - Window 
	m_pWindow = SDL_CreateWindow("Vadim Osipov - Tower Defense Game", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, 800, 600, SDL_WINDOW_SHOWN | SDL_WINDOW_INPUT_FOCUS);
	//Creating Renderer - VSYNC on
	m_pRenderer = SDL_CreateRenderer(m_pWindow, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
	//Sucsess
	SDL_Log("\nLoaded SDL_VIDEO - Sucsessfully.\n");
	return true; 
}

void Renderer::Update(Uint32 timePast)
{
	//Renderig the scene
	RenderScene();
}

void Renderer::AddObject(RendererComp* pObject)
{
	m_renderableObjects.push_back(pObject);
}

void Renderer::RenderScene()
{
	//Drawing first BG Color
	SDL_SetRenderDrawColor(m_pRenderer, 0, 13, 0, 255);

	//Clearing screen
	SDL_RenderClear(m_pRenderer);

	for (RendererComp* comp : m_renderableObjects)
	{
		SDL_RenderCopy(m_pRenderer, comp->GetTexture(), NULL, comp->GetRect());

		////Quick'n'dirty outline - might keep it
		//SDL_Rect outlineRect; 
		//SDL_Rect textureRect = *comp->GetRect();
		//outlineRect.x = textureRect.x;
		//outlineRect.y = textureRect.y;
		//outlineRect.w = 40;
		//outlineRect.h = 40;
		//SDL_SetRenderDrawColor(m_pRenderer, 0, 0, 0, 255);
		//SDL_RenderDrawRect(m_pRenderer, &outlineRect); 
	}

	//Presenting from back buffer
	SDL_RenderPresent(m_pRenderer);
}

void Renderer::OnInitialize()
{
	//Add yourself to the world 
	World::GetInstance()->AddManager((Manager*)this);
}
void Renderer::OnShutdown()
{
	//Remove yourself from the world
	World::GetInstance()->RemoveManager((Manager*)this);
}
void Renderer::DeleteGamePiece(GamePiece* pGamePiece)
{
	//Delete GamePiece you have
}

GUID* Renderer::GetManagerGUID()
{
	/// [josh] This is bad because you're returning the address of a temporary variable.
	/// What that means is that when CreateGUID returns, it temporarily creates a variable (behind the scenes)
	/// for its return value as it is by-value.  You can grab the address of it, BUT, that temporary variable could
	/// be destroyed at any time and you'd be left pointing to garbage.
	/// The compiler usually catches this and issues a warning.  Not sure why its not happening here.
	if (m_myGUID == nullptr)
		m_myGUID = &CreateGIUD();

	return m_myGUID;
}