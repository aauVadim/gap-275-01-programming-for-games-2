
#pragma once

#include "../World/World.h"
#include "../SDL2/include/SDL.h"
#include "windows.h"


static bool CanAct(Uint32 actRate, Uint32& outLastActTime)
{
	Uint32 time = World::GetInstance()->GetTimePast();

	if ((time > 0) && ((time % actRate) == 0) && (time != outLastActTime))
	{
		outLastActTime = time;
		return true;
	}

	return false;
}

//GUID - Using to store in the std::map
static GUID CreateGIUD()
{
	GUID objectID;
	CoCreateGuid(&objectID);
	//SDL_Log("Created object ID: %i ", objectID.Data1);
	return objectID;
}
