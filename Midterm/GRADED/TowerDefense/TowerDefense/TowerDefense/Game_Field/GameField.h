
#pragma once

#include "../Tiles/Tile.h"

class GameField
{
	//Size of the map
	int m_sizeX, m_sizeY;

	GamePiece** m_ppGameField;
	//Tile* m_pGameField; 
	char* m_tempCharArray; 
public: 
	//TODO: FIXME: Returns char.
	char GetChar(int index) { return m_tempCharArray[index]; }
	
	//Creating new tile
	void CreateTile(Tile* newTile, int index) { m_ppGameField[index] = newTile; }

	GamePiece* GetTile(int index) const { return m_ppGameField[index]; }

	//Size getters
	int GetSizeX() { return m_sizeX; }
	int GetSizeY() { return m_sizeY; }

	GameField(int sizeX, int sizeY);
	~GameField();
	void Initialize();

	void ShutDown();

private:
	void LoadMap();
};