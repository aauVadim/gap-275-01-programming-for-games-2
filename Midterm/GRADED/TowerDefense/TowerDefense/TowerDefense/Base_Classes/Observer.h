//	Observer.h
//	Name: Vadim Osipov 
//	Date: 10/25

#pragma once

#include "windows.h"

class Observable;

//---------------------------------------------------------------------------------------------------------------------
//	My understanding of Observer concept
//		- Observes all of the Observalbe objects that have singed up
//---------------------------------------------------------------------------------------------------------------------
class Observer
{

public: 
	virtual ~Observer() { };
	
	virtual void OnSpawn() = 0;
	virtual void OnDestroy() = 0;

	virtual void Update(Observable* pObservable) = 0;
	//Must have
	virtual const GUID GetGUID() = 0;
};