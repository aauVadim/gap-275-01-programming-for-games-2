//	GamePiece.cpp
//	Name: Vadim Osipov 
//	Date: 10/19

#include "GamePiece.h"

#include "../Renderer/RendererComp.h"
#include "../Components/Component.h"
#include "Behavior.h"

#include "windows.h"
#include <assert.h>


GamePiece::GamePiece(RendererComp* pRenderer, GUID myID)
	: m_pMyRendererComp(pRenderer)
	, m_myID(myID)
{

}

GamePiece::~GamePiece()
{
	delete m_pMyRendererComp;
	m_pMyRendererComp = nullptr;

	m_MyComponents.clear();

	m_pMyBehaviors.clear();
}

void GamePiece::AddComponent(Component* pComp)
{
	m_MyComponents.push_back(pComp);
}

void GamePiece::RemoveComponent(Component* pComp)
{
	//
}

void GamePiece::GetRectPosition(Uint32& x, Uint32& y)
{
	x = m_pMyRendererComp->GetPosX();
	y = m_pMyRendererComp->GetPosY();
}

//Sets new position on renderer Rect
void GamePiece::MoveMe(int plusPosition, Direction direction)
{
	//Getting rect from our Renderer component
	SDL_Rect* ourRect = m_pMyRendererComp->GetRect();

	if (direction == Direction::k_south)
		ourRect->y += plusPosition;
	else if (direction == Direction::k_west)
		ourRect->x += plusPosition;
	else if (direction == Direction::k_east)
		ourRect->x -= plusPosition;
	else if (direction == Direction::k_north)
		ourRect->y -= plusPosition;
	
	m_pMyRendererComp->SetRect(ourRect);
}

void GamePiece::DestroyItself()
{
	m_pMyRendererComp->ShutDown();
	m_pMyRendererComp = nullptr;

	for (Component* pComp : m_MyComponents)
		delete pComp;
	m_MyComponents.clear();

	for (Behavior* pBehav : m_pMyBehaviors)
		delete pBehav;
	m_pMyBehaviors.clear();

}

void GamePiece::LerpTo(SDL_Point* pPointTo, float percent)
{

	SDL_Rect* ourRect = m_pMyRendererComp->GetRect();
	
	assert(pPointTo && ourRect); 
	
	//Lerp X 
	ourRect->x = (ourRect->x + percent * (pPointTo->x - ourRect->x));
	//Lerp Y
	ourRect->y = (ourRect->y + percent * (pPointTo->y - ourRect->y));


	m_pMyRendererComp->SetRect(ourRect);
}