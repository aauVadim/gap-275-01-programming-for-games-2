//	TileBehavior.
//	Name: Vadim Osipov 
//	Date: 10/20

#pragma once

#include "../SDL2/include/SDL.h"

#include "GamePiece.h"

class Behavior
{
protected:
	GamePiece* m_pMyBase;

public:
	Behavior(GamePiece* pMyBase) : m_pMyBase(pMyBase) { }
	virtual ~Behavior() { if (m_pMyBase) delete m_pMyBase; };

	virtual void Execute() { };
	//Returns its base class
	GamePiece* GetGamePiece() const { return m_pMyBase; }
	//Rect of a thing
	SDL_Rect* GetBaseRect() const;
	//Center point 
	SDL_Point* GetMyCenterPoint() const;

	//Returns nullptr by default - Road behaviors should not return
	virtual Behavior* GetBehavior() { return nullptr; }

	//Utility
	double GetDistance(SDL_Point* pThis, SDL_Point* pThat);
};