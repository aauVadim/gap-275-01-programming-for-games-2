
#include "Behavior.h"
#include "GamePiece.h"

#include "../SDL2/include/SDL.h"

#include <math.h>

SDL_Rect* Behavior::GetBaseRect() const
{
	return m_pMyBase->GetRect();
}

SDL_Point* Behavior::GetMyCenterPoint() const
{
	SDL_Point* pNewPoint = nullptr;
	SDL_Rect* pTempRect = m_pMyBase->GetRect();

	pNewPoint->y = (pTempRect->y + pTempRect->h) / 2;
	pNewPoint->x = (pTempRect->x + pTempRect->w) / 2;
	delete pTempRect;
	return pNewPoint;
}

double Behavior::GetDistance(SDL_Point* pThis, SDL_Point* pThat)
{
	double distanceX = (double)((pThat->x - pThis->x) * (pThat->x - pThis->x));
	double distanceY = (double)((pThat->y - pThis->y) * (pThat->y - pThis->y));

	double distance = sqrt(distanceX + distanceY);

	return distance;
}
