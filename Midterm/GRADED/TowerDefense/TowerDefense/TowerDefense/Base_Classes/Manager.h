//	Manager.h
//	Name: Vadim Osipov
//	Date: 10/24

#pragma once

#include "../SDL2/include/SDL.h"

#include "windows.h"

class GamePiece;

class Manager
{
public:
	virtual ~Manager() { }

	virtual void OnInitialize() = 0;
	virtual void Update(Uint32 timePast = 0) = 0;
	virtual void OnShutdown() = 0;
	virtual void DeleteGamePiece(GamePiece* pGamePiece) = 0;
	/// [josh] GUID is a simple struct that you should be able to pass around by value.
	virtual GUID* GetManagerGUID() = 0;
};