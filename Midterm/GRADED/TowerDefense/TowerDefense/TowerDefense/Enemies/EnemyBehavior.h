//	EnemyBehavior.h
//	Name: Vadim Osipov
//	Date: 10/20

#pragma once

#include "../Base_Classes/GamePiece.h"

//Inherits (is-a)
#include "../Base_Classes/Behavior.h"
#include "../Base_Classes/Observable.h"

#include "../Utility/Direction.h"

#include "../SDL2/include/SDL.h"

#include <deque>

class GamePiece;
class TileRoadBehavior;

class EnemyBehavior : public Behavior, public Observable
{
	int m_movementSpeed;
	Direction m_myDirection;

	std::deque<TileRoadBehavior*> m_roadTiles;

	int m_health;
	bool m_isDestroyed;
public:
	//Observable - virtuals
	virtual void Attach() override;
	virtual void Detach() override;
	virtual void Notify() override;
	virtual void ReceiveMessage(int value) override;
	virtual bool IsDestroyed() const { return m_isDestroyed; };

	/// [josh] Unused?
	void DestroyThis();

	virtual const GUID GetGUID() override;
	virtual const SDL_Point* GetThisPoint() override;
	virtual const SDL_Rect* GetThisRect() override { return m_pMyBase->GetRect(); }

	//Returns health of this unit
	int GetEnemyHealth() const { return m_health; }

	//Setter/Getter - Direction
	void SetMovingDirection(Direction newDirection);
	Direction GetMovingDirection() const { return m_myDirection; }

	//Sets road tiles
	void SetRoadTiles(std::deque<TileRoadBehavior*> roadTiles) { m_roadTiles = roadTiles; }

	EnemyBehavior(GamePiece* pMyBase, int movementSpeed, int unitHealth);
	virtual void Execute() override;

private:
	void UpdateDirection();
};