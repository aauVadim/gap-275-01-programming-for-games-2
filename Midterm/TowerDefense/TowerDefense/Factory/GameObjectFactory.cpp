#include "GameObjectFactory.h"

//Components
#include "../Components/InteractibleComp.h"

//Structures
#include "../Structures/Structure.h"
//Structure Behaviors 
#include "../Structures/Behaviors/ArcherTowerBeahvior.h"

//Projectiles
#include "../Structures/Prejectiles/Projectile.h"

//Tile Behaviors
#include "../Tiles/Behaviors/TowerBaseBehavior.h"
#include "../Tiles/Behaviors/EnemySpawnBehavior.h"
#include "../Tiles/Behaviors/TileRoadBehavior.h"
#include "../Tiles/Behaviors/TileCastleBehavior.h"

//Direction 
#include "../Utility/Direction.h"

//Enemy Behaviors 
#include "../Enemies/EnemyBehavior.h"

#include "../Game_Field/GameField.h"
//Utility
#include "../Utility/UtilityFunctions.h"

//Player
#include "../Player/Player.h"

#include "Windows.h"
#include <assert.h>


Tile* GameObjectFactory::CreateGroundTile(char tileChar, int posX, int posY)
{
	//To Rez: Hastable? Should I use it for texture loadings?

	//Textures for tiles
	char* grassTextureLocation		= "Textures/Ground/Texture_Grass.png";
	char* groundTextureLocation		= "Textures/Ground/Texture_Road.png";
	char* castleTextureLocation		= "Textures/Ground/Texture_Castle.png";
	char* buildableTextureLocation	= "Textures/Ground/Texture_TowerBase.png";
	char* spawnTextureLocation		= "Textures/Ground/Texture_Spawn.png";
	
	//Creating new tile 
	Tile* pNewTile = nullptr;
	//Creating new Renderer - All tiles shoul have renderer component
	RendererComp* pNewRenderer = nullptr;


	switch (tileChar)
	{
	case 'X':
		pNewRenderer = new RendererComp(0, grassTextureLocation, posX, posY);
		pNewTile = new Tile(pNewRenderer, CreateGIUD());
		break;
		/*Direction tiles*/
	case '6'://Tile will lead player rigth
		pNewRenderer = new RendererComp(0, groundTextureLocation, posX, posY);
		pNewTile = new Tile(pNewRenderer, CreateGIUD());
		pNewTile->AddBehavior(new TileRoadBehavior(pNewTile, posX, posY, Direction::k_west));
		break;
	case'2'://Tile will lead player down
		pNewRenderer = new RendererComp(0, groundTextureLocation, posX, posY);
		pNewTile = new Tile(pNewRenderer, CreateGIUD());
		pNewTile->AddBehavior(new TileRoadBehavior(pNewTile, posX, posY, Direction::k_south));
		break;
		/*End Direction tiles*/
	case 'C':
		pNewRenderer = new RendererComp(0, castleTextureLocation, posX, posY);
		pNewTile = new Tile(pNewRenderer, CreateGIUD());
		pNewTile->AddBehavior(new TileRoadBehavior(pNewTile, posX, posY, Direction::k_none));
		break;
	case 'T':
		pNewRenderer = new RendererComp(0, buildableTextureLocation, posX, posY);
		pNewTile = new Tile(pNewRenderer, CreateGIUD());
		//Adding Interactible component for buildable tile
		pNewTile->AddComponent(new InteractibleComp(posX, posY, pNewTile->GetRect(), pNewTile));
		pNewTile->AddBehavior(new TowerBaseBehavior(pNewTile));
		break;
	case 'S':
		pNewRenderer = new RendererComp(0, spawnTextureLocation, posX, posY);
		pNewTile = new Tile(pNewRenderer, CreateGIUD());
		pNewTile->AddBehavior(new EnemySpawnBehavior(pNewTile));
		break;
	default:
		if (pNewTile)
			pNewTile = nullptr;
		break;
	}



	//Returning newly created tile
	return pNewTile;
}


//Replaces behaviors on tiles - OldBehavior is used to remove TowerBaseBehavior
//Creates a structure instead of bildable tile
void GameObjectFactory::CreateStructure(GamePiece* pObjectBase)
{
	//Textures
	char* structureTextureLocation = "Textures/Structures/Texture_Structure.png";

	//Zeroing out for now
	GamePiece* pNewTower = nullptr;
	RendererComp* pNewRenderer = nullptr;
	
	Uint32 x, y;

	pObjectBase->GetRectPosition(x, y);
	pNewRenderer = new RendererComp(1, structureTextureLocation, x, y);
	//Creating actual thing that will exist in the world
	pNewTower = new GamePiece(pNewRenderer, CreateGIUD());
	//Adding a behavior - Archer Tower
	pNewTower->AddBehavior(new ArcherTowerBehavior(pNewTower, 100, 1));
}

//Creating an enemy
void GameObjectFactory::CreateEnemy(Tile* pPlacementTile)
{
	assert(pPlacementTile);
	char* enemyTextureLocation = "Textures/Enemies/Texture_Enemy_Medium.png";

	GamePiece* pNewGamePiece = nullptr;
	RendererComp* pNewRendererComp = nullptr;

	Uint32 x, y;
	//Getting and saving position of spawn point
	pPlacementTile->GetRectPosition(x, y);
	//Creating RendererComp for this class
	pNewRendererComp = new RendererComp(1, enemyTextureLocation, x, y);
	//Creating actual thing that will exist in the world
	pNewGamePiece = new GamePiece(pNewRendererComp, CreateGIUD());
	//Adding a behavior - Enemy. Movement speed: 1
	pNewGamePiece->AddBehavior(new EnemyBehavior(pNewGamePiece, 1, 100));

}

void GameObjectFactory::CreateProjectile(ArcherTowerBehavior* pFromStructure, SDL_Point* pTarget)
{
	//Texture
	char* projectileTextureLocation = "Textures/Structures/Texture_Projectile.png";

	GamePiece* pNewGamePiece = nullptr;
	RendererComp* pNewRendererComp = nullptr;

	Uint32 x, y;

	pFromStructure->GetGamePiece()->GetRectPosition(x, y);

	SDL_Log("Created projectile, posX: %i, posY: %i", x, y);

	pNewRendererComp = new RendererComp(1, projectileTextureLocation, x, y);
	//Creating actual thing that will exist in the world
	pNewGamePiece = new GamePiece(pNewRendererComp, CreateGIUD());
	//Adding a behavior - Projectile
	pNewGamePiece->AddBehavior(new ProjectileBehavior(pTarget, pNewGamePiece, 0.3f));
}

//	Simple but needs to happen here on the factory
Player* GameObjectFactory::CreatePlayer()
{
	Player* pNewPlayer = new Player(100);
	return pNewPlayer;
}

