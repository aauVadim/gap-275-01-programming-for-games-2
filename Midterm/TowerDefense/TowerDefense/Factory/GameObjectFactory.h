#pragma once

#include "../Utility/Singleton.h"
#include "../Tiles/Tile.h"
#include "../Game_Field/GameField.h"

class InteractibleComp;
class Structure;
class ArcherTowerBehavior;
class Player;

class GameObjectFactory : public Singleton<GameObjectFactory>
{
public:
	Tile* CreateGroundTile(char tileChar, int posX, int posY);
	void CreateStructure(GamePiece* pObjectBase);
	void CreateEnemy(Tile* pPlacementTile);
	void CreateProjectile(ArcherTowerBehavior* pFromStructure, SDL_Point* pTarget);
	Player* CreatePlayer();
};

