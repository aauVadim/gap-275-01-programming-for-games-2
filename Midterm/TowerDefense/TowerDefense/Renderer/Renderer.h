//	Renderer.h
//	Name: Vadim Osipov 
//	Date: 10/15/15

#pragma once

#include <deque>

#include "..\Utility\Singleton.h"
#include "../Base_Classes/Manager.h"

#include "RendererComp.h"
#include "windows.h"

class Renderer : public Singleton<Renderer>, public Manager
{
	//SDL Window variables
	SDL_Window* m_pWindow; 
	SDL_Renderer* m_pRenderer;

	std::deque<RendererComp*> m_renderableObjects;

	GUID* m_myGUID;

public:
	
	bool Initialize();

	virtual void Update(Uint32 timePast = 0) override;
	virtual void OnInitialize() override;
	virtual void OnShutdown() override;
	virtual void DeleteGamePiece(GamePiece* pGamePiece) override;
	virtual GUID* GetManagerGUID() override;


	SDL_Renderer* GetRenderer() { return m_pRenderer; }

	//Adding object to the queue
	void AddObject(RendererComp* pObject);

	//Removing object from the queue - OFF FOR NOW
	std::deque<RendererComp*> RemoveObject(RendererComp* pObject) { };
	
	//Will render the queue in Z-Order of objects 
	void RenderScene();

private:
	~Renderer();
	//Utility function to sort the queue. OFF FOR NOW 
	std::deque<RendererComp*> SortQueue(std::deque<RendererComp*> queue) { };
};