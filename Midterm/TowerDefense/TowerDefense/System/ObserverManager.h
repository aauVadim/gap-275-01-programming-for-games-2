//	ObserverManager.h
//	Name: Vadim Osipov 
//	Date: 10/25

#pragma once

//Inherits
#include "../Base_Classes/Manager.h"
#include "../Utility/Singleton.h"

//SDL
#include "../SDL2/include/SDL.h"

//Works with
class Observer;
class Observable;

//Platform
#include <map>
#include "windows.h"

class ObserverManager : public Singleton<ObserverManager>, public Manager
{
	//Decided to keep them both in std::maps - because both of them can come and go
	std::map<DWORD, Observer*>		m_observers;
	std::map<DWORD, Observable*>	m_observables;

	GUID* m_myGUID;

public: 
	//Manager - pure virtuals
	virtual void Update(Uint32 timePast) override { }
	virtual void OnInitialize() override;
	virtual void OnShutdown() override;
	virtual void DeleteGamePiece(GamePiece* pGamePiece) override;
	virtual GUID* GetManagerGUID() override;

	void GetUpdates(Observable* pObservable);

	void AddObserver(Observer* pObserver);
	void RemoveObserver(Observer* pObserver);

	void AddObservable(Observable* pObservable); 
	void RemoveObservable(Observable* pObservable);

private:
	~ObserverManager();

};