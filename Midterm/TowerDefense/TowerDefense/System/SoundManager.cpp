//	SoundManager.cpp
//	Name: Vadim Osipov
//	Date: 10/27

#include "SoundManager.h"

#include "../SDL2/include/SDL.h"
#include "../SDL2_mixer/include/SDL_mixer.h"

SoundManager::~SoundManager()
{
	Mix_FreeMusic(m_pBGMusic);
	m_pBGMusic = nullptr;

	Mix_CloseAudio();
	Mix_Quit();
}

void SoundManager::OnInitialize()
{
	if (SDL_Init(SDL_INIT_AUDIO) < 0)
	{
		SDL_Log("\nFailed to initialize to Audio");
		//Print things out
		return;
	}

	if (Mix_Init(MIX_INIT_MP3) < 0)
		return;

	if (Mix_OpenAudio(MIX_DEFAULT_FREQUENCY, MIX_DEFAULT_FORMAT, MIX_DEFAULT_CHANNELS, 1024) < 0) //4096
	{
		SDL_Log("\nMix_OpenAudio() - Failed");
		return;
	}

	if (!LoadSounds())
	{
		return;
	}
	else
	{
		Mix_VolumeMusic(255);
		if (Mix_PlayMusic(m_pBGMusic, 1) < 0)
		{
			SDL_Log("SOMETHING WENT WRONG!!");
		}
		SDL_Log("Started Music");
	}
	
}

bool SoundManager::LoadSounds()
{
	m_pBGMusic = Mix_LoadMUS("Sounds/BGMusic/BackgroundMusic.mp3");
	if (!m_pBGMusic)
	{
		SDL_Log("\n Could not load Sounds/BGMusic/BG_Music.mp3 \n");
		return false;
	}

	SDL_Log("\n Loaded sound files \n");
	return true;
}