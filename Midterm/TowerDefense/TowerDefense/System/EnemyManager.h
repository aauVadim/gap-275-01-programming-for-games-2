#pragma once

//Inherits
#include "../Utility/Singleton.h"
#include "../Base_Classes/Manager.h"
//SDL 
#include "../SDL2/include/SDL.h"

//Platform
#include "windows.h"
#include <deque>
#include <map>

class Tile;
class EnemyBehavior;
class TileRoadBehavior;


class EnemyManager : public Singleton<EnemyManager>, public Manager
{
	std::deque<TileRoadBehavior*> m_roadTiles;
	std::deque<Tile*> m_spawnLocations;
	std::map<DWORD, EnemyBehavior*> m_enemies;
	
	//Individial enemy spawned every 
	int m_individualEnemySpawnInterval = 5;
	//Group of evemies spawned every 10 seconds
	int m_groupEnemySpawnInterval = 10; 

	Uint32 m_lastSpawnTime = 0; 

	GUID* m_myGUID = nullptr;

public: 
	//Add/Remove spawn point
	void AddSpawnPoint(Tile* pSpawnPoint);
	void RemoveSpawnPoint(Tile* pSpawnPoint) { };				//TODO: Make this, if need
	// Add/Remove Enemy BEhaviors 
	void AddEnemy(EnemyBehavior* pEnemy);
	void RemoveEnemy(EnemyBehavior* pEnemy);				

	//Add/Remove Road Tile Behaviors 
	void AddRoadBeahvior(TileRoadBehavior* pRoadTile);
	void RemoveRoadBeahvior(TileRoadBehavior* pRoadTile) { };	//TODO: Make this, if need

	virtual void Update(Uint32 timePast) override;


	virtual void OnInitialize() override;
	virtual void OnShutdown() override;
	virtual void DeleteGamePiece(GamePiece* pGamePiece) override;
	virtual GUID* GetManagerGUID() override;

private:
	~EnemyManager();

	//Spawning evemy
	void SpawnEnemy(Uint32 timePast);
	void MoveEnemies();
	void CheckPoints();
};