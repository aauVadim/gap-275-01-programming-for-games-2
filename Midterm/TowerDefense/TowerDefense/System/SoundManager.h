//	SoundManager.h
//	Name: Vadim Osipov
//	Date: 10/27

#pragma once

//inherits
#include "../Base_Classes/Manager.h"
#include "../Utility/Singleton.h"

#include "../SDL2/include/SDL.h"
#include "../SDL2_mixer/include/SDL_mixer.h"

class SoundManager : public Singleton<SoundManager>, public Manager
{
	Mix_Music* m_pBGMusic;

public:
	//Do I need this? Lets keep it in for now;
	virtual void OnInitialize() override;
	virtual void Update(Uint32 timePast) override { };
	virtual void OnShutdown() override { };
	virtual void DeleteGamePiece(GamePiece* pGamePiece) override { };
	virtual GUID* GetManagerGUID() override { return nullptr; };

private:
	~SoundManager();
	bool LoadSounds();
};