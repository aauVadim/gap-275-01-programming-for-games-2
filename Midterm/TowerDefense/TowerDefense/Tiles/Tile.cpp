

#include "Tile.h"

#include "../Base_Classes/Behavior.h"

Tile::Tile(RendererComp* pRenderer, GUID myID)
	:GamePiece(pRenderer, myID)
{

}

Tile::~Tile()
{
	if (m_pMyRect)
		delete m_pMyRect;
}
void Tile::AddBehavior(Behavior* pBehavior)
{
	m_pMyBehaviors.push_back(pBehavior);
}


//For now 
void Tile::RemoveBehavior(Behavior* pBehavior)
{
	for (Behavior* pBehav : m_pMyBehaviors)
		pBehav = nullptr;

	m_pMyBehaviors.clear();
}

void Tile::Execute()
{
	for (Behavior* pBehav : m_pMyBehaviors)
	{
		pBehav->Execute();
	}
}

