
#include "EnemySpawnBehavior.h"

#include "../../Base_Classes/GamePiece.h"

#include "../../System/EnemyManager.h"

EnemySpawnBehavior::EnemySpawnBehavior(GamePiece* pMyBase)
	: Behavior(pMyBase)
{
	AddToManager();
}
//Might not be the best way doing it. But here it is.
void EnemySpawnBehavior::AddToManager()
{
	EnemyManager::GetInstance()->AddSpawnPoint((Tile*)m_pMyBase);
}
