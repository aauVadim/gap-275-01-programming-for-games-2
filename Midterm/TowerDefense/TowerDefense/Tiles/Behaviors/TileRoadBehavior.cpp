#include "TileRoadBehavior.h"

#include "../../System/EnemyManager.h"

#include "../../World/World.h"

TileRoadBehavior::TileRoadBehavior(GamePiece* pMyBase, int posX, int posY, Direction myDirection)
	: Behavior(pMyBase)
	, m_myDirection(myDirection)
{
	//Adding itself to a manager
	AddToManager();
}

void TileRoadBehavior::AddToManager()
{
	EnemyManager::GetInstance()->AddRoadBeahvior(this);
}

void TileRoadBehavior::OnEnterCenterOfTile(GamePiece* pPiece)
{
	
}

void TileRoadBehavior::GetPosition(Uint32& x, Uint32& y)
{
	Uint32 tempX, tempY;
	m_pMyBase->GetRectPosition(tempX, tempY);
	x = tempX;
	y = tempY;
}