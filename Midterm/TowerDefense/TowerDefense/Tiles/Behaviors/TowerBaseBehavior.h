
#pragma once

#include "../../Base_Classes/Behavior.h"

class GamePiece;

class TowerBaseBehavior : public Behavior
{

public:
	~TowerBaseBehavior() { };

	TowerBaseBehavior(GamePiece* pMyTile);
	virtual void Execute() override;
};