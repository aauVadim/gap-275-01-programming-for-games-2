//	Singleton.h
//	Name: Vadim Osipov
//	Date: 10/15/15

#pragma once

template <typename Type>
class Singleton
{
	static Type* m_pInstance;

public: 
	static Type* GetInstance() { if (!m_pInstance) m_pInstance = new Type; return m_pInstance; };

protected:
	Singleton() { };
	virtual ~Singleton() { };
};

template <typename Type>
Type* Singleton<Type>::m_pInstance = nullptr;

