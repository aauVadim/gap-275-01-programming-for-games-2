#pragma once

enum class Direction
{
	k_none, 
	k_north, 
	k_south, 
	k_west, 
	k_east
};