//	Structure.h 
//	Name: Vadim Osipov
//	Date: 10/17 


#pragma once

#include "../SDL2/include/SDL.h"
#include "../Components/InteractibleComp.h"

#include "../Base_Classes/GamePiece.h"

#include "../SDL2/include/SDL.h"

class RendererComp;


class Structure : public GamePiece
{

	//Storing loop pos
	int m_posX, m_posY;
	//Reference to our base
	InteractibleComp* m_pBaseTile; 
	


public:
	Structure(RendererComp* pRenderer, int posX, int posY, GUID myID);
	virtual ~Structure() { if (m_pBaseTile) delete m_pBaseTile; m_pBaseTile = nullptr; }

	void Shoot(SDL_Point* pPointToShootAt);
};