//	ArcherTowerBehavior.h
//	Name: Vadim Osipov 
//	Date: 10/24

#pragma once

//Inherits
#include "../../Base_Classes/Behavior.h"
#include "../../Base_Classes/Observer.h"

#include "../../SDL2/include/SDL.h"

class GamePiece;
class Observable;

class ArcherTowerBehavior : public Behavior, public Observer
{
	int m_shootDistance; 
	Uint32 m_shootRate; 
	Uint32 m_lastShot; 

public: 
	ArcherTowerBehavior(GamePiece* myBase, int shootDistance, Uint32 shootRate);
	~ArcherTowerBehavior() { }
	
	//Observer - pure virtuals
	virtual void OnSpawn() override;
	virtual void OnDestroy() override;
	virtual void Update(Observable* pObservable) override;
	virtual const GUID GetGUID() override;

	
	void ShootAt(SDL_Point* pTarget);
};