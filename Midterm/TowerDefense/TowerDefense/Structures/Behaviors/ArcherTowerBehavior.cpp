//	ArcherTowerBehavior.cpp
//	Name: Vadim Osipov 
//	Date: 10/24

#include "ArcherTowerBeahvior.h"

#include "../../System/StructureManager.h"
#include "../../System/ObserverManager.h"

#include "../../Factory/GameObjectFactory.h"

#include "../../Base_Classes/Observable.h"

#include "../../World/World.h"

#include "../../Utility/UtilityFunctions.h"

ArcherTowerBehavior::ArcherTowerBehavior(GamePiece* myBase, int shootDistance, Uint32 shootRate)
	: Behavior(myBase)
	, m_shootDistance(shootDistance)
	, m_shootRate(shootRate)
{
	OnSpawn();
}

void ArcherTowerBehavior::ShootAt(SDL_Point* pTarget)
{
	//Not cool casting here - dont look
	GameObjectFactory::GetInstance()->CreateProjectile(this, pTarget);
}

//Observer
void ArcherTowerBehavior::OnSpawn()
{
	StructureManager::GetInstance()->AddStructure(this);
	ObserverManager::GetInstance()->AddObserver((Observer*)this);
}
void ArcherTowerBehavior::OnDestroy()
{
	//Nothing
}
void ArcherTowerBehavior::Update(Observable* pObservable)
{
	SDL_Point* myPoint = new SDL_Point;
	myPoint->x = m_pMyBase->GetRect()->x;
	myPoint->y = m_pMyBase->GetRect()->y;

	SDL_Point* hisPoint = new SDL_Point;
	hisPoint->x = pObservable->GetThisPoint()->x;
	hisPoint->y = pObservable->GetThisPoint()->y;

	if (GetDistance(myPoint, hisPoint) <= m_shootDistance)
	{
		if (CanAct(m_shootRate, m_lastShot))
			ShootAt(hisPoint);
	}
}
const GUID ArcherTowerBehavior::GetGUID()
{
	return m_pMyBase->GetGUID();
}

//bool ArcherTowerBehavior::CanShoot()
//{
//	Uint32 time = World::GetInstance()->GetTimePast();
//
//	if ((time > 0) && ((time % m_shootRate) == 0) && (time != m_lastShot))
//	{
//		m_lastShot = time;
//		return true;
//	}
//
//	return false;
//}
