//	Component.h
//	Name: Vadim Osipov 
//	Date: 10/17

#pragma once

#include "../SDL2/include/SDL.h"

class Component
{
protected:
	int m_posX, m_posY;
	SDL_Rect* m_pRect;

public: 
	
	Component(int posX, int posY) : m_posX(posX), m_posY(posY), m_pRect(nullptr) { }
	virtual ~Component() { if (m_pRect) delete m_pRect; }

	virtual void AddToManager() = 0;

	//Position getters
	int GetPosX() { return m_posX; }
	int GetPosY() { return m_posY; }

	//Returns current rect 
	SDL_Rect* GetRect() { return m_pRect; }
	//Sets given rect to new rect 
	void SetRect(SDL_Rect* newRect) { m_pRect = newRect; }

};