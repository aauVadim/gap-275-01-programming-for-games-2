//	Observable.h
//	Name: Vadim Osipov 
//	Date: 10/25

#pragma once

#include "windows.h"
#include "../SDL2/include/SDL.h"

//---------------------------------------------------------------------------------------------------------------------
//	My understanding of Observable concept
//		- Need to be attached to an object that we wish to observe. 
//		- Needs to be coupled with Observer
//---------------------------------------------------------------------------------------------------------------------
class Observable
{
public:
	virtual ~Observable() { };

	virtual void Attach() = 0;
	virtual void Detach() = 0; 
	virtual void Notify() = 0;

	virtual void ReceiveMessage(int value = 0) = 0;

	virtual bool IsDestroyed() const = 0;

	//Must have
	virtual const GUID GetGUID() = 0;
	//Must return its point
	virtual const SDL_Point* GetThisPoint() = 0;
	virtual const SDL_Rect* GetThisRect() = 0;
};