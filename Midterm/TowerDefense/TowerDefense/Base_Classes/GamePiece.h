//	GamePiece.h
//	Name: Vadim Osipov 
//	Date: 10/19

#pragma once

#include <deque>

//class RendererComp;

#include "../Renderer/RendererComp.h"
#include "../Utility/Direction.h"

#include "windows.h"

class Component;
class Behavior;
//---------------------------------------------------------------------------------------------------------------------
// This is very base class for any object that can be interacted with or has to be rendered in the game.
//---------------------------------------------------------------------------------------------------------------------
class GamePiece 
{
	RendererComp* m_pMyRendererComp;
	GUID m_myID;

protected:
	std::deque<Component*> m_MyComponents;
	std::deque<Behavior*> m_pMyBehaviors;

public: 
	GamePiece(RendererComp* pRenderer, GUID myID);
	virtual ~GamePiece();

	//Returns SDL_Rect of this object
	SDL_Rect* GetRect() const { return m_pMyRendererComp->GetRect(); }

	//Add/Remove Components 
	virtual void AddComponent(Component* pComp);
	virtual void RemoveComponent(Component* pComp);

	//Add/Remove Behaviors
	virtual void AddBehavior(Behavior* pBehavior) { } 
	virtual void RemoveBehavior(Behavior* pBehavior) { };
	
	//Returns object GUID
	GUID GetGUID() const { return m_myID; }

	//Returns position of this object
	void GetRectPosition(Uint32& x, Uint32& y);
	
	//Not pure virtual for debuggin purposes. I should change it if i can
	virtual void Execute() { };

	//Siply moves GamePiece on X or Y
	void MoveMe(int plusPosition, Direction direction);
	//Moves GamePiece to given point
	void LerpTo(SDL_Point* pPointTo, float percent);

	void DestroyItself();
};