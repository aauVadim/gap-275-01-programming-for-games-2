//	World.cpp
//	Name: Vadim Osipov 
//	Date: 10/17/15

#include "World.h"

#include "../System/StructureManager.h"
#include "../System/EnemyManager.h"
#include "../Renderer/Renderer.h"
#include "../System/SoundManager.h"

#include "../Base_Classes/GamePiece.h"

#include "../Factory/GameObjectFactory.h"



World::~World()
{
	m_managers.clear();

	if (m_pPlayer)
		delete m_pPlayer;
	m_pPlayer = nullptr;

	if (m_pGameField)
		delete m_pGameField;
	m_pGameField = nullptr;
}

void World::Initialize()
{
	//calling other managers to shoot
	OnInitialize();

	//Safety check
	if (m_isGameOver)
		m_isGameOver = false;

	//Creating the player
	m_pPlayer = GameObjectFactory::GetInstance()->CreatePlayer();

	m_gridHeight = 15;
	m_gridWidth = 20;
	
	//Initializing
	m_timePast = 0;

	//Creatin the grid
	m_pGameField = new GameField(m_gridWidth, m_gridHeight);
	m_pGameField->Initialize();



}
void World::OnInitialize()
{

	if (!Renderer::GetInstance()->Initialize())
		SDL_Log("Main() - Cannot initialize Renderer.");

	SoundManager::GetInstance()->OnInitialize();
}




void World::Update(Uint32 timePast)
{
	//Saving time
	m_timePast = timePast;

	//Updating towers and their projectiles
	StructureManager::GetInstance()->Update();
	//Moving and spawning enemies
	EnemyManager::GetInstance()->Update(timePast);
	//Rendering
	Renderer::GetInstance()->Update();
}

void World::AddManager(Manager* pManager)
{
	m_managers[pManager->GetManagerGUID()->Data1] = pManager;
}
void World::RemoveManager(Manager* pManager)
{
	m_managers.erase(pManager->GetManagerGUID()->Data1);
}

void World::DeleteGamePiece(GamePiece* pGamePiece)
{
	for (std::map<DWORD, Manager*>::iterator it = m_managers.begin(); it != m_managers.end(); ++it)
	{
		it->second->DeleteGamePiece(pGamePiece);
	}
}

bool World::IsGameOver()
{
	//SDL_Log("Player health: %i", m_pPlayer->GetHealth());

	if (m_pPlayer->GetHealth() <= 0)
		return m_isGameOver = true;

	return m_isGameOver = false;
}