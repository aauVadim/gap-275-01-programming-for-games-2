//ObjLoader.h

#ifndef OBJ_LOADER_H
#define OBJ_LOADER_H

#include <iostream>
#include <fstream>
#include <vector>
#include <string>
using std::string;

#include "Object.h"

class ObjLoader
{

private:
	void LoadObject(std::ifstream& fileToLoad, std::vector<Vertex> &vertexList, std::vector<Face> &faceList);

public:
	ObjLoader();
	~ObjLoader();

	Object* LoadObj(const std::string& filename);
};

#endif