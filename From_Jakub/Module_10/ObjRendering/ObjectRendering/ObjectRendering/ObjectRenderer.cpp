//ObjectRenderer.cpp
#include <SDL.h>
#include <gl/glew.h>
#include <SDL_OpenGL.h>
#include <conio.h>
#include "OpenGlEngine.h"
int main(int argc, char* argv[])
{
	OpenGlEngine openGlEngine;


	bool done = false;
	while (!done)
	{
		done = openGlEngine.Update();
	}

	openGlEngine.~OpenGlEngine();

	return 0;
}