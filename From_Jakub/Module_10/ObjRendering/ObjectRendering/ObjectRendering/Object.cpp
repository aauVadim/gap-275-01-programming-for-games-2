//Object.cpp

#include "Object.h"

Object::Object(std::vector<Vertex> vertexList, std::vector<Face> faceList)
{
	for (unsigned int i = 0; i < vertexList.size(); ++i)
	{
		m_vertexVector.push_back(vertexList[i].x);
		m_vertexVector.push_back(vertexList[i].y);
		m_vertexVector.push_back(vertexList[i].z);
	}

	for (unsigned int i = 0; i < faceList.size(); ++i)
	{
		m_faceVector.push_back(faceList[i].x);
		m_faceVector.push_back(faceList[i].y);
		m_faceVector.push_back(faceList[i].z);
	}
}

Object::~Object()
{
}