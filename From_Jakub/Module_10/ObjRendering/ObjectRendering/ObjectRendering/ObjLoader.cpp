//ObjLoader.cpp

#include "ObjLoader.h"

using std::vector;

ObjLoader::ObjLoader()
{

}

ObjLoader::~ObjLoader()
{

}

Object* ObjLoader::LoadObj(const std::string& filename)
{
	//Opening the file and checking if it opened
	std::ifstream loadedFile;
	loadedFile.open(filename);
	if (!loadedFile.is_open())
	{
		std::cout << "Failed to open file!" << std::endl;
		return false;
	}

	std::vector<Vertex> vertexVector; //our list of vertecies
	std::vector<Face> faceVector; //our list of faces
	LoadObject(loadedFile, vertexVector, faceVector);
	return new Object(vertexVector, faceVector);
}

void ObjLoader::LoadObject(std::ifstream& fileToLoad, std::vector<Vertex> &vertexList, std::vector<Face> &faceList)
{
	Vertex tempVertex;
	Face tempFaces;
	fileToLoad.seekg(0, std::ios_base::beg);
	while (!fileToLoad.eof())
	{
		string line;
		getline(fileToLoad, line);

		string tempString;
		if (line[0] == 118)
		{
			int tempInt = 0;
			for (unsigned int i = 2; i < line.size(); ++i)
			{
				char ch = line[i];
				if (ch == ' ')
				{
					++tempInt;
					if (!tempString.empty())
					{
						if (tempInt == 1)
						{
							tempVertex.x = (float)(atof(tempString.c_str()));
						}
						else if (tempInt == 2)
						{
							tempVertex.y = (float)(atof(tempString.c_str()));
						}
						tempString.clear();
						continue;
					}
				}
				else
				{
					tempString += ch;
				}
			}
			if (!tempString.empty())
			{
				float tempFloat = (float)(atof(tempString.c_str()));
				tempVertex.z = tempFloat;
				tempString.clear();
				vertexList.push_back(tempVertex);
			}
		}
		else if (line[0] == 'f')
		{
			int tempInt = 0;
			for (unsigned int i = 2; i < line.size(); ++i)
			{
				char ch = line[i];
				if (ch == ' ')
				{
					++tempInt;
					if (!tempString.empty())
					{
						if (tempInt == 1)
						{
							tempFaces.x = (int)(atof(tempString.c_str())) -1;
						}
						else if (tempInt == 2)
						{
							tempFaces.y = (int)(atof(tempString.c_str())) -1;
						}
						tempString.clear();
						continue;
					}
				}
				else
				{
					tempString += ch;
				}
			}
			if (!tempString.empty())
			{
				float tempFloat = (float)(atof(tempString.c_str())) -1;
				//int tempInt = (int)(tempFloat * 1000000.000);
				//float tempFloat2 = (float)tempInt;
				//tempFloat2 /= 1000000.000;

				tempFaces.z = (int)tempFloat;
				tempString.clear();
				faceList.push_back(tempFaces);
			}
		}
	}
}

