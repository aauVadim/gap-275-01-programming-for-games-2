#include "ObjFile.h"
#include <iostream>

using std::cout;
using std::endl;
using std::vector;
using std::string;

ObjFile::ObjFile()
{
}

ObjFile::~ObjFile()
{
}

bool ObjFile::Load(const std::string& filename)
{
	
	//Opening the file and checking if it opened
	m_objFile.open(filename);
	if (!m_objFile.is_open())
	{
		std::cout << "Failed to open file!" << std::endl;
		return false;
	}
	LoadVertices();

	return true; // TODO
}

const std::vector<Vertex>& ObjFile::GetVertices() const 
{
		
	return m_vertexVector; // TODO:
}

const std::vector<Face>& ObjFile::GetFaces() const 
{
	return m_faceVector; // TODO
}


void ObjFile::LoadVertices()
{
	Vertex tempVertex;
	Face tempFaces;
	m_objFile.seekg(0, std::ios_base::beg);
	while (!m_objFile.eof())
	{
		string line;
		getline(m_objFile, line);

		string tempString;
		if (line[0] == 118)
		{
			int tempInt = 0;
			for (int i = 0; i < line.size(); ++i)
			{
				char ch = line[i];
				if (ch == ' ')
				{
					++tempInt;
					if (!tempString.empty())
					{
						if (tempInt == 2)
						{
							tempVertex.x = (atof(tempString.c_str()));
						}
						else if (tempInt == 3)
						{
							tempVertex.y = (atof(tempString.c_str()));
						}
						tempString.clear();
						continue;
					}
				}
				else if (tempInt > 0)
				{
					tempString += ch;
				}
			}
			if (!tempString.empty())
			{
				float tempFloat = (atof(tempString.c_str()));
				//int tempInt = (int)(tempFloat * 1000000.000);
				//float tempFloat2 = (float)tempInt;
				//tempFloat2 /= 1000000.000;

				tempVertex.z = tempFloat;
				tempString.clear();
				m_vertexVector.push_back(tempVertex);
			}
		}
		else if (line[0] == 'f')
		{
			int tempInt = 0;
			for (int i = 0; i < line.size(); ++i)
			{
				char ch = line[i];
				if (ch == ' ')
				{
					++tempInt;
					if (!tempString.empty())
					{
						if (tempInt == 2)
						{
							tempFaces.indexX = (atof(tempString.c_str()));
						}
						else if (tempInt == 3)
						{
							tempFaces.indexY = (atof(tempString.c_str()));
						}
						tempString.clear();
						continue;
					}
				}
				else if (tempInt > 0)
				{
					tempString += ch;
				}
			}
			if (!tempString.empty())
			{
				float tempFloat = (atof(tempString.c_str()));
				//int tempInt = (int)(tempFloat * 1000000.000);
				//float tempFloat2 = (float)tempInt;
				//tempFloat2 /= 1000000.000;

				tempFaces.indexZ = tempFloat;
				tempString.clear();
				m_faceVector.push_back(tempFaces);
			}
		}
	}
}

