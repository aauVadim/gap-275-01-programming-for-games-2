//Main.cpp

//Header of them all - windows 
#include<Windows.h>

bool g_running = true;

LRESULT CALLBACK WindowProc(
	HWND   hwnd,
	UINT   uMsg,
	WPARAM wParam,
	LPARAM lParam
	);

void PrintWindowsError()
{
	DWORD errorCode = ::GetLastError();
	wchar_t errorString[1024];

	::FormatMessage(FORMAT_MESSAGE_FROM_SYSTEM, NULL, errorCode, 0, errorString, 1024, NULL);

	OutputDebugString(errorString);
}

//The user-provided entry point for a graphical Windows-based application.
int CALLBACK WinMain(
	HINSTANCE hInstance,
	HINSTANCE hPrevInstance,
	LPSTR     lpCmdLine,
	int       nCmdShow
	)
{
	//:: looking in global main scope - dont have to do this - it's just more explisit
	::OutputDebugString(L"Hello from WinMain!\n");

	//Contains the window class attributes that are registered by the RegisterClass function. @MSDN
	WNDCLASSEX windowClass;	//WNDCLASS - Old school. WNDCLASSEX - Modern API
	windowClass.cbSize = sizeof(windowClass);
	windowClass.style = 0;
	windowClass.lpfnWndProc = &WindowProc;
	windowClass.cbWndExtra = 0;
	windowClass.cbClsExtra = 0;
	windowClass.hInstance = hInstance;
	windowClass.hIcon = 0;
	windowClass.hCursor = 0;
	windowClass.hbrBackground = 0;
	windowClass.lpszMenuName = 0;
	windowClass.lpszClassName = L"Win32IntroApp";
	windowClass.hIconSm = 0;
	//Neat way to zero things out ^^^ 
	//::ZeroMemory(&windowClass, sizeof(windowClass));
	
	if (::RegisterClassEx(&windowClass) == 0)
	{
		PrintWindowsError();
		return -1;
	}
	//Creates an overlapped, pop - up, or child window with an extended window style; 
	//otherwise, this function is identical to the CreateWindow function.For more information about creating a window and for full descriptions of the other parameters of CreateWindowEx, see CreateWindow.
	HWND hwnd = ::CreateWindowEx(0, L"Win32IntroApp", L"win32 Intro App", 
		WS_OVERLAPPEDWINDOW, 0, 0, 640, 480, NULL, NULL, hInstance, NULL);

	if (hwnd == NULL)
	{
		PrintWindowsError();
		return -1;
	}
	//Sets the specified window's show state. @MSDN 
	::ShowWindow(hwnd, SW_SHOW);

	::OutputDebugString(L"Created window!");

	while (g_running)
	{
		MSG msg;
		if (::GetMessage(&msg, hwnd, 0, 0))
		{
			::DispatchMessage(&msg);
		}
	}


	//If return 0 - that means program was complete
	return 0;
}

//Message that we recive form windows 
//Event handler - THE BIG DEAL! 
LRESULT CALLBACK WindowProc(
	HWND   hwnd,
	UINT   uMsg,
	WPARAM wParam,
	LPARAM lParam
	)
{
	if (uMsg == WM_CLOSE)
	{
		g_running = false;
	}

	return ::DefWindowProc(hwnd, uMsg, wParam, lParam);
}