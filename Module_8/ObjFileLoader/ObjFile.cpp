#include "ObjFile.h"

#include <fstream>
#include <iostream>
#include <string>
#include <vector>

using std::string;
using std::vector;
using std::cout;
using std::endl;

//By the time deconstructor comes around - we should already have our file closed. But you can't be too safe. 
ObjFile::~ObjFile()
{
	m_faces.clear();
	m_verts.clear();
}

bool ObjFile::Load(const std::string& filename)
{
	std::ifstream file;
	file.open(filename);

	if (file.is_open())
	{
		//Gather Face and Vertex info 
		GatherData(file);
		file.close();
		return true;
	}
	return false;
}

//To Josh:
//	- I did used same code for Floats and Ints. I know this is not the best way of doing it and I probably should have tried to cast it 
//	and just have one generic loop to figure out if we are dealing with Int or Float. I probaly will ask you how to do that in class - really do not want to google it.  
//	I hope I got the Idea right though :) 
//
//---------------------------------------------------------------------------------------------------------------------
// Gathering Vertex and Faces from the file
//---------------------------------------------------------------------------------------------------------------------
void ObjFile::GatherData(std::ifstream& file)
{
	while (!file.eof())
	{
		string line;
		std::getline(file, line);
		
		//Gathering Vertex info
		if (line[0] == 'v')
		{
			ReadVertex(line);
		}
		//Gathering Faces info
		else if (line[0] == 'f')
		{
			ReadFaces(line);
		}
		line.clear();
	}
}
//Gathering Vertex info 
void ObjFile::ReadVertex(std::string& line)
{
	std::vector<float> tFloats;
	string tSting;

	//Starting from third character
	for (int i = 2; i < (line.size() + 1); ++i)
	{
		//Skipping spaces, end line check
		if (line[i] == ' ' || i == (line.size()))
		{
			tFloats.push_back((atof(tSting.c_str())));
			tSting.clear();
		}
		else
			tSting += line[i];
	}
	//Cant be too safe
	tSting.clear();
	//If we have 3 values in vector.
	//Acept only legit vectors
	if (!tFloats.empty() && tFloats.size() == 3)
	{
		Vertex tempVerts;
		tempVerts.x = tFloats[0];
		tempVerts.y = tFloats[1];
		tempVerts.z = tFloats[2];
		//Dumping old values
		tFloats.clear();
		//Storing values in out Vertex vector 
		m_verts.push_back(tempVerts);
	}
}

//Gathering Faces info 
void ObjFile::ReadFaces(string& line)
{
	std::vector<int> tInts;
	string tSting;
	//Starting from third character
	for (int i = 2; i < (line.size() + 1); ++i)
	{
		//Skipping spaces, end line check
		if (line[i] == ' ' || i == line.size())
		{
			tInts.push_back((atoi(tSting.c_str())));
			tSting.clear();
		}
		else
			tSting += line[i];
	}
	//Can't be too safe
	tSting.clear();
	//If we have 3 values in vector.
	//Acept only legit vectors
	if (!tInts.empty() && tInts.size() == 3)
	{
		Face tFaces;
		tFaces.indexX = tInts[0];
		tFaces.indexY = tInts[1];
		tFaces.indexZ = tInts[2];
		
		//Dumping old values
		tInts.clear();
		//Pushing back to our main face vector
		m_faces.push_back(tFaces);
	}
}