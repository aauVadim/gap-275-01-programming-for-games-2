#ifndef OBJFILE_H
#define OBJFILE_H

#include <iostream>
#include <string>
#include <vector>
#include <fstream>

/// Representation of a 3D vertex.
struct Vertex
{
    float x;
    float y;
    float z;
};

/// Representation of a polygonal face.
/// The indexes in a face represents indexes into the Vertex
/// array for which vertices make up a face.
/// Faces with more than three vertices are supported by the obj
/// format, but we only support loading triangles.
struct Face
{
    int indexX;
    int indexY;
    int indexZ;
};


//	Moved Constructor to this file with just { } - blank body. 
//	Moved: GetVertices and GetFaces here too - It is my personal preference, I think it looks cleaner this way. 
class ObjFile
{
	//Storing this file
	//std::ifstream m_file;
	std::vector<Face> m_faces;
	std::vector<Vertex> m_verts;

public:
	ObjFile() { };
    ~ObjFile();

	/// Loads a Wavefrom Obj file.
	///
	/// Returns whether the file was loaded successfully.
    bool Load(const std::string& filename);

	/// Returns the loaded list of vertices, or none if nothing has been loaded.
	const std::vector<Vertex>& GetVertices() const { return m_verts; };

	/// Returns the loaded list of faces, or none if nothing has been loaded.
	const std::vector<Face>& GetFaces() const { return m_faces; };

private:
	// Gathering data of the file
	void GatherData(std::ifstream& file);
	void ReadVertex(std::string& line);
	void ReadFaces(std::string& line);
};

#endif // OBJFILE_H