#ifndef MEMORYGAME_H
#define MEMORYGAME_H

#pragma once

#define WIN32_LEAN_AND_MEAN
#include <Windows.h>
#include <windowsx.h>
#include <Xinput.h>

struct MemoryCard
{
	bool m_isFacedUp = false;
	RECT m_cardRect; 

};

class MemoryGame
{
public:

	static MemoryGame& getInstance();

	MemoryGame();
	~MemoryGame();

	bool StartUp();
	void Update();
	void Shutdown();

	bool isRunning() const { return m_isRunning; };


	//static - deletes 'this' pointer - needed for WindProc
	static LRESULT CALLBACK WindowProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam);

private:
	//Painting BG function 
	void PaintWindow();
	void PaintCard(const MemoryCard& card, int col, int row);

	MemoryCard m_cards[6];

	bool m_isRunning; 
	//Window handle
	HWND m_hwnd;

	HBRUSH m_bgBrush; 
	HBRUSH m_cardBackBrush;
	HBRUSH m_cardFrontBrush;
};

#endif //MEMORYGAME_H