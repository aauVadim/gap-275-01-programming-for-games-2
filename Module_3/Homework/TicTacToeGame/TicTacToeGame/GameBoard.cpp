#include "GameBoard.h"

GameBoard::GameBoard()
	: m_gameBoard(nullptr)
	, m_winnerTileArray(nullptr)
	, m_ppGameBoardTilesArray(nullptr)
	, m_isGameOver(false)
	, m_isPat(false)
{
	m_ppGameBoardTilesArray = new Tile*[3];
	for (int i = 0; i < 3; ++i)
	{
		m_ppGameBoardTilesArray[i] = new Tile[3];
	}
	//Array to store winner tiles
	m_winnerTileArray = new Tile[3];
}

GameBoard::~GameBoard()
{
	delete[] m_ppGameBoardTilesArray;
	delete[] m_winnerTileArray;
	delete m_gameBoard;
}

bool GameBoard::PaintGameFieldsBackround(HWND hwnd, HBRUSH fieldBrush)
{
	//Storing for future reference;
	m_hwnd = hwnd;

	for (int x = 0; x < 3; ++x)
	{
		for (int y = 0; y < 3; ++y)
		{
			PaintGameFields(hwnd, fieldBrush, m_ppGameBoardTilesArray[y][x], x, y);
		}
	}
	return true;
}



void GameBoard::PaintGameFields(HWND hwnd, HBRUSH fieldBrush, Tile& field, int col, int row)
{
	const int fieldWith = 150;
	const int fieldHeight = 150;

	const int offset = 5;

	RECT fieldRect;
	//Positionning
	fieldRect.left = 10 + (fieldWith + 10) * col;
	fieldRect.top = 10 + (fieldHeight + 10) * row;
	//Size of the fields
	fieldRect.right = fieldRect.left + 150;
	fieldRect.bottom = fieldRect.top + 150;

	//Graphic thing, still don't fully get it
	HDC dc = ::GetDC(hwnd);

	field.m_myRect = fieldRect;
	//Saving position of offset corners
	//Top-Left point with 5px offset in
	field.m_myPoints[0].x = fieldRect.left + offset;
	field.m_myPoints[0].y = fieldRect.top + offset;
	//Bottom-Right point with 5px offset in
	field.m_myPoints[1].x = fieldRect.right - offset;
	field.m_myPoints[1].y = fieldRect.bottom - offset;
	//Top-Right point with 5px offset in
	field.m_myPoints[2].x = fieldRect.left + (fieldWith - offset);
	field.m_myPoints[2].y = fieldRect.top + offset;
	//Bottom-Left point with 5px offset in
	field.m_myPoints[3].x = fieldRect.left + offset;
	field.m_myPoints[3].y = fieldRect.top + (fieldHeight - offset);
	
	//Drawing bg images 
	::FillRect(dc, &fieldRect, fieldBrush);
}

void GameBoard::DrawFieldsGraphic(Tile& currentTile, HDC dc, RECT fieldRect, HBRUSH currentBrush, bool isPlayerOne)
{

	int penThickness = 10;

	//Drawing thingies
	if (isPlayerOne)
	{
		//Make Crosses --- 
		//Creating a pen
		HPEN linePen;
		//Pen Color
		COLORREF lineColor = RGB(0, 83, 13);
		//Creating actial pen
		linePen = ::CreatePen(PS_SOLID, penThickness, lineColor);
		//Selecting it
		HPEN penOld = (HPEN)::SelectObject(dc, linePen);

		//Moving to first pos
		::MoveToEx(dc, currentTile.m_myPoints[0].x, currentTile.m_myPoints[0].y, NULL);
		//Drawing to corner
		::LineTo(dc, currentTile.m_myPoints[1].x, currentTile.m_myPoints[1].y);

		//Moving to second pos
		::MoveToEx(dc, currentTile.m_myPoints[2].x, currentTile.m_myPoints[2].y, NULL);
		//Drawing to corner
		::LineTo(dc, currentTile.m_myPoints[3].x, currentTile.m_myPoints[3].y);
		
		//Has the tile been used? 
		currentTile.m_isPlayed = true;
		//Is it a Cross?
		currentTile.m_isCross = true;

		//Cleaning up? 
		::DeleteObject(linePen);
		::DeleteObject(penOld);
	}
	else
	{
		//Make circles ---
		//Creating a pen
		HPEN linePen;
		//Color of the pen
		COLORREF lineColor = RGB(255, 0, 0);
		//Creating it
		linePen = ::CreatePen(PS_SOLID, penThickness, lineColor);
		//Selecting the pen
		HPEN penOld = (HPEN)::SelectObject(dc, linePen);
		
		//Making a circle
		::Arc(dc, currentTile.m_myPoints[0].x, currentTile.m_myPoints[0].y, currentTile.m_myPoints[1].x, currentTile.m_myPoints[1].y, 0, 0, 0, 0);
		//Has the tile been used? 
		currentTile.m_isPlayed = true;
		
		//Is it a Cross?
		currentTile.m_isCross = false;
		
		//Cleaning up? 
		::DeleteObject(linePen);
		::DeleteObject(penOld);
	}

	CountTiles();
}

//Big-huge chunk of ducktape that i'm very not proud of
void GameBoard::CountTiles()
{
	int playedPieces = 0;
	for (int y = 0; y < 3; ++y)
	{
		//Checking horisontally. This way: -- 
		//Checking for X
		if (m_ppGameBoardTilesArray[y][0].m_isCross && m_ppGameBoardTilesArray[y][0].m_isPlayed
			&& m_ppGameBoardTilesArray[y][1].m_isCross && m_ppGameBoardTilesArray[y][1].m_isPlayed
			&& m_ppGameBoardTilesArray[y][2].m_isCross && m_ppGameBoardTilesArray[y][2].m_isPlayed)
		{
			m_isGameOver = true;
			DrawWinnerLine(m_ppGameBoardTilesArray[y][0], m_ppGameBoardTilesArray[y][1], m_ppGameBoardTilesArray[y][2]);
		}
		//Checking for O
		if (!m_ppGameBoardTilesArray[y][0].m_isCross && m_ppGameBoardTilesArray[y][0].m_isPlayed
			&& !m_ppGameBoardTilesArray[y][1].m_isCross && m_ppGameBoardTilesArray[y][1].m_isPlayed
			&& !m_ppGameBoardTilesArray[y][2].m_isCross && m_ppGameBoardTilesArray[y][2].m_isPlayed)
		{
			m_isGameOver = true;
			DrawWinnerLine(m_ppGameBoardTilesArray[y][0], m_ppGameBoardTilesArray[y][1], m_ppGameBoardTilesArray[y][2]);
		}
		for (int x = 0; x < 3; ++x)
		{
			//Checking vertically. This way: |
			//Checking for X
			if (m_ppGameBoardTilesArray[0][x].m_isCross && m_ppGameBoardTilesArray[0][x].m_isPlayed
				&& m_ppGameBoardTilesArray[1][x].m_isCross && m_ppGameBoardTilesArray[1][x].m_isPlayed
				&& m_ppGameBoardTilesArray[2][x].m_isCross && m_ppGameBoardTilesArray[2][x].m_isPlayed)
			{
				m_isGameOver = true;
				DrawWinnerLine(m_ppGameBoardTilesArray[0][x], m_ppGameBoardTilesArray[1][x], m_ppGameBoardTilesArray[2][x]);
			}
			//Checking for O
			if (!m_ppGameBoardTilesArray[0][x].m_isCross && m_ppGameBoardTilesArray[0][x].m_isPlayed
				&& !m_ppGameBoardTilesArray[1][x].m_isCross && m_ppGameBoardTilesArray[1][x].m_isPlayed
				&& !m_ppGameBoardTilesArray[2][x].m_isCross && m_ppGameBoardTilesArray[2][x].m_isPlayed)
			{
				m_isGameOver = true;
				DrawWinnerLine(m_ppGameBoardTilesArray[0][x], m_ppGameBoardTilesArray[1][x], m_ppGameBoardTilesArray[2][x]);
			}

			if (x == 0 && y == 0)	//Checkig diagonal. Foward slash: "\"
			{
				//Checking for X
				if (m_ppGameBoardTilesArray[y][x].m_isCross && m_ppGameBoardTilesArray[y][x].m_isPlayed
					&& m_ppGameBoardTilesArray[y + 1][x + 1].m_isCross && m_ppGameBoardTilesArray[y + 1][x + 1].m_isPlayed
					&& m_ppGameBoardTilesArray[y + 2][x + 2].m_isCross && m_ppGameBoardTilesArray[y + 2][x + 2].m_isPlayed)
				{
					m_isGameOver = true;
					DrawWinnerLine(m_ppGameBoardTilesArray[y][x], m_ppGameBoardTilesArray[y + 1][x + 1], m_ppGameBoardTilesArray[y + 2][x + 2]);
				}
				//Checking for O
				if (!m_ppGameBoardTilesArray[y][x].m_isCross && m_ppGameBoardTilesArray[y][x].m_isPlayed
					&& !m_ppGameBoardTilesArray[y + 1][x + 1].m_isCross && m_ppGameBoardTilesArray[y + 1][x + 1].m_isPlayed
					&& !m_ppGameBoardTilesArray[y + 2][x + 2].m_isCross && m_ppGameBoardTilesArray[y + 2][x + 2].m_isPlayed)
				{
					m_isGameOver = true;
					DrawWinnerLine(m_ppGameBoardTilesArray[y][x], m_ppGameBoardTilesArray[y + 1][x + 1], m_ppGameBoardTilesArray[y + 2][x + 2]);
				}
			}
			if (x == 2 && y == 0)	//Checkig diagonal. Foward slash: "/"
			{
				//Checking for X
				if (m_ppGameBoardTilesArray[y][x].m_isCross && m_ppGameBoardTilesArray[y][x].m_isPlayed
					&& m_ppGameBoardTilesArray[y + 1][x - 1].m_isCross && m_ppGameBoardTilesArray[y + 1][x - 1].m_isPlayed
					&& m_ppGameBoardTilesArray[y + 2][x - 2].m_isCross && m_ppGameBoardTilesArray[y + 2][x - 2].m_isPlayed)
				{
					m_isGameOver = true;
					DrawWinnerLine(m_ppGameBoardTilesArray[y][x], m_ppGameBoardTilesArray[y + 1][x - 1], m_ppGameBoardTilesArray[y + 2][x - 2]);
				}
				//Checking for O
				if (!m_ppGameBoardTilesArray[y][x].m_isCross && m_ppGameBoardTilesArray[y][x].m_isPlayed
					&& !m_ppGameBoardTilesArray[y + 1][x - 1].m_isCross && m_ppGameBoardTilesArray[y + 1][x - 1].m_isPlayed
					&& !m_ppGameBoardTilesArray[y + 2][x - 2].m_isCross && m_ppGameBoardTilesArray[y + 2][x - 2].m_isPlayed)
				{
					m_isGameOver = true;
					DrawWinnerLine(m_ppGameBoardTilesArray[y][x], m_ppGameBoardTilesArray[y + 1][x - 1], m_ppGameBoardTilesArray[y + 2][x - 2]);
				}
			}

			//Check for 'pat' state
			if (m_ppGameBoardTilesArray[y][x].m_isPlayed)
				++playedPieces;
		}
	}

	//If field has 9 pieces and game was not over - 'pat' - close the app. no-one won
	if (playedPieces == 9 && !m_isGameOver)
	{
		m_isGameOver = true;
		m_isPat = true;
	}
}

void GameBoard::DrawWinnerLine(Tile& first, Tile& second, Tile& third)
{
	HDC dc = ::GetDC(m_hwnd);

	//Make Crosses --- 
	//Creating a pen
	HPEN linePen;
	//Pen Color
	COLORREF lineColor = RGB(21, 31, 166);
	//Creating actial pen
	linePen = ::CreatePen(PS_SOLID, 20, lineColor);
	//Selecting it
	HPEN penOld = (HPEN)::SelectObject(dc, linePen);

	//Moving to middle of first
	::MoveToEx(dc, first.m_myRect.left + 75, first.m_myRect.top + 75, NULL);
	//Drawing to middle of third
	::LineTo(dc, third.m_myRect.left + 75, third.m_myRect.top + 75);


	//Cleaning up? 
	::DeleteObject(linePen);
	::DeleteObject(penOld);
}

