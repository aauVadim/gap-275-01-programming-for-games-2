//	OBJReader.cpp
//	Name: Vadim Osipov 
//	Date: 11/11

#include <iostream>
#include <fstream>

#include "OBJReader.h"

#include "../Object/Object.h"

Object* OBJReader::CreateObjectFromFile(const char* fileName)
{
	std::vector<Face> facesVector;
	std::vector<Vertex> vertexVector;

	std::ifstream file;
	file.open(fileName);

	if (file.is_open())
	{
		while (!file.eof())
		{
			std::string line;
			std::getline(file, line);

			if (line[0] == 'v')
			{
				ReadVertex(line, vertexVector);
			}
			else if (line[0] == 'f')
			{
				ReadFaces(line, facesVector);
			}
			line.clear();
		}
		file.close();
	}
	else
	{
		//Bail out
		return nullptr;
	}

	return new Object(fileName, facesVector, vertexVector);
}

void OBJReader::ReadVertex(std::string& line, std::vector<Vertex>& vertVector)
{
	std::vector<float> tFloats;
	std::string tSting;

	//Starting from third character
	for (int i = 2; i < (line.size() + 1); ++i)
	{
		//Skipping spaces, end line check
		if (line[i] == ' ' || i == (line.size()))
		{
			tFloats.push_back((atof(tSting.c_str())));
			tSting.clear();
		}
		else
			tSting += line[i];
	}
	//Cant be too safe
	tSting.clear();
	//If we have 3 values in vector.
	//Acept only legit vectors
	if (!tFloats.empty() && tFloats.size() == 3)
	{
		Vertex tempVerts;
		tempVerts.m_x = tFloats[0];
		tempVerts.m_y = tFloats[1];
		tempVerts.m_z = tFloats[2];
		//Dumping old values
		tFloats.clear();
		//Storing values in out Vertex vector 
		vertVector.push_back(tempVerts);
	}
}
void OBJReader::ReadFaces(std::string& line, std::vector<Face>& faceVector)
{
	std::vector<int> tInts;
	std::string tSting;
	//Starting from third character
	for (int i = 2; i < (line.size() + 1); ++i)
	{
		//Skipping spaces, end line check
		if (line[i] == ' ' || i == line.size())
		{
			tInts.push_back((atoi(tSting.c_str())));
			tSting.clear();
		}
		else
			tSting += line[i];
	}
	//Can't be too safe
	tSting.clear();
	//If we have 3 values in vector.
	//Acept only legit vectors
	if (!tInts.empty() && tInts.size() == 3)
	{
		Face tFaces;
		tFaces.m_indX = tInts[0];
		tFaces.m_indY = tInts[1];
		tFaces.m_indZ = tInts[2];

		//Dumping old values
		tInts.clear();
		//Pushing back to our main face vector
		faceVector.push_back(tFaces);
	}
}