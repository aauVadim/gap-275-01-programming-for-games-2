//	OBJReader.h
//	Name: Vadim Osipov 
//	Date: 11/11

#ifndef __OBJREADER_H__
#define __OBJREADER_H__

#include <string>
#include <vector>

#include "../Globals/Globals.h"

class Object;

//Basically a factory that also reads a file
class OBJReader
{
public: 
	Object* CreateObjectFromFile(const char* fileName);

private:
	void ReadVertex(std::string& line, std::vector<Vertex>& vertVector);
	void ReadFaces(std::string& line, std::vector<Face>& faceVector);
};

#endif //__OBJREADER_H__