//	ObjectBuffer.h
//	Name: Vadim Osipov
//	Date: 11/14


#include <SDL.H>
#include <gl/glew.h>
#include <SDL_OpenGL.h>

class Object;

class ObjectBuffer
{
public:
	GLuint CreateBuffer(Object* pReadObject);
};