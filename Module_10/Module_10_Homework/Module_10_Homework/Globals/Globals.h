//	Globals.h
//	Name: Vadim Osipov 
//	Date: 11/11

#ifndef __GLOBALS_H__
#define __GLOBALS_H__

//Faces
struct Face
{
	int m_indX;
	int m_indY;
	int m_indZ;
};
//Vertex
struct Vertex
{
	float m_x;
	float m_y;
	float m_z;
};

#endif //__GLOBALS_H__