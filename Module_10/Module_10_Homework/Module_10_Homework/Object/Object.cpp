//	Object.h
//	Name: Vadim Osipov
//	Date: 11/11

#include "Object.h"

Object::Object(std::string myFileName, std::vector<Face> myFaces, std::vector<Vertex> myVertex)
	: m_fileName(myFileName)
	, m_faces(myFaces)
	, m_vertex(myVertex)
{

}