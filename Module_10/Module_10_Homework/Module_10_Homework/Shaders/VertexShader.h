//	VertexShader.h
//	Name: Vadim Osipov
//	Date: 11/14

const char* g_kVertexShader[] = 
{
	"#version 400\n",			//This is the only line that requers newline char
	"in vec3 vertex;",			//Taking in the Vertex coordinates
	"void main() {",
	"	gl_Position = vec4(vertex, 1.0);",	//Position is our vertex coordinate 
	"}",
}