//	PixelShader.h
//	Name: Vadim Osipov
//	Date: 11/14

//I know its Fragment Shader in OpenGL language. 
//But Pixel Shader is just easier for me to remember
const char* g_kPixelShader[] =
{
	"#version 400\n",			//This is the only line that requers newline char
	"out vec4 colorRGBA;",		//Color of pixels
	"void main() {",
	"    colorRGBA = vec4(1.0, 0.2, 0.0, 1.0);", //Coloring the pixels
	"}",
};
