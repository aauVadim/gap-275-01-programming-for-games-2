#include <SDL.h>
#include <gl/glew.h>	//BEFORE SDL_OpenGL.h
#include <SDL_OpenGL.h>

void CheckGLError()
{
	GLenum error = glGetError();
	if (error != 0)
	{
		SDL_Log("GL Error: %d %s", error, glewGetErrorString(error));
		__debugbreak();
	}
}

int main(int args, char* argv[])
{
	SDL_Init(SDL_INIT_VIDEO);

	//Calling to OpenGL 4.0
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 4);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 0);

	//SDL Window
	SDL_Window* window = SDL_CreateWindow("SDL OpenGL Demo", 
		SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, 
		800, 600, 
		SDL_WINDOW_SHOWN |SDL_WINDOW_OPENGL);
	//init things 
	SDL_GLContext context = SDL_GL_CreateContext(window);

	//Lazy way to initialize glew
	//pull up functions for requered OpenGL vercion(4.0)
	glewExperimental = GL_TRUE;
	//Initializing glew
	glewInit();
	glGetError(); //Clering? 

	//Triangle. Vertex array 
	const float triangle[] = 
	{
		//	X	Y	Z
		0.0f,	0.5f,	0.0f,
		0.5f,	-0.5f,	0.0f,
		-0.5f,	-0.5f,	0.0f,
	};

	//Creating a buffer
	//Input Data

	GLuint triangleBuffer;
	glCreateBuffers(1, &triangleBuffer);
	glBindBuffer(GL_ARRAY_BUFFER, triangleBuffer);
	//Placing data into buffer. Putting data to Graphics card
	glBufferData(GL_ARRAY_BUFFER, sizeof(triangle), triangle, GL_STATIC_DRAW);
	
	//Index array 
	const int indicies[] =
	{
		0, 1, 2
	};
	GLuint triangleIndexBuffer;
	glCreateBuffers(1, &triangleIndexBuffer);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, triangleIndexBuffer);
	//Placing data into buffer. Putting data to Graphics card
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indicies), indicies, GL_STATIC_DRAW);
	
	//-----------------------------------------------------------------------------------------------------------------
	//	Creating Vertext Shader - Begin
	//-----------------------------------------------------------------------------------------------------------------
	//Shader code - GLSL GL Shading language
	const char* vertexShaderCode[] =
	{
		"#version 400\n",			//This is the only line that requers newline char
		"in vec3 vertex;",			//Taking in the Vertex coordinates
		"void main() {",
		"	gl_Position = vec4(vertex, 1.0);",	//Position is our vertex coordinate 
		"}",
	};
	//Creating actual shader
	GLuint vertexShader = glCreateShader(GL_VERTEX_SHADER);
	//Giving shader its code
	glShaderSource(vertexShader, _ARRAYSIZE(vertexShaderCode), vertexShaderCode, nullptr);
	//Compiling shader
	glCompileShader(vertexShader);
	CheckGLError();
	//-----------------------------------------------------------------------------------------------------------------
	//	Creating Vertext Shader - End
	//-----------------------------------------------------------------------------------------------------------------
	
	
	//-----------------------------------------------------------------------------------------------------------------
	//	Creating Fragment Shader - Begin
	//-----------------------------------------------------------------------------------------------------------------
	//Shader code - GLSL GL Shading language
	const char* fragmentShaderCode[] =
	{
		"#version 400\n",			//This is the only line that requers newline char
		"out vec4 colorRGBA;",		//Color of pixels
		"void main() {",
		"    colorRGBA = vec4(1.0, 0.2, 0.0, 1.0);", //Coloring the pixels
		//"	colorRGBA = gl_FragCoord;",
		"}",
	};
	//Creating actual shader
	GLuint fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
	//Giving shader its code
	glShaderSource(fragmentShader, _ARRAYSIZE(fragmentShaderCode), fragmentShaderCode, nullptr);
	//Compiling shader
	glCompileShader(fragmentShader);
	//-----------------------------------------------------------------------------------------------------------------
	//	Creating Fragment Shader - End
	//-----------------------------------------------------------------------------------------------------------------


	//-----------------------------------------------------------------------------------------------------------------
	//	Linking Vertex and Pixel/Fragment Shader
	//-----------------------------------------------------------------------------------------------------------------
	GLuint shaderProgram = glCreateProgram();
	glAttachShader(shaderProgram, vertexShader);
	glAttachShader(shaderProgram, fragmentShader);
	glLinkProgram(shaderProgram);
	//-----------------------------------------------------------------------------------------------------------------
	//	Linking Vertex and Pixel/Fragment Shader
	//-----------------------------------------------------------------------------------------------------------------

	GLuint vertArray; 
	glGenVertexArrays(1, &vertArray);
	glBindVertexArray(vertArray);
	glEnableVertexAttribArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, triangleBuffer);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, triangleIndexBuffer);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);


	bool done = false;
	while (!done)
	{
		SDL_Event event;
		while (SDL_PollEvent(&event))
		{
			if (event.type == SDL_WINDOWEVENT && event.window.event == SDL_WINDOWEVENT_CLOSE)
			{
				done = true;
				break;
			}
		}

		//OpenGL - writing color into back buffer.
		glClearColor(0.5f, 0.5f, 0.0f, 1.0f);
		//To the screen
		glClear(GL_COLOR_BUFFER_BIT);

		//-----------------------------------------------------------------------------------------------------------------
		//	Drawing Graphic - begin
		//-----------------------------------------------------------------------------------------------------------------
		glUseProgram(shaderProgram);
		glBindVertexArray(vertArray);
		glDrawArrays(GL_TRIANGLES, 0, _ARRAYSIZE(indicies));
		//-----------------------------------------------------------------------------------------------------------------
		//	Drawing Graphic - end
		//-----------------------------------------------------------------------------------------------------------------



		//Presenting -
		SDL_GL_SwapWindow(window);
	}

	SDL_GL_DeleteContext(context);
	SDL_DestroyWindow(window);
	SDL_Quit();
	return 0;

}