
#include <fstream>
#include <iostream>
#include <string>
#include <vector>

void main()
{
	std::ifstream csvFile;

	//csvFile.open("Sample.csv");
	csvFile.open("Numbers.csv");
	if (!csvFile.is_open())
	{
		std::cout << "Failed to open file!" << std::endl;
		return;
	}

	csvFile.seekg(0, std::ios_base::end);
	std::cout << "File is " << csvFile.tellg() << " bytes!" << std::endl;

	//std::vector<std::vector<std::string>> values;
	std::vector<std::vector<int>> values;
	std::vector<int> tempVector;

	csvFile.seekg(0, std::ios_base::beg);
	while (!csvFile.eof())
	{
		std::string line;
		std::getline(csvFile, line);

		std::cout << "Line: " << line << std::endl;

		std::string tempString;
		for (int i = 0; i < line.size(); ++i)
		{
			char ch = line[i];

			if (ch == ',')
			{
				tempVector.push_back(atoi(tempString.c_str()));
				tempString.clear();
			}
			else
			{
				tempString += ch;
				//values.push_back(std::string(1, ch));
			}
		}

		if (!tempString.empty())
		{
			tempVector.push_back(atoi(tempString.c_str()));
		}

		values.push_back(tempVector);
		tempVector.clear();

		//int ch = csvFile.get();
		//std::cout << "Char: " << ch << static_cast<char>(ch) << std::endl;
	}

	for (int i = 0; i < values.size(); ++i)
	{
		for (int j = 0; j < values[i].size(); ++j)
		{
			std::cout << "values[" << i << "][" << j << "]: " << values[i][j] << std::endl;
		}
	}
}