Hello Josh.

The game: 		Minesweeper.
Objective:  	Mark all of the mines of the field. 
Score given: 	Based on how many mines you marked, plus how many tiles you left closed. 
Controls: 
	- W,A,S,D - Move player cursor around the field. 
	- Space Bar - Open the tile. 
	- E - Mark a tile. 
	
About screens: 
	Main Menu: 
		Three different difficulty levels: (1)Easy, (2)Medium, (3)Hard. Difference is the of the map.
		I highly recommend to play Easy, it takes way to long to complete the other ones.  
		By pressing S you can always see the score board(You have to compile project at least once for it to find Scores.txt)
	
	Game Screen: 
		Just game screen :) Use controls to navigate.
		Also shows number of bombs on the field. And number of Markers left. 
		
	Win screen: 
		Shows player the score. 
		If player chooses he can save the score. 
		
	Lose screen
		Embedded in to game screen. Nothing fancy, just "you suck" - message :) 
		

		
Classes: 

	- GameBoard - Responsible: 
			- Draw calls
			- Opening/Marking tiles
			- Building gameboard
			- Placing bombs and number tiles. 
			
	- BoardPiece - Responsible: 
			- Bomb/Number/Tile - Enumerator
			- Player Cursor - boolean 
			- Open - boolean
			- Has been checked - boolean 
			- Checks tiles next to itself. On opening. 
	- UI - Responsible: 
			- Menus/Screens
			- Saving and reading files. 
	- Player - Responsible:
			- Score
			

P.S.: 
	I wanted to do more for this game, but unfortunately other finals and time got on me. 