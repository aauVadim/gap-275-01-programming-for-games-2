#pragma once

//#include "GameBoard.h"

class GameBoard;

struct BoardPiece
{
	enum TyleType
	{
		EEmpty, 
		EWithBomb, 
		EBombsAround,
	};

	TyleType m_tyleType;

	//Position
	int *m_position = new int[2];

	//Player cursor representation
	char m_playerChar;
	//Is Player cursor bool
	bool m_IsPlayerCursor; 
	//Bombs variables
	//Bools
	bool m_isOpen; 
	bool m_beenChecked;
	bool m_beenMarked; 
	//Chars
	char m_emptyTileChar;
	char m_closedTileChar; 
	char m_bombChar; 
	//Ints
	int m_bombsAround; 

	//Default
	BoardPiece()
		: m_bombsAround(0)
		, m_bombChar('+')
		, m_closedTileChar('#')
		, m_emptyTileChar(' ')
		, m_playerChar('@')
	{
		//Enums
		m_tyleType = TyleType::EEmpty;
		//Bools
		m_IsPlayerCursor = false; 
		m_isOpen = false;
		m_beenChecked = false;
		m_beenMarked = false;
	}

	//Deconstructor
	~BoardPiece()
	{
		if (m_position != nullptr)
			delete[] m_position;
	}

	TyleType SetTyle(TyleType newType);

	void CheckTilesAround(GameBoard& gameBoard);

	//Never figured out how to scope this out....
	//Thats why it is still in .h file 
	template<typename T>
	T GetTyle()
	{
		//if you're not lit by cursor, return yourself
		if (!m_IsPlayerCursor)
		{
			if (!m_isOpen)
				return m_closedTileChar;
			else
			{
				if (this->m_tyleType == TyleType::EEmpty)
					return m_emptyTileChar;
				if (this->m_tyleType == TyleType::EWithBomb)
					return m_bombChar;
				if (this->m_tyleType == TyleType::EBombsAround)
					return m_bombsAround;
			}
		}
		else
			// Else return player cursor
			return m_playerChar;
		
	}
};