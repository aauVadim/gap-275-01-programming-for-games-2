#include "GameBoard.h"
#include "BoardPiece.h"

//System
#define WIN32_LEAN_AND_MEAN
#include "Windows.h"
#include <time.h>
#include <conio.h>
#include <stdio.h>
#include <iostream>
//Random
#include <ctime>
#include <cstdlib>
#include <stdlib.h>
//Development feature
//#define DEBUGGIN 

HANDLE console = GetStdHandle(STD_OUTPUT_HANDLE); //Windows console handler, used to change colors. 

//constructor
GameBoard::GameBoard(const int difficultyLevel)
{
	//Iitialization
	m_boardSizeX = 0;
	m_boardSizeY = 0;
	m_board = nullptr;
	m_bombsPlaced = 0;
	m_bombsMarked = 0;

	m_Player = new Player;
	/*
		I set field to be 20 tall so I do not have to resize the window. 
		numbers can be changed anytime
	*/
	//Building the board 
	switch (difficultyLevel)
	{
	//Small Board
	case 1:
		BuildBoard(10, 10);
		break;
	//Medium board
	case 2:
		BuildBoard(20, 50);
		break;
	//Large board
	case 3:
		BuildBoard(20, 70);
		break;
	default:
		break;
	}
}
//Deconstructor
GameBoard::~GameBoard()
{
	if (m_board != nullptr)
		delete[] m_board;
}
void GameBoard::BuildBoard(const int boardY, const int boardX)
{
	std::srand(std::time(NULL));

	//TODO: Build board according to the number given
	//Place bombs
	//Calculate every 
	
	//Essential build loop
	
	//Some weird voodoo right here 
	m_board = new BoardPiece*[boardY]; 
	for (int i = 0; i < boardY; ++i) m_board[i] = new BoardPiece[boardX];
	//end of some weird voodoo

	m_boardSizeX = boardX;
	m_boardSizeY = boardY;
	//Lets place some bombs
	PlaceBombs();
	//And count them
	InitializeTileNumbers();
}

void GameBoard::PlaceBombs()
{
	//Initialize the bombs and the PlayerC
	for (int y = 0; y < m_boardSizeY; ++y)
	{
		for (int x = 0; x < m_boardSizeX; ++x)
		{
			//Placing the cursor
			if (y == 0 && x == 0)
				m_board[y][x].m_IsPlayerCursor = true;

			//Setting the position of the tile
			m_board[y][x].m_position[0] = y;
			m_board[y][x].m_position[1] = x;
			//Random
			int dice = (std::rand() % 10);
			//Setting bombs if dice = 0
			if (dice == 0)
			{
				//Place a bomb
				m_board[y][x].SetTyle(BoardPiece::EWithBomb);
				m_bombsPlaced++;
			}
			else
			{
				m_board[y][x].SetTyle(BoardPiece::EEmpty);
			}
		}
	}
	m_Player->m_markersLeft = m_bombsPlaced;
}

//Sets the numbers of the tiles according to the bobms around, very ugly looking
//I wanted to edit this function and make it look a little more appealing. But did not find time to do so. 
void GameBoard::InitializeTileNumbers()
{
	//Initialize the numbers
	for (int y = 0; y < m_boardSizeY; ++y)
	{
		for (int x = 0; x < m_boardSizeX; ++x)
		{
			//If this tile is not a bomb
			if (m_board[y][x].m_tyleType != BoardPiece::EWithBomb)
			{
				//Down check
				if ((y + 1) < m_boardSizeY)
				{
					if (m_board[y + 1][x].m_tyleType == BoardPiece::EWithBomb)
					{
						m_board[y][x].SetTyle(BoardPiece::EBombsAround);
						m_board[y][x].m_bombsAround++;
					}
				}
				//Up Check
				if ((y - 1) >= 0)
				{
					if (m_board[y - 1][x].m_tyleType == BoardPiece::EWithBomb)
					{
						m_board[y][x].SetTyle(BoardPiece::EBombsAround);
						m_board[y][x].m_bombsAround++;
					}
				}
				//Right check
				if ((x + 1) < m_boardSizeX)
				{
					if (m_board[y][x + 1].m_tyleType == BoardPiece::EWithBomb)
					{
						m_board[y][x].SetTyle(BoardPiece::EBombsAround);
						m_board[y][x].m_bombsAround++;
					}
				}
				//LEFT check 
				if ((x - 1) >= 0)
				{
					if (m_board[y][x - 1].m_tyleType == BoardPiece::EWithBomb)
					{
						m_board[y][x].SetTyle(BoardPiece::EBombsAround);
						m_board[y][x].m_bombsAround++;
					}
				}
				//Diagonal checks
				//UP-LEFT
				if ((y - 1) >= 0 && (x - 1) >= 0)
				{
					if (m_board[y - 1][x - 1].m_tyleType == BoardPiece::EWithBomb)
					{
						m_board[y][x].SetTyle(BoardPiece::EBombsAround);
						m_board[y][x].m_bombsAround++;
					}
				}
				//UP-RIGHT
				if ((y - 1) >= 0 && (x + 1) < m_boardSizeX)
				{
					if (m_board[y - 1][x + 1].m_tyleType == BoardPiece::EWithBomb)
					{
						m_board[y][x].SetTyle(BoardPiece::EBombsAround);
						m_board[y][x].m_bombsAround++;
					}
				}
				//DOWN-RIGHT
				if ((y + 1) < m_boardSizeY && (x + 1) < m_boardSizeX)
				{
					if (m_board[y + 1][x + 1].m_tyleType == BoardPiece::EWithBomb)
					{
						m_board[y][x].SetTyle(BoardPiece::EBombsAround);
						m_board[y][x].m_bombsAround++;
					}
				}
				//DOWN-LEFT
				if ((y + 1) < m_boardSizeY && (x - 1) >= 0)
				{
					if (m_board[y + 1][x - 1].m_tyleType == BoardPiece::EWithBomb)
					{
						m_board[y][x].SetTyle(BoardPiece::EBombsAround);
						m_board[y][x].m_bombsAround++;
					}
				}
			}
		}
	}
}

//Moves player according to the Enumirator.
//Not the most beautiful way of doing it, but gets the job done :) 
void GameBoard::MovePlayer(GameBoard::MoveDirection direction)
{
	for (int y = 0; y < m_boardSizeY; ++y)
	{
		for (int x = 0; x < m_boardSizeX; ++x)
		{
			if (m_board[y][x].m_IsPlayerCursor)
			{
				switch (direction)
				{
				case MoveDirection::EDown:
					if ((y + 1) < m_boardSizeY)
					{
						std::cout << "Going DOWN" << std::endl;
						m_board[y][x].m_IsPlayerCursor = false;
						m_board[y + 1][x].m_IsPlayerCursor = true;
						DrawBoard();
						return;
					}
					else
						break;
					break;
				case MoveDirection::EUp:
					if ((y - 1) >= 0)
					{
						std::cout << "Going UP" << std::endl;
						m_board[y][x].m_IsPlayerCursor = false;
						m_board[y - 1][x].m_IsPlayerCursor = true;
						DrawBoard();
						return;
					}
					else
						break;
					break;
				case MoveDirection::ERight:
					if ((x + 1) < m_boardSizeX)
					{
						std::cout << "Going RIGHT" << std::endl;
						m_board[y][x].m_IsPlayerCursor = false;
						m_board[y][x + 1].m_IsPlayerCursor = true;
						DrawBoard();
						return;
					}
					else
						break;
					break;
				case MoveDirection::ELeft:
					if ((x - 1) >= 0)
					{
						std::cout << "Going LEFT" << std::endl;
						m_board[y][x].m_IsPlayerCursor = false;
						m_board[y][x - 1].m_IsPlayerCursor = true;
						DrawBoard();
						return;
					}
					else
						break;
					break;
				default:
					std::cout << "MovePlayer(): Something went wrong. You should not see this message" << std::endl;
					break;
				}
			}
		}
	}
}

//Simple function to open the tile
void GameBoard::OpenTheTile()
{
	BoardPiece *currentPlayerTilePtr = &GetTile();

	//CHECK TILES
	switch (currentPlayerTilePtr->m_tyleType)
	{
	case BoardPiece::EBombsAround:
		//OPEN JUST THiS TILE
		currentPlayerTilePtr->m_isOpen = true;
		break;
	case BoardPiece::EWithBomb:
		//BLOW UP! GAME OVER
		//Has to loop thourgh all of the bombs, and open them
		Explode();
		break;
	case BoardPiece::EEmpty:
		if (!currentPlayerTilePtr->m_isOpen && currentPlayerTilePtr->m_tyleType == BoardPiece::EEmpty)
		{
			currentPlayerTilePtr->CheckTilesAround(*this);
		}
		break;
	default:
		std::cout << "Ops... Something went wrong in GameBoard::OpenTheTile(). Fix it!" << std::endl;
		break;
	}
}

//Same as open the tile, just checks the boolean.
void GameBoard::MarkTheTile()
{
	BoardPiece *tempPlayerPosPtr = &GetTile();
	//MARKED
	//If youre player cursor, youre closed and you HAVE NOT BEEN marked yet
	if (tempPlayerPosPtr->m_IsPlayerCursor && !tempPlayerPosPtr->m_isOpen && !tempPlayerPosPtr->m_beenMarked)
	{
		if (m_Player->m_markersLeft > 0)
		{
			tempPlayerPosPtr->m_beenMarked = true;
			m_Player->m_markersLeft--;
			
			if (tempPlayerPosPtr->m_tyleType == BoardPiece::EWithBomb)
				m_bombsMarked++;

			DrawBoard();
		}
		else
		{
			DrawBoard();
			SetConsoleTextAttribute(console, 14);
			std::cout << "Player has no more markers left" << std::endl;
		}
		
	}
	//UN-MARKED
	//if you're player cursor, you're closed and you HAVE BEEN makre 
	else if (tempPlayerPosPtr->m_IsPlayerCursor && !tempPlayerPosPtr->m_isOpen && tempPlayerPosPtr->m_beenMarked)
	{
		tempPlayerPosPtr->m_beenMarked = false;

		m_Player->m_markersLeft++;

		if (tempPlayerPosPtr->m_tyleType == BoardPiece::EWithBomb)
			m_bombsMarked--;

		DrawBoard();
	}

}
//EXPLODE! :) 
void GameBoard::Explode()
{
	for (int y = 0; y < m_boardSizeY; ++y)
	{
		for (int x = 0; x < m_boardSizeX; ++x)
		{
			if (m_board[y][x].m_tyleType == BoardPiece::EWithBomb)
			{
				m_board[y][x].m_isOpen = true;
			}
		}
	}

	DrawBoard();
}
//Draws the board. 
void GameBoard::DrawBoard()
{
	system("CLS");
	for (int y = 0; y < m_boardSizeY; ++y)
	{
		for (int x = 0; x < m_boardSizeX; ++x)
		{
			if(x == 0)
				std::cout << std::endl;
			//If you're not players cursor
			if (!m_board[y][x].m_IsPlayerCursor)
			{
				//If youre closed
				if (!m_board[y][x].m_isOpen)
				{
					//if you're have not been marked
					if (!m_board[y][x].m_beenMarked)
					{
						SetConsoleTextAttribute(console, 6);	//Brown
						std::cout << m_board[y][x].GetTyle<char>();
					}
					else
					{
						SetConsoleTextAttribute(console, 4);	//Red
						std::cout << m_board[y][x].GetTyle<char>();
					}
				}
				//If you're open
				else
				{
					if (m_board[y][x].m_tyleType == BoardPiece::EBombsAround)
					{
						switch (m_board[y][x].m_bombsAround)
						{
						case 1:
							SetConsoleTextAttribute(console, 1);	//Blue
							std::cout << m_board[y][x].GetTyle<int>();
							break;
						case 2:
							SetConsoleTextAttribute(console, 3);	//Cyan
							std::cout << m_board[y][x].GetTyle<int>();
							break;
						case 3:
							SetConsoleTextAttribute(console, 14);	//Yellow
							std::cout << m_board[y][x].GetTyle<int>();
							break;
						case 4:
							SetConsoleTextAttribute(console, 5);	//Magenta
							std::cout << m_board[y][x].GetTyle<int>();
							break;
						default:
							SetConsoleTextAttribute(console, 4);	//Red
							std::cout << m_board[y][x].GetTyle<int>();
							break;
						}
					}
					else if (m_board[y][x].m_tyleType == BoardPiece::EWithBomb)
					{
						SetConsoleTextAttribute(console, 4);	//Red
						std::cout << m_board[y][x].GetTyle<char>();
					}
					else
					{
						std::cout << m_board[y][x].GetTyle<char>();
					}
				}
			}
			else
			{
				SetConsoleTextAttribute(console, 2);		//Green
				std::cout << m_board[y][x].GetTyle<char>();
			}
		}
	}

	SetConsoleTextAttribute(console, 15);		//Green
	std::cout << std::endl;
	std::cout << "--Development variables--" << std::endl;
	std::cout << "Bombs on the field:		" << m_bombsPlaced << std::endl;
#ifdef DEBUGGIN 
	std::cout << "Bombs marked:			" << m_bombsMarked << std::endl;
	std::cout << "Players Markers Left:		" << m_Player->m_markersLeft << std::endl;

#else
	std::cout << "Players Markers Left:		" << m_Player->m_markersLeft << std::endl;
#endif
}
//Returns current player posistion tile
//Checked by m_IsPlayerCursor bool
BoardPiece& GameBoard::GetTile()
{
	BoardPiece *currentTilePtr = nullptr;
	for (int y = 0; y < m_boardSizeY; ++y)
	{
		for (int x = 0; x < m_boardSizeX; ++x)
		{
			if (m_board[y][x].m_IsPlayerCursor)
			{
				currentTilePtr = &m_board[y][x];
				return *currentTilePtr;
				break;
			}
		}
	}
}
//Returns pointer to an array
//[0] - Tiles Left Closed 
//[1] - Bombs Marked; 
void GameBoard::ReturnPlayerScore(int& tilesLeft, int& bombsMarked)
{
	tilesLeft = 0;
	bombsMarked = 0;

	//Count tiles left. Done
	for (int y = 0; y < m_boardSizeY; ++y)
	{
		for (int x = 0; x < m_boardSizeX; ++x)
		{
			//if you're closed
			if (!m_board[y][x].m_isOpen)
				tilesLeft += 1;
		}
	}
	bombsMarked = m_bombsMarked;
}

/* COLORS
0   BLACK
1   BLUE
2   GREEN
3   CYAN
4   RED
5   MAGENTA
6   BROWN
7   LIGHTGRAY
8   DARKGRAY
9   LIGHTBLUE
10  LIGHTGREEN
11  LIGHTCYAN
12  LIGHTRED
13  LIGHTMAGENTA
14  YELLOW
15  WHITE
*/