#pragma once
#include "Player.h"

struct UI
{
	int m_difficultyLevel;

	UI()
		: m_difficultyLevel(0)
	{

	}
	
	//Greetin of the game
	void DrawGreeting();
	//Description
	void DrawDescription();
	//Updates score
	int UpdateScore(int& playerScore);
	void SelectDifficultyLevel();
	int& GetDifficultyLevel();
	//Main Menu
	void DrawMainMenu();
	//Game statistics screen, drawn every time player makes an input
	int DrawGameStatisticScreen(const int& bombsOnTheField, const int& playerMarkedBombs, const int& playerMarkersLeft);

	void GameOverScreen(const int& bombsOnTheField, const int& markedBombs, const int& tilesLeft, Player& playerReference);
	void SaveProgressToFile(Player& currentPlayer);
	void ReadScoresFromFile();
};
	