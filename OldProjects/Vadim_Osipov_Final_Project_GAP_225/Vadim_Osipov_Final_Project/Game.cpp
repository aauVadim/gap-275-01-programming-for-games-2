//System
#include<iostream>
#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#include <string>
#include <conio.h>
#include <stdio.h>

#include "Game.h"


bool *bIsGameRunningPtr;
bool *bIsPlayingPtr;
UI *gameUIPtr;
GameBoard *gameBoardPtr;

void HandleInput(int input);

void main()
{


	//resize window portion came from CPlusPlus.com
	HWND console = GetConsoleWindow();
	RECT r;
	GetWindowRect(console, &r); //stores the console's current dimensions

	//MoveWindow(window_handle, x, y, width, height, redraw_window);
	MoveWindow(console, r.left, r.top, 800, 800, TRUE);


	//Main game loop bool
	bool isGameRunning = true;
	bIsGameRunningPtr = &isGameRunning;

	while (*bIsGameRunningPtr)
	{
		//Inner game loop bool
		bool isPlaying = true;
		bIsPlayingPtr = &isPlaying;
		//Game UI
		UI gameUI;
		gameUIPtr = &gameUI;

		//Game Board reference
		gameUIPtr->DrawMainMenu();

		GameBoard gameBoard(gameUIPtr->GetDifficultyLevel());
		gameBoardPtr = &gameBoard;

		gameBoardPtr->DrawBoard();

		while (*bIsPlayingPtr)
		{
			int input = _getch();
			HandleInput(input);
			//You you have marked al of the bombs. Win Game
			if (gameBoardPtr->m_bombsMarked == gameBoardPtr->m_bombsPlaced)
				*bIsPlayingPtr = false;
		}

		//Some duct-tape... Try to fix
		int tilesLeft, bombsMarked;
		//Player Class counts the score according to the bombs marked and tiles open on the field
		gameBoardPtr->ReturnPlayerScore(tilesLeft, bombsMarked);
		//Then Game Over screen drawn, ether win or lose. 
		gameUIPtr->GameOverScreen(gameBoardPtr->m_bombsPlaced, bombsMarked, tilesLeft, *gameBoardPtr->m_Player);
	}
}
//Input handler
void HandleInput(int input)
{
	switch (input)
	{
	case 'p'://Reseting the game loop
		std::cout << "reseting the main menu" << std::endl;
		*bIsPlayingPtr = false;
		break;
	//Move up
	case 'w':
		gameBoardPtr->MovePlayer(GameBoard::MoveDirection::EUp); 
		break;
	//Move left
	case 'a':
		gameBoardPtr->MovePlayer(GameBoard::MoveDirection::ELeft);
		break;
	//Move right
	case 's':
		gameBoardPtr->MovePlayer(GameBoard::MoveDirection::EDown);
		break;
	//Move down
	case 'd':
		gameBoardPtr->MovePlayer(GameBoard::MoveDirection::ERight);
		break;
	case 32:
		//REally rough work around, but if you oppened a bomb you're done. 
		if (gameBoardPtr->GetTile().m_tyleType == BoardPiece::EWithBomb)
		{
			gameBoardPtr->OpenTheTile();
			*bIsPlayingPtr = false;
		}
		else
			gameBoardPtr->OpenTheTile();
		break;
	case 'e':
		//Simply marks the tile
		gameBoardPtr->MarkTheTile();
		break;
	default:
		std::cout << "Wrong input, press something else! \n";
		break;
	}
}