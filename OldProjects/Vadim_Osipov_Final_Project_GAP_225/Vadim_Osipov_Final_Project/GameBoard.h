#pragma once
#include "BoardPiece.h"
#include "Player.h"
#define WIN32_LEAN_AND_MEAN
#include <windows.h>

struct GameBoard
{
	unsigned int m_boardSizeX;
	unsigned int m_boardSizeY;

	enum MoveDirection
	{
		EDown, 
		EUp, 
		ERight, 
		ELeft
	};

	Player *m_Player; 
	//Double pointer?!
	BoardPiece **m_board;
	//Used to count the bombs on the field
	int m_bombsPlaced; 
	int m_bombsMarked;

	//Constructor
	GameBoard(const int difficultyLevel);
	//Demolisher
	~GameBoard();

	//Builds the board
	void BuildBoard(const int boardY, const int boardX);
	//Places bombs randomly on the board
	void PlaceBombs();
	//Sets the numbers of the tiles according to the bobms around
	void InitializeTileNumbers();
	//Moves Player Indicator 
	void MovePlayer(GameBoard::MoveDirection direction);
	//Opens the tile. Checks tiles around
	void OpenTheTile();
	//Marks the tile that player is on
	void MarkTheTile();
	//Game over
	void Explode();
	//Draws the board
	void DrawBoard();
	//Returns the tile that player cursor is on
	BoardPiece& GetTile();

	void ReturnPlayerScore(int& tilesLeft, int& bombsMarked);

};