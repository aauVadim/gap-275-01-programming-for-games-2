#include "Player.h"


//Calculates Player Score Based on how many bombs were marked and how many tiles left closed
//ToDo: Maybe add some fancy score function
int& Player::SetPlayerScore(const int& bombsMarked, const int& tilesLeftClosed)
{
	int result;
	result = bombsMarked + tilesLeftClosed;
	m_score = result;
	return result;
}